#include "bluecontrol.h"

#include <err.h>
#include <ev++.h>
#include <cxxopts.hpp>
#include <sys/capsicum.h>

#include <iostream>
#include <string>
#include <vector>

#include <string.h>

#include "api.h"

#ifndef BLUED_VERSION_STRING
#define BLUED_VERSION_STRING "unknown version"
#endif

// Global variable used to store the program's exit code.
int _exit_code;

class sig_handler
{
public:
	sig_handler(ev::loop_ref loop) : loop(loop) {}

	void handle(ev::sig &s, int event)
	{
		loop.break_loop(ev::ALL);
	}

private:
	ev::loop_ref loop;
};

int main(int argc, char *argv[])
{
	ev::default_loop loop;
	cxxopts::Options options("bluecontrol", "Send commands to blued");
	sig_handler int_handler(loop);
	ev::sig s_int(loop);
	sig_handler term_handler(loop);
	ev::sig s_term(loop);

	options.add_options()
		("command",
		 "Command to send to blued\n"
		 "list_known\n"
		 "pair bdaddr\n"
		 "request_name bdaddr\n"
		 "scan\n"
		 "set_pin bdaddr pin\n"
		 "unpair bdaddr\n"
		 "connect bdaddr\n"
		 "disconnect bdaddr\n",
		 cxxopts::value<std::string>())
		("arguments", "Arguments to command",
		 cxxopts::value<std::vector<std::string>>())
		("s,socket", "Path to blued's socket (Default: /var/run/blued/rpc)",
		 cxxopts::value<std::string>()->default_value("/var/run/blued/rpc"))
		("h,help", "Print usage")
		("version", "Print program version");
	options.parse_positional({"command", "arguments"});
	options.positional_help("command arguments");
	options.show_positional_help();

	cxxopts::ParseResult cmds;
	try
	{
		cmds = options.parse(argc, argv);
	}
	catch (const std::exception &e)
	{
		std::cout << "Error: " << e.what() << std::endl;
		return 1;
	}

	if (cmds.count("help") || cmds.arguments().empty())
	{
		std::cout << options.help() << std::endl;
		return 0;
	}

	if (cmds.count("version"))
	{
		std::cout << "Version: \n" << BLUED_VERSION_STRING << std::endl;
		return 0;
	}

	std::string command =
		cmds.count("command") ? cmds["command"].as<std::string>() : "";
	std::vector<std::string> arguments;
	if (cmds.count("arguments"))
		arguments = cmds["arguments"].as<std::vector<std::string>>();
	try
	{
		bluecontrol bc(loop, cmds["socket"].as<std::string>().c_str());

		if (cap_enter())
			throw std::runtime_error("cap_enter failed: " +
					std::string(strerror(errno)));

		s_int.set<sig_handler, &sig_handler::handle>(&int_handler);
		s_int.set(SIGINT);
		s_int.start();

		s_term.set<sig_handler, &sig_handler::handle>(&term_handler);
		s_term.set(SIGTERM);
		s_term.start();

		if (command == "pair")
		{
			if (arguments.size() != 1)
			{
				std::cout << options.help() << std::endl;
				return 1;
			}
			bc.pair(arguments[0]);
		}
		else if (command == "scan")
			bc.scan();
		else if (command == "set_pin")
		{
			if (arguments.size() != 2)
			{
				std::cout << options.help() << std::endl;
				return 1;
			}
			bc.set_pin(arguments[0], arguments[1]);
		}
		else if (command == "list_known")
			bc.list_known();
		else if (command == "unpair")
		{
			if (arguments.size() != 1)
			{
				std::cout << options.help() << std::endl;
				return 1;
			}
			bc.unpair(arguments[0]);
		}
		else if (command == "request_name")
		{
			if (arguments.size() != 1)
			{
				std::cout << options.help() << std::endl;
				return 1;
			}
			bc.request_name(arguments[0]);
		}
		else if (command == "connect")
		{
			if (arguments.size() != 1)
			{
				std::cout << options.help() << std::endl;
				return 1;
			}
			bc.connect(arguments[0]);
		}
		else if (command == "disconnect")
		{
			if (arguments.size() != 1)
			{
				std::cout << options.help() << std::endl;
				return 1;
			}
			bc.disconnect(arguments[0]);
		}
		else
		{
			std::cerr << "Unrecognised command: " << command << std::endl;
			return 1;
		}

		loop.run();
	}
	catch (const std::exception &e)
	{
		std::cerr << "Error: " << e.what() << std::endl;
	}

	return _exit_code;
}
