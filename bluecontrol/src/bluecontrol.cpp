#include "bluecontrol.h"

#include <err.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <exception>
#include <iostream>

#include "api.h"
#include "api_helper.h"

// Global variable used to store the program's exit code.
extern int _exit_code;

bluecontrol::bluecontrol(ev::default_loop &loop, const std::string &socket_path) :
	loop(loop), rpc_watcher(loop)
{
	int rpc_fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (rpc_fd == -1)
		throw std::runtime_error("Could not create RPC socket: " +
		                         std::string(strerror(errno)));

	fd = rpc_fd;

	sockaddr_un addr{};
	strcpy(addr.sun_path, socket_path.c_str());
	addr.sun_len = socket_path.size();
	addr.sun_family = AF_UNIX;

	if (::connect(rpc_fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) == -1)
		throw std::runtime_error("Could not connect to blued socket: " +
		                         std::string(strerror(errno)));

	rpc_watcher.set<bluecontrol, &bluecontrol::rpc_callback>(this);
	rpc_watcher.set(rpc_fd, EV_READ);
	rpc_watcher.start();

}

void bluecontrol::rpc_callback(ev::io &watcher, int events)
{
	ssize_t res;

	if (events & EV_READ)
	{
		unpacker.reserve_buffer(blue::MAX_RPC_MSG_SIZE);
		res = recv(watcher.fd, unpacker.buffer(), blue::MAX_RPC_MSG_SIZE, 0);
		if (res == -1 || res == 0)
		{
			err(1, "Failed to receive RPC data");
			loop.break_loop(ev::ALL);
			return;
		}
		unpacker.buffer_consumed(res);

		msgpack::object_handle oh;
		while (unpacker.next(oh))
		{
			msgpack::object deserialised = oh.get();

			blue::rpc_msg m;
			deserialised.convert(m);

			switch (m.op)
			{
			case blue::rpc_op::OP_PAIR_START:
				std::cout << "Pairing started" << std::endl;
				break;
			case blue::rpc_op::OP_PAIR_DONE:
				std::cout << "Device paired successfully" << std::endl;
				done(0);
				break;
			case blue::rpc_op::OP_PAIR_UNKNOWN:
				std::cout << "Device not found" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_PAIR_ERROR:
				std::cerr << "An error occurred!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_PAIR_ALREADY_PAIRED:
				std::cerr << "The device is already considered paired. "
				          << "Unpair it first." << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_SCAN_START:
				break;
			case blue::rpc_op::OP_SCAN_ITEM:
			{
				blue::msg_scan_item item;
				deserialised.convert(item);
				std::cout << item.addr << std::endl;
				break;
			}
			case blue::rpc_op::OP_SCAN_DONE:
				done(0);
				break;
			case blue::rpc_op::OP_DEVICE_PIN_DONE:
				done(0);
				break;
			case blue::rpc_op::OP_DEVICE_PIN_UNKNOWN:
				std::cerr << "Could not set PIN! Device unknown!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_DEVICE_PIN_TOO_LONG:
				std::cerr << "Provided PIN is too long!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_LIST_KNOWN_ITEM:
			{
				blue::msg_known_item m;
				deserialised.convert(m);

				std::cout << m.addr;

				if (m.name != "")
					std::cout << " " << m.name;

				if (m.paired || m.connected)
				{
					std::cout << " ("
					          << (m.paired ? "paired" : "" )
					          << (m.connected ?
					              (m.paired ? ", connected)" : "connected)")
					              : ")");
				}

				std::cout << std::endl;
				break;
			}
			case blue::rpc_op::OP_LIST_KNOWN_DONE:
				done(0);
				break;
			case blue::rpc_op::OP_UNPAIR_DONE:
				done(0);
				break;
			case blue::rpc_op::OP_UNPAIR_NOT_PAIRED:
				std::cerr << "The device is not paired!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_UNPAIR_UNKNOWN:
				std::cerr << "Unknown device!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_UNPAIR_ERROR:
				std::cerr << "An error occurred!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_NAME_REQUEST_RUNNING:
				std::cerr << "Querying name from device..." << std::endl;
				break;
			case blue::rpc_op::OP_NAME_REQUEST_UNKNOWN:
				std::cerr << "Requested name for unknown device!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_NAME_REQUEST_ERROR:
				std::cerr << "An error ocurred requesting the name!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_NAME_REQUEST_DONE:
			{
				blue::msg_name_request_done m;
				deserialised.convert(m);
				std::cout << m.name << " (" << m.addr << ")" << std::endl;
				done(0);
				break;
			}
			case blue::rpc_op::OP_CONNECTION_REQUEST_RUNNING:
				std::cerr << "Connection request in progress..." << std::endl;
				break;
			case blue::rpc_op::OP_CONNECTION_REQUEST_DONE:
				std::cout << "Device connected!" << std::endl;
				done(0);
				break;
			case blue::rpc_op::OP_CONNECTION_REQUEST_ERROR:
				std::cerr << "Could not connect to device!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_CONNECTION_REQUEST_UNKNOWN:
				std::cerr << "Unknown device!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_DISCONNECT_REQUEST_RUNNING:
				std::cerr << "Disconnect request in progress..." << std::endl;
				break;
			case blue::rpc_op::OP_DISCONNECT_REQUEST_DONE:
				std::cout << "Device disconnected!" << std::endl;
				done(0);
				break;
			case blue::rpc_op::OP_DISCONNECT_REQUEST_ERROR:
				std::cerr << "Could not disconnect from device!" << std::endl;
				done(1);
				break;
			case blue::rpc_op::OP_DISCONNECT_REQUEST_UNKNOWN:
				std::cerr << "Unknown device!" << std::endl;
				done(1);
				break;
			default:
				std::cerr << "Received unknown RPC message with OP: "
				          << static_cast<uint32_t>(m.op) << std::endl;
				done(1);
				break;
			}
		}
	}
}

void bluecontrol::pair(std::string addr)
{
	blue::msg_pair_request m;

	m.addr = addr;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send pair request: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::scan()
{
	blue::rpc_msg m;

	m.op = blue::rpc_op::OP_SCAN_REQUEST;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send scan start request: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::set_pin(std::string addr, std::string pin)
{
	blue::msg_set_pin_request m;

	m.op = blue::rpc_op::OP_DEVICE_PIN_SET;
	m.addr = addr;
	m.pin = pin;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send set PIN request: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::list_known()
{
	blue::rpc_msg m;

	m.op = blue::rpc_op::OP_LIST_KNOWN_REQUEST;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send list known request: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::unpair(std::string addr)
{
	blue::msg_unpair_request m;

	m.addr = addr;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send unpair request: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::request_name(std::string addr)
{
	blue::msg_name_request m;

	m.addr = addr;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send name reuest: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::connect(std::string addr)
{
	blue::msg_conn_request m;

	m.addr = addr;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send connection reuest: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::disconnect(std::string addr)
{
	blue::msg_discon_request m;

	m.addr = addr;
	if (!blue::send_reply(fd, m))
		throw std::runtime_error("Failed to send disconnect request: " +
		                         std::string(strerror(errno)));
}

void bluecontrol::done(int exit_code)
{
	_exit_code = exit_code;
	loop.break_loop(ev::ALL);
}
