#ifndef BLUECONTROL_H
#define BLUECONTROL_H

#include <ev++.h>
#include <msgpack.hpp>

class bluecontrol
{
public:
	bluecontrol(ev::default_loop &loop, const std::string &socket_path);
	~bluecontrol() = default;

	void rpc_callback(ev::io &watcher, int events);

	void pair(std::string addr);
	void scan();
	void set_pin(std::string addr, std::string pin);
	void list_known();
	void unpair(std::string addr);
	void request_name(std::string addr);
	void connect(std::string addr);
	void disconnect(std::string addr);

private:
	void done(int exit_code);

	ev::default_loop &loop;
	ev::io rpc_watcher;
	int fd;
	msgpack::unpacker unpacker;
};

#endif // BLUECONTROL_H
