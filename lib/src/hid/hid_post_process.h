#ifndef HID_POST_PROCESS_H
#define HID_POST_PROCESS_H

namespace blue::hid
{

/*
 * Fill in fields in the hid descriptor from the raw USB descriptor.
 */
int32_t process_hid_descriptor(hid_descriptor &d);

}; // namespace blue::hid
#endif // HID_POST_PROCESS_H
