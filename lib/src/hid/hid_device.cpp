#include "hid/hid_device.h"

#include <sys/fcntl.h>
#include <syslog.h>

#include "cap_kbd.h"
#include "cap_sdp.h"
#include "hid/bthid.h"

namespace blue::hid
{

hid_device::hid_device(hid_device_config &&config, blue::hid::hid_descriptor &&d) :
	kbd_chan(config.kbd_chan),
	addr(config.addr),
	desc(std::move(d)),
	ctrl(std::move(config.ctrl)),
	ctrl_watcher(config.loop),
	intr_watcher(config.loop),
	parent(config.parent),
	cons_fd(config.cons_fd),
	uinput(config.uinput)
{
	init_state();
	populate_devid(config.sdp_chan, config.local_addr);

	ctrl_watcher.set<hid_device, &hid_device::ctrl_callback>(this);
	ctrl_watcher.set(ctrl, ev::READ);
	ctrl_watcher.start();
}

void hid_device::init_state()
{
	intr = util::make_unique_fd(-1);
	vkbd = util::make_unique_fd(-1);
	ukbd = util::make_unique_fd(-1);
	umouse = util::make_unique_fd(-1);
	obutt = 0;
	obutt_cons = 0;
	consk = 0;
	keys1 = {};
	keys2 = {};

	for (int i = 0; i < MOUSE_MAXBUTTON; i++)
	{
		initial_click[i] = std::chrono::steady_clock::now();
		clicks[i] = 0;
	}
}

void hid_device::populate_devid(util::shared_cap_chan sdp_chan,
                                blue::bdaddr local_addr)
{
	try
	{
		std::optional<blue::hid::device_id> devid =
			blue::hid::get_devid(sdp_chan, local_addr, addr);
		if (devid)
		{
			vendor_id = devid->vendor_id;
			product_id = devid->product_id;
			version = devid->version;

			syslog(LOG_DEBUG, "%s: vendor_id = 0x%04x, product_id = 0x%04x, "
			       "version = 0x%04x", addr.str().c_str(), vendor_id,
			       product_id, version);
		}
		else
			populate_default_devid();
	}
	catch (const std::runtime_error &)
	{
		populate_default_devid();
	}
}

void hid_device::populate_default_devid()
{
	syslog(LOG_NOTICE, "Could not query device ID from %s."
	       " Falling back on defaults.", addr.str().c_str());

	// Defaults taken from Bluetooth DeviceID_SPEC V13.
	vendor_id = 0xFFFF;
	// product_id and version are selected at will by the manufacturer
	// and have no default in the spec so set them to zero.
	product_id = 0;
	version = 0;
}

hid_device::~hid_device()
{
	intr_watcher.stop();
	ctrl_watcher.stop();
}

void hid_device::disconnect()
{
	syslog(LOG_NOTICE, "Device %s disconnected", addr.str().c_str());
	parent->disconnect(addr);
}

void hid_device::intr_opened(util::unique_fd &&intr_fd)
{
	if (desc.keyboard)
	{
		vkbd = blue::kbd::cap_open_vkbdctl(kbd_chan);
		if (vkbd == -1)
		{
			syslog(LOG_ERR, "Could not open virtual keyboard");
			throw std::runtime_error("Could not open virtual keyboard; "
			                         "try \"kldload vkbd\"");
		}
	}

	if (desc.mouse && uinput)
	{
		// FIXME: local address?
		umouse = uinput_open_mouse(blue::bdaddr("00:00:00:00:00:00"));
		if (umouse == -1)
		{
			syslog(LOG_ERR, "Could not open virtual uinput mouse");
			throw std::runtime_error("could not open virtual uinput mouse");
		}

	}

	if (desc.keyboard && uinput)
	{
		// FIXME: local address?
		ukbd = uinput_open_keyboard(blue::bdaddr("00:00:00:00:00:00"));
		if (ukbd == -1)
		{
			syslog(LOG_ERR, "Could not open virtual uinput keyboard");
			throw std::runtime_error("could not open virtual uinput keyboard");
		}

	}

	intr = std::move(intr_fd);
	intr_watcher.set<hid_device, &hid_device::intr_callback>(this);
	intr_watcher.set(intr, ev::READ);
	intr_watcher.start();
}

void hid_device::ctrl_callback(ev::io &watcher, int events)
{
	std::array<uint8_t, 4096> buf;
	ssize_t ret;

	if ((ret = read(watcher.fd, buf.data(), buf.size())) == -1)
	{
		syslog(LOG_ERR, "Failed reading control data for %s",
		       addr.str().c_str());
		return;
	}

	if (ret == 0)
	{
		disconnect();
		return;
	}

	if (hid_control(buf.data(), ret) == -1)
	{
		syslog(LOG_ERR, "Failed handling control data for %s",
		       addr.str().c_str());
		return;
	}
}

void hid_device::intr_callback(ev::io &watcher, int events)
{
	std::array<uint8_t, 4096> buf;
	ssize_t ret;

	if ((ret = read(watcher.fd, buf.data(), buf.size())) == -1)
	{
		syslog(LOG_ERR, "Failed reading interrupt data for %s",
		       addr.str().c_str());
		return;
	}

	if (ret == 0)
	{
		disconnect();
		return;
	}

	if (hid_interrupt(buf.data(), ret) == -1)
	{
		syslog(LOG_ERR, "Failed handling interrupt data for %s",
		       addr.str().c_str());
		return;
	}
}

} // namespace blue::hid
