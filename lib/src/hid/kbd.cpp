/*
 * kbd.c
 */

/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2006 Maksim Yevmenkin <m_evmenkin@yahoo.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id: kbd.c,v 1.4 2006/09/07 21:06:53 max Exp $
 * $FreeBSD$
 */

#include <sys/consio.h>
#include <sys/ioctl.h>
#include <sys/kbio.h>
#include <sys/queue.h>
#include <sys/wait.h>
#include <assert.h>
#include <bluetooth.h>
#include <dev/usb/usb.h>
#include <dev/usb/usbhid.h>
#include <dev/vkbd/vkbd_var.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include "hid/kbd.h"

#include "hid/hid_device.h"

namespace blue::hid
{

static void kbd_write(std::bitset<xsize> &m, int32_t fb, int32_t make, int32_t fd);
static int32_t	kbd_xlate(int32_t code, int32_t make, int32_t *b, int32_t const *eob);

/*
 * Process keys
 */

int32_t
hid_device::kbd_process_keys()
{
	std::bitset<xsize> diff;
	int32_t		f1, f2, i;

	assert(parent != NULL);

	/* Check if the new keys have been pressed */
	f1 = -1;
	if (keys1.any())
	{
		f1 = 0;
		while (f1 < keys1.size() && !keys1.test(f1))
			f1++;
	}

	/* Check if old keys still pressed */
	f2 = -1;
	if (keys2.any())
	{
		f2 = 0;
		while (f2 < keys2.size() && !keys2.test(f2))
			f2++;
	}

	if (f1 == -1) {
		/* no new key pressed */
		if (f2 != -1) {
			/* release old keys */
			kbd_write(keys2, f2, 0, vkbd);
			uinput_kbd_write(keys2, f2, 0, ukbd);
			keys2 = {};
		}

		return (0);
	}

	if (f2 == -1) {
		/* no old keys, but new keys pressed */
		assert(f1 != -1);

		keys2 = keys1;
		kbd_write(keys1, f1, 1, vkbd);
		uinput_kbd_write(keys1, f1, 1, ukbd);
		keys1 = {};

		return (0);
	}

	/* new keys got pressed, old keys got released */
	diff = {};

	for (i = f2; i < keys2.size(); i ++) {
		if (keys2.test(i)) {
			if (!keys1.test(i)) {
				keys2.reset(i);
				diff.set(i);
			}
		}
	}

	for (i = f1; i < xsize; i++) {
		if (keys1.test(i)) {
			if (!keys2.test(i))
				keys2.set(i);
			else
				keys1.reset(i);
		}
	}

	f2 = -1;
	for (size_t i = 0; i < diff.size(); i++)
	{
		if (diff.test(i))
			f2 = i;
	}

	if (f2 > 0) {
		kbd_write(diff, f2, 0, vkbd);
		uinput_kbd_write(diff, f2, 0, ukbd);
	}

	f1 = -1;
	for (size_t i = 0; i < keys1.size(); i++)
	{
		if (keys1.test(i))
			f1 = i;
	}
	if (f1 > 0) {
		kbd_write(keys1, f1, 1, vkbd);
		uinput_kbd_write(keys1, f1, 1, ukbd);
		keys1 = {};
	}

	return (0);
}

/*
 * Translate given keymap and write keyscodes
 */
void
hid_device::uinput_kbd_write(const std::bitset<xsize> &m, int32_t fb,
		int32_t make, int fd)
{
	int32_t i;

	if (fd >= 0) {
		for (i = fb; i < m.size(); i++) {
			if (m.test(i))
				uinput_rep_key(fd, i, make);
		}
	}
}

/*
 * Translate given keymap and write keyscodes
 */

static void
kbd_write(std::bitset<xsize> &m, int32_t fb, int32_t make, int32_t fd)
{
	int32_t	i, *b, *eob, n, buf[64];

	b = buf;
	eob = b + sizeof(buf)/sizeof(buf[0]);
	i = fb;

	while (i < m.size()) {
		if (m.test(i)) {
			n = kbd_xlate(i, make, b, eob);
			if (n == -1) {
				write(fd, buf, (b - buf) * sizeof(buf[0]));
				b = buf;
				continue;
			}

			b += n;
		}

		i ++;
	}

	if (b != buf)
		write(fd, buf, (b - buf) * sizeof(buf[0]));
}

/*
 * Translate HID code into PS/2 code and put codes into buffer b.
 * Returns the number of codes put in b. Return -1 if buffer has not
 * enough space.
 */

#undef  PUT
#define PUT(c, n, b, eob)	\
do {				\
	if ((b) >= (eob))	\
		return (-1);	\
	*(b) = (c);		\
	(b) ++;			\
	(n) ++;			\
} while (0)

static int32_t
kbd_xlate(int32_t code, int32_t make, int32_t *b, int32_t const *eob)
{
	int32_t	c, n;

	n = 0;

	if (code >= xsize)
		return (0); /* HID code is not in the table */

	/* Handle special case - Pause/Break */
	if (code == 0x48) {
		if (!make)
			return (0); /* No break code */

#if 0
XXX FIXME
		if (ctrl_is_pressed) {
			/* Break (Ctrl-Pause) */
			PUT(0xe0, n, b, eob);
			PUT(0x46, n, b, eob);
			PUT(0xe0, n, b, eob);
			PUT(0xc6, n, b, eob);
		} else {
			/* Pause */
			PUT(0xe1, n, b, eob);
			PUT(0x1d, n, b, eob);
			PUT(0x45, n, b, eob);
			PUT(0xe1, n, b, eob);
			PUT(0x9d, n, b, eob);
			PUT(0xc5, n, b, eob);
		}
#endif

		return (n);
	}

	if ((c = x[code]) == -1)
		return (0); /* HID code translation is not defined */

	if (make) {
		if (c & E0PREFIX)
			PUT(0xe0, n, b, eob);

		PUT((c & CODEMASK), n, b, eob);
	} else if (!(c & NOBREAK)) {
		if (c & E0PREFIX)
			PUT(0xe0, n, b, eob);

		PUT((0x80|(c & CODEMASK)), n, b, eob);
	}

	return (n);
}

/*
 * Process status change from vkbd(4)
 */

int32_t
hid_device::kbd_status_changed(uint8_t *data, int32_t len)
{
	vkbd_status_t	st;
	uint8_t		found, report_id;
	hid_data_t	d;
	hid_item_t	h;
	uint8_t		leds_mask = 0;

	assert(len == sizeof(vkbd_status_t));

	memcpy(&st, data, sizeof(st));
	found = 0;
	report_id = NO_REPORT_ID;

	data[0] = 0xa2; /* DATA output (HID output report) */
	data[1] = 0x00;
	data[2] = 0x00;

	for (d = hid_start_parse(desc.desc, 1 << hid_output, -1);
	     hid_get_item(d, &h) > 0; ) {
		if (HID_PAGE(h.usage) == HUP_LEDS) {
			found++;

			if (report_id == NO_REPORT_ID)
				report_id = h.report_ID;
			else if (h.report_ID != report_id)
				syslog(LOG_WARNING, "Output HID report IDs " \
					"for %s do not match: %d vs. %d. " \
					"Please report",
					addr.str().c_str(),
					h.report_ID, report_id);

			switch(HID_USAGE(h.usage)) {
			case 0x01: /* Num Lock LED */
				if (st.leds & LED_NUM)
					hid_set_data(&data[1], &h, 1);
				leds_mask |= LED_NUM;
				break;

			case 0x02: /* Caps Lock LED */
				if (st.leds & LED_CAP)
					hid_set_data(&data[1], &h, 1);
				leds_mask |= LED_CAP;
				break;

			case 0x03: /* Scroll Lock LED */
				if (st.leds & LED_SCR)
					hid_set_data(&data[1], &h, 1);
				leds_mask |= LED_SCR;
				break;

			/* XXX add other LEDs ? */
			}
		}
	}
	hid_end_parse(d);

	if (report_id != NO_REPORT_ID) {
		data[2] = data[1];
		data[1] = report_id;
	}

	if (found)
		write(intr, data, (report_id != NO_REPORT_ID) ? 3 : 2);

	if (found && uinput && desc.keyboard)
		uinput_rep_leds(ukbd, st.leds, leds_mask);

	return (0);
}

} // namespace blue::hid
