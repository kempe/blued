/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 2006 Maksim Yevmenkin <m_evmenkin@yahoo.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $Id: parser.y,v 1.7 2006/09/07 21:06:53 max Exp $
 * $FreeBSD$
 *
 * NOTE: Imported from bthidd revision
 *       9553ea80a8807d9ff2b7b3498bdb002f6cb66b26.
 *
 */

#include <stdint.h>
#include <unistd.h>

#include <dev/usb/usbhid.h>
#include <syslog.h>

#include "hid/bthid.h"

namespace blue::hid
{

int32_t process_hid_descriptor(hid_descriptor &d)
{
	hid_data_t	hd;
	hid_item_t	hi;
	int32_t		page, mdepth;

	mdepth = 0;

	/* XXX somehow need to make sure descriptor is valid */
	for (hd = hid_start_parse(d.desc, ~0, -1); hid_get_item(hd, &hi) > 0; ) {
		switch (hi.kind) {
		case hid_collection:
			if (mdepth != 0)
				mdepth++;
			else if (hi.collection == 1 &&
			     hi.usage ==
			      HID_USAGE2(HUP_GENERIC_DESKTOP, HUG_MOUSE))
				mdepth++;
			break;
		case hid_endcollection:
			if (mdepth != 0)
				mdepth--;
			break;
		case hid_output:
		case hid_feature:
			break;

		case hid_input:
			/* Check if the device may send keystrokes */
			page = HID_PAGE(hi.usage);
			if (page == HUP_KEYBOARD)
				d.keyboard = 1;
			if (page == HUP_CONSUMER &&
			    (hi.flags & (HIO_CONST|HIO_RELATIVE)) == 0)
				d.has_cons = 1;
			/* Check if the device may send relative motion events */
			if (mdepth == 0)
				break;
			if (hi.usage ==
			     HID_USAGE2(HUP_GENERIC_DESKTOP, HUG_X) &&
			    (hi.flags & (HIO_CONST|HIO_RELATIVE)) == HIO_RELATIVE)
				d.mouse = 1;
			if (hi.usage ==
			     HID_USAGE2(HUP_GENERIC_DESKTOP, HUG_Y) &&
			    (hi.flags & (HIO_CONST|HIO_RELATIVE)) == HIO_RELATIVE)
				d.mouse = 1;
			if (hi.usage ==
			     HID_USAGE2(HUP_GENERIC_DESKTOP, HUG_WHEEL) &&
			    (hi.flags & (HIO_CONST|HIO_RELATIVE)) == HIO_RELATIVE)
				d.has_wheel = 1;
			if (hi.usage ==
			    HID_USAGE2(HUP_CONSUMER, HUC_AC_PAN) &&
			    (hi.flags & (HIO_CONST|HIO_RELATIVE)) == HIO_RELATIVE)
				d.has_hwheel = 1;
			break;
		}
	}
	hid_end_parse(hd);

	return (1);
}

} // namespace blue::hid
