#include "hid/bthid.h"

#include <syslog.h>

// FIXME: File bug upstream that include guards are missing
#ifndef USBHID_H
#define USBHID_H
#include <usbhid.h>
#endif // USBHID_H

#include <optional>

#include "cap_sdp.h"
#include "sdp_parser.h"
#include "hid_post_process.h"


#define SDP_SINGLE_ATTR(val) SDP_ATTR_RANGE((val), (val))

using namespace blue::sdp::parser;

namespace blue::hid
{

static uint16_t extract_control_psm(const attr &sdp_attr)
{
	try
	{
		/*
		 * Descriptor example dump obtained using
		 * sdp::parser::attr::str():
		 * elements = 2, type = 6, size_idx = 5
		 *     elements = 2, type = 6, size_idx = 5
		 *         type = 3, size_idx = 1
		 *         type = 1, size_idx = 1
		 *     elements = 1, type = 6, size_idx = 5
		 *         type = 3, size_idx = 1
		 */

		const sequence &s0 =
			dynamic_cast<const sequence &>(sdp_attr.get_entry());

		const sequence &s1 =
			dynamic_cast<const sequence &>(*s0.get_sequence().at(0));

		const uuid16 l2cap_id =
			dynamic_cast<const uuid16 &>(*s1.get_sequence().at(0));
		if (l2cap_id.get_value() != 0x0100)
			throw std::runtime_error("extract_control_psm: "
			                         "protocol ID violates HID specification");

		const numeric_entry<uint16_t> &psm_entry =
			dynamic_cast<const numeric_entry<uint16_t> &>(*s1.get_sequence().at(1));

		return psm_entry.get_value();
	}
	catch (const std::bad_cast &e)
	{
		syslog(LOG_ERR, "%s: not a proper HID descriptor", __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
	catch (const std::out_of_range &e)
	{
		syslog(LOG_ERR, "%s: unexpected number of elements in HID descriptor",
		       __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
}

static uint16_t extract_interrupt_psm(const attr &sdp_attr)
{
	try
	{
		/*
		 * Descriptor example dump obtained using
		 * sdp::parser::attr::str():
		 *
		 * elements = 1, type = 6, size_idx = 5
		 *     elements = 2, type = 6, size_idx = 5
		 *         elements = 2, type = 6, size_idx = 5
		 *             type = 3, size_idx = 1
		 *             type = 1, size_idx = 1
		 *         elements = 1, type = 6, size_idx = 5
		 *             type = 3, size_idx = 1
		 */

		const sequence &s0 =
			dynamic_cast<const sequence &>(sdp_attr.get_entry());

		const sequence &s1 =
			dynamic_cast<const sequence &>(*s0.get_sequence().at(0));

		const sequence &s2 =
			dynamic_cast<const sequence &>(*s1.get_sequence().at(0));

		const uuid16 l2cap_id =
			dynamic_cast<const uuid16 &>(*s2.get_sequence().at(0));
		if (l2cap_id.get_value() != 0x0100)
			throw std::runtime_error("extract_interrupt_psm: "
			                         "protocol ID violates HID specification");

		const numeric_entry<uint16_t> &psm_entry =
			dynamic_cast<const numeric_entry<uint16_t> &>(*s2.get_sequence().at(1));

		return psm_entry.get_value();
	}
	catch (const std::bad_cast &e)
	{
		syslog(LOG_ERR, "%s: not a proper HID descriptor", __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
	catch (const std::out_of_range &e)
	{
		syslog(LOG_ERR, "%s: unexpected number of elements in HID descriptor",
		       __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
}

static bool extract_bool(const attr &sdp_attr)
{
	try
	{
		return dynamic_cast<const boolean &>(sdp_attr.get_entry()).get_value();
	}
	catch (const std::bad_cast &e)
	{
		syslog(LOG_ERR, "%s: not a proper bool attribute", __FUNCTION__);
		throw std::runtime_error("SDP bool attribute parsing failed");
	}
}

static uint16_t extract_uint16(const attr &sdp_attr)
{
	try
	{
		return dynamic_cast<const numeric_entry<uint16_t> &>(
				sdp_attr.get_entry()).get_value();
	}
	catch (const std::bad_cast &e)
	{
		syslog(LOG_ERR, "%s: not a proper uint16_t attribute", __FUNCTION__);
		throw std::runtime_error("SDP uint16_t attribute parsing failed");
	}
}

static report_desc_t extract_usb_hid_desc(const attr &sdp_attr)
{
	// The descriptor is a USB HID descriptor. Convert it to binary
	// and use libusbhid to parse it.

	try
	{
		const sequence &s0 =
			dynamic_cast<const sequence &>(sdp_attr.get_entry());

		const sequence &s1 =
			dynamic_cast<const sequence &>(*s0.get_sequence().at(0));

		const numeric_entry<uint8_t> &id =
			dynamic_cast<const numeric_entry<uint8_t> &>(*s1.get_sequence().at(0));
		if (id.get_value() <= 0x20 || id.get_value() > 0x30)
		{
			syslog(LOG_ERR, "The USB descriptor type class is invalid: %u",
			       id.get_value());
			throw std::runtime_error("invalid USB descriptor type");
		}

		std::vector<uint8_t> bin_desc = s1.get_sequence().at(1)->bin();
		report_desc_t usb_desc =
			hid_use_report_desc(bin_desc.data(), bin_desc.size());
		if (!usb_desc)
		{
			syslog(LOG_ERR, "%s: could not allocate USB HID descriptor",
				   __FUNCTION__);
			throw std::runtime_error("HID parsing failed");
		}

		return usb_desc;
	}
	catch (const std::bad_cast &e)
	{
		syslog(LOG_ERR, "%s: not a proper HID descriptor", __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
	catch (const std::out_of_range &e)
	{
		syslog(LOG_ERR, "%s: unexpected number of elements in HID descriptor",
		       __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
}

static bool is_hid_device(const attr &sdp_attr)
{
	try
	{
		const sequence &s0 =
			dynamic_cast<const sequence &>(sdp_attr.get_entry());

		const entry &e = *s0.get_sequence().at(0);
		switch (e.get_size_idx())
		{
		case size_idx::two_bytes:
		{
			const uuid16 &id =
				dynamic_cast<const uuid16 &>(e);

			if (id.get_value() == SDP_SERVICE_CLASS_HUMAN_INTERFACE_DEVICE)
				return true;
			else
				return false;
		}
		case size_idx::four_bytes:
		case size_idx::sixteen_bytes:
			return false;
		default:
			throw std::bad_cast();
		}
	}
	catch (const std::bad_cast &e)
	{
		syslog(LOG_ERR, "%s: not a proper SDP attribute", __FUNCTION__);
		throw std::runtime_error("HID parsing failed");
	}
}

hid_descriptor::hid_descriptor() : desc(nullptr)
{
}

hid_descriptor::~hid_descriptor()
{
	if (desc)
		hid_dispose_report_desc(desc);
}

hid_descriptor::hid_descriptor(hid_descriptor &&other)
{
	desc = nullptr;
	*this = std::move(other);
}

hid_descriptor &hid_descriptor::operator=(hid_descriptor &&other)
{
	control_psm = other.control_psm;
	interrupt_psm = other.interrupt_psm;
	vendor_id = other.vendor_id;
	product_id = other.product_id;
	version = other.version;
	reconnect_initiate = other.reconnect_initiate;
	battery_power = other.battery_power;
	normally_connectable = other.normally_connectable;
	keyboard = other.keyboard;
	mouse = other.mouse;
	has_wheel = other.has_wheel;
	has_hwheel = other.has_hwheel;
	has_cons = other.has_cons;

	std::swap(desc, other.desc);

	return *this;
}

std::optional<hid_descriptor> get_descriptor(util::shared_cap_chan sdp_chan,
		blue::bdaddr local_addr, blue::bdaddr addr)
{
	hid_descriptor ret;
	uint64_t handle;

	if (blue::sdp::cap_sdp_open(sdp_chan, local_addr, addr, handle) == -1)
		throw std::runtime_error("Could not open SDP connection.");

	/*
	 * Bluetooth HID format taken from HUMAN INTERFACE DEVICE PROFILE
	 * v1.1.1 of the Bluetooth specification.
	 */

	std::vector<uint16_t> pp =
	{
		SDP_SERVICE_CLASS_HUMAN_INTERFACE_DEVICE,
	};
	std::vector<uint32_t> ap =
	{
		SDP_SINGLE_ATTR(SDP_ATTR_SERVICE_CLASS_ID_LIST),
		SDP_SINGLE_ATTR(SDP_ATTR_PROTOCOL_DESCRIPTOR_LIST),
		SDP_SINGLE_ATTR(SDP_ATTR_ADDITIONAL_PROTOCOL_DESCRIPTOR_LISTS),
		SDP_SINGLE_ATTR(blue::hid::HIDReconnectInitiate),
		SDP_SINGLE_ATTR(blue::hid::HIDDescriptorList),
		SDP_SINGLE_ATTR(blue::hid::HIDBatteryPower),
		SDP_SINGLE_ATTR(blue::hid::HIDNormallyConnectable),
	};
	std::vector<blue::sdp::sdp_attr> vp(ap.size());

	if (blue::sdp::cap_sdp_search(sdp_chan, handle, pp, ap, vp) == -1)
	{
		if (blue::sdp::cap_sdp_close(sdp_chan, handle) == -1)
			syslog(LOG_ERR, "Failed to close SDP connection.");
		throw std::runtime_error("Could not search for SDP attributes.");
	}

	if (blue::sdp::cap_sdp_close(sdp_chan, handle) == -1)
		syslog(LOG_ERR, "Failed to close SDP connection.");

	auto attr_itr = vp.begin();

	// If we didn't get the ID list, this is not a HID device.
	if (attr_itr->flags != SDP_ATTR_OK)
		return {};

	// Double check for posterity that we really have a HID device.
	const sdp::parser::attr id_list(*attr_itr++);
	if (!is_hid_device(id_list))
		return {};

	// SDP_ATTR_PROTOCOL_DESCRIPTOR_LIST contains the control PSM.
	const sdp::parser::attr pd(*attr_itr++);
	ret.control_psm = extract_control_psm(pd);

	// SDP_ATTR_ADDITIONAL_PROTOCOL_DESCRIPTOR_LISTS contains the
	// interrupt PSM.
	const sdp::parser::attr epd(*attr_itr++);
	ret.interrupt_psm = extract_interrupt_psm(epd);

	// blue::hid::HIDReconnectInitiate
	const sdp::parser::attr ri(*attr_itr++);
	ret.reconnect_initiate = extract_bool(ri);

	// blue::hid::HIDDescriptorList
	const sdp::parser::attr usb_hid(*attr_itr++);
	ret.desc = extract_usb_hid_desc(usb_hid);
	if (process_hid_descriptor(ret) == -1)
	{
		syslog(LOG_ERR, "%s: invalid USB HID descriptor", __FUNCTION__);
		throw std::runtime_error("invalid USB HID descriptor");
	}

	// blue::hid::HIDBatteryPower is an optional attribute,
	// we default to false if it is not present.
	ret.battery_power = false;
	if (vp[4].flags == SDP_ATTR_OK)
	{
		const sdp::parser::attr bp(*attr_itr++);
		ret.battery_power = extract_bool(bp);
	}

	// blue::hid::HIDNormallyConnectable is an optional attribute,
	// we default ot false if it is not present.
	ret.normally_connectable = false;
	if (vp[5].flags == SDP_ATTR_OK)
	{
		const sdp::parser::attr nc(*attr_itr++);
		ret.normally_connectable = extract_bool(nc);
	}

	return ret;
}

std::optional<device_id> get_devid(util::shared_cap_chan sdp_chan,
		blue::bdaddr local_addr, blue::bdaddr addr)
{
	device_id ret;
	uint64_t handle;

	if (blue::sdp::cap_sdp_open(sdp_chan, local_addr, addr, handle) == -1)
		throw std::runtime_error("Could not open SDP connection.");

	std::vector<uint16_t> pp =
	{
		SDP_SERVICE_CLASS_PNP_INFORMATION,
	};
	std::vector<uint32_t> ap =
	{
		SDP_SINGLE_ATTR(SpecificationID),
		SDP_SINGLE_ATTR(VendorID),
		SDP_SINGLE_ATTR(ProductID),
		SDP_SINGLE_ATTR(Version),
		SDP_SINGLE_ATTR(PrimaryRecord),
		SDP_SINGLE_ATTR(VendorIDSource),
	};
	std::vector<blue::sdp::sdp_attr> vp(ap.size());

	if (blue::sdp::cap_sdp_search(sdp_chan, handle, pp, ap, vp) == -1)
	{
		if (blue::sdp::cap_sdp_close(sdp_chan, handle) == -1)
			syslog(LOG_ERR, "Failed to close SDP connection.");
		throw std::runtime_error("Could not search for SDP attributes.");
	}

	if (blue::sdp::cap_sdp_close(sdp_chan, handle) == -1)
		syslog(LOG_ERR, "Failed to close SDP connection.");

	auto attr_itr = vp.begin();

	ret.specification_id = extract_uint16(*attr_itr++);
	ret.vendor_id = extract_uint16(*attr_itr++);
	ret.product_id = extract_uint16(*attr_itr++);
	ret.version = extract_uint16(*attr_itr++);
	ret.primary_record = extract_bool(*attr_itr++);
	ret.vendor_id_source = extract_uint16(*attr_itr++);

	return ret;
}

}
