#include "cap_l2cap.h"

#include <fcntl.h>

extern "C"
{
#include <sys/nv.h>
#include <libcasper.h>
#include <libcasper_service.h>
} // extern "C"
#include <bluetooth.h>

#include <string>

#include <syslog.h>

namespace blue::l2cap
{

static sockaddr_l2cap create_addr(const bdaddr_t *bd_addr, uint16_t psm)
{
	sockaddr_l2cap addr{};

	addr.l2cap_len = sizeof(addr);
	addr.l2cap_family = AF_BLUETOOTH;
	addr.l2cap_psm = htole16(psm);
	addr.l2cap_bdaddr_type = BDADDR_BREDR;
	std::copy(bd_addr, bd_addr + 1, &addr.l2cap_bdaddr);
	addr.l2cap_cid = 0;

	return addr;
}

static int bind_socket(const bdaddr_t *local_addr, uint16_t psm)
{

	sockaddr_l2cap addr = create_addr(local_addr, psm);

	int fd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BLUETOOTH_PROTO_L2CAP);
	if (fd == -1)
		return -1;

	if (bind(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) == -1)
	{
		syslog(LOG_ERR, "Failed to bind any l2cap socket");
		close(fd);
		return -1;
	}

	return fd;
}

util::unique_fd cap_l2cap_listen(util::shared_cap_chan chan, const bdaddr_t *listen_addr,
		uint16_t psm)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "l2cap_listen");
	nvlist_add_binary(nvl, "listenaddr", listen_addr, sizeof(bdaddr_t));
	nvlist_add_number(nvl, "psm", psm);

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return util::make_unique_fd(-1);
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return util::make_unique_fd(-1);
	}

	int fd = dup(nvlist_get_descriptor(nvl, "fd"));

	nvlist_destroy(nvl);

	return util::make_unique_fd(fd);
}

static int impl_l2cap_listen(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int fd = -1;
	uint16_t psm = static_cast<uint16_t>(nvlist_get_number(nvlin, "psm"));
	size_t addr_size;
	bdaddr_t listen_addr;

	const void *l = nvlist_get_binary(nvlin, "listenaddr", &addr_size);

	if (sizeof(bdaddr_t) != addr_size)
		return EINVAL;

	std::copy(reinterpret_cast<const bdaddr_t *>(l),
	          reinterpret_cast<const bdaddr_t *>(l) + 1,
	          &listen_addr);

	if ((fd = bind_socket(&listen_addr, psm)) == -1)
		return errno;

	if (listen(fd, 128) == -1)
	{
		close(fd);
		return errno;
	}

	nvlist_add_descriptor(nvlout, "fd", fd);

	close(fd);

	return 0;
}

util::unique_fd cap_l2cap_connect(util::shared_cap_chan chan,
		const bdaddr_t *local_addr, uint16_t psm,
		const bdaddr_t *addr)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "l2cap_connect");
	nvlist_add_binary(nvl, "localaddr", local_addr, sizeof(bdaddr_t));
	nvlist_add_number(nvl, "psm", psm);
	nvlist_add_binary(nvl, "addr", addr, sizeof(bdaddr_t));

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return util::make_unique_fd(-1);
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return util::make_unique_fd(-1);
	}

	int fd = dup(nvlist_get_descriptor(nvl, "fd"));

	nvlist_destroy(nvl);

	return util::make_unique_fd(fd);
}

static int impl_l2cap_connect(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int fd = -1;
	size_t addr_size;
	uint16_t psm = static_cast<uint16_t>(nvlist_get_number(nvlin, "psm"));
	bdaddr_t addr;
	bdaddr_t local_addr;

	const void *l = nvlist_get_binary(nvlin, "localaddr", &addr_size);

	if (sizeof(bdaddr_t) != addr_size)
		return EINVAL;

	std::copy(reinterpret_cast<const bdaddr_t *>(l),
	          reinterpret_cast<const bdaddr_t *>(l) + 1,
	          &local_addr);

	const void *a = nvlist_get_binary(nvlin, "addr", &addr_size);

	if (sizeof(bdaddr_t) != addr_size)
		return EINVAL;

	std::copy(reinterpret_cast<const bdaddr_t *>(a),
	          reinterpret_cast<const bdaddr_t *>(a) + 1,
	          &addr);

	if ((fd = bind_socket(&local_addr, 0)) == -1)
		return errno;

	sockaddr_l2cap sa = create_addr(&addr, psm);
	const sockaddr *sp = reinterpret_cast<const sockaddr *>(&sa);
	if (connect(fd, sp, sizeof(sa)) == -1)
	{
		close(fd);
		return errno;
	}

	nvlist_add_descriptor(nvlout, "fd", fd);

	close(fd);

	return 0;
}

static int l2cap_command(const char *cmd, const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int error = EINVAL;
	const std::string cmd_str(cmd);

	if (cmd_str == "l2cap_listen")
		error = impl_l2cap_listen(limits, nvlin, nvlout);
	else if (cmd_str == "l2cap_connect")
		error = impl_l2cap_connect(limits, nvlin, nvlout);

	if (error)
		syslog(LOG_ERR, "cap_l2cap: error: %s", strerror(errno));

	return error;
}

static int l2cap_limit(const nvlist_t *oldlimits, const nvlist_t *newlimits)
{
	return 0;
}

CREATE_SERVICE("blue.l2cap", l2cap_limit, l2cap_command, 0);
} // namespace blue::l2cap
