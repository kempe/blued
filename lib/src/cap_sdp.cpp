#include "cap_sdp.h"

#include "bdaddr.h"

#include <assert.h>
extern "C"
{
#include <sys/nv.h>
#include <libcasper.h>
#include <libcasper_service.h>
} // extern "C"

#include <map>
#include <string>

// Container for keeping track of open sdp sessions.
// A uint64_t handle maps to a void pointer returned by sdp_open.
static uint64_t next_handle = 0;
static std::map<uint64_t, void *> sdp_handles;

namespace blue::sdp
{

sdp_attr::operator sdp_attr_t()
{
	sdp_attr_t ret;

	ret.flags = flags;
	ret.attr = attr;
	ret.vlen = vlen;
	ret.value = value.data();

	return ret;
}

int cap_sdp_open(util::shared_cap_chan chan, const bdaddr_t *l,
                 const bdaddr_t *r, uint64_t &handle)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "sdp_open");
	nvlist_add_binary(nvl, "localaddr", l, sizeof(bdaddr_t));
	nvlist_add_binary(nvl, "remoteaddr", r, sizeof(bdaddr_t));

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return -1;
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return -1;
	}

	handle = nvlist_get_number(nvl, "handle");
	nvlist_destroy(nvl);
	return 0;
}

static int impl_sdp_open(const nvlist_t *limits, nvlist_t *nvlin,
		nvlist_t *nvlout)
{
	size_t addr_size;
	bdaddr_t local;
	bdaddr_t remote;

	const void *l = nvlist_get_binary(nvlin, "localaddr", &addr_size);

	if (sizeof(bdaddr_t) != addr_size)
		return EINVAL;

	std::copy(reinterpret_cast<const bdaddr_t *>(l),
	          reinterpret_cast<const bdaddr_t *>(l) + 1,
	          &local);

	const void *r = nvlist_get_binary(nvlin, "remoteaddr", &addr_size);

	if (sizeof(bdaddr_t) != addr_size)
		return EINVAL;

	std::copy(reinterpret_cast<const bdaddr_t *>(r),
	          reinterpret_cast<const bdaddr_t *>(r) + 1,
	          &remote);

	void *h = sdp_open(&local, &remote);
	if (!h)
		return ENOMEM;

	uint64_t handle = next_handle++;
	sdp_handles[handle] = h;

	nvlist_add_number(nvlout, "handle", handle);

	return 0;
}

template<class T>
static std::vector<uint64_t> to_uint64_vector(T other)
{
	std::vector<uint64_t> ret;

	std::copy(other.begin(), other.end(), std::back_inserter(ret));

	return ret;
}

template<class T>
static std::vector<T> to_T_vector(const nvlist *nvl, const char *key)
{
	size_t length;
	std::vector<T> ret;

	const uint64_t *p = nvlist_get_number_array(nvl, key, &length);
	std::copy(p, p + length, std::back_inserter(ret));

	return ret;
}

static nvlist *pack_sdp_attr(const sdp_attr &attr)
{
	nvlist_t *nvl = nvlist_create(0);
	if (!nvl)
		return nullptr;

	nvlist_add_number(nvl, "flags", attr.flags);
	nvlist_add_number(nvl, "attr", attr.attr);
	nvlist_add_number(nvl, "vlen", attr.vlen);
	nvlist_add_binary(nvl, "value", attr.value.data(), attr.vlen);

	return nvl;
}

static int pack_sdp_attrs(nvlist *nvl, const std::vector<sdp_attr> vp)
{
	std::vector<nvlist *> attrs;

	for (const sdp_attr &attr : vp)
	{
		nvlist *attr_list = pack_sdp_attr(attr);
		if (!attr_list)
		{
			for (nvlist *p : attrs)
				nvlist_destroy(p);
			return -1;
		}
		attrs.push_back(attr_list);
	}
	nvlist_add_nvlist_array(nvl, "vp", attrs.data(), attrs.size());

	for (nvlist *l : attrs)
		nvlist_destroy(l);

	return 0;
}

static int unpack_sdp_attr(const nvlist *nvl, sdp_attr &attr)
{
	size_t attr_len;

	attr.flags = static_cast<uint16_t>(nvlist_get_number(nvl, "flags"));
	attr.attr = static_cast<uint16_t>(nvlist_get_number(nvl, "attr"));
	attr.vlen = static_cast<uint32_t>(nvlist_get_number(nvl, "vlen"));

	const void *p = nvlist_get_binary(nvl, "value", &attr_len);
	if (attr.value.size() < attr_len)
		return -1;
	const uint8_t *p_u8 = reinterpret_cast<const uint8_t *>(p);
	std::copy(p_u8, p_u8 + attr_len, attr.value.data());

	return 0;
}

static int unpack_sdp_attrs(nvlist *nvl, std::vector<sdp_attr> &attrs)
{
	size_t n_attrs;
	const nvlist_t *const *lists = nvlist_get_nvlist_array(nvl, "vp", &n_attrs);

	for (size_t i = 0; i < n_attrs; i++)
	{
		sdp_attr attr;
		if (unpack_sdp_attr(lists[i], attr) == -1)
			return -1;
		attrs.push_back(attr);
	}

	return 0;
}

int cap_sdp_search(util::shared_cap_chan chan, uint64_t handle,
		std::vector<uint16_t> pp, std::vector<uint32_t> ap,
		std::vector<sdp_attr> &vp)
{
	// Max 12 service IDs can be given
	assert(pp.size() <= 12);
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "sdp_search");
	nvlist_add_number(nvl, "handle", handle);
	std::vector<uint64_t> pp_64 = to_uint64_vector(pp);
	nvlist_add_number_array(nvl, "pp", pp_64.data(), pp_64.size());
	std::vector<uint64_t> ap_64 = to_uint64_vector(ap);
	nvlist_add_number_array(nvl, "ap", ap_64.data(), ap_64.size());
	if (pack_sdp_attrs(nvl, vp) == -1)
	{
		nvlist_destroy(nvl);
		errno = ENOMEM;
		return -1;
	}

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return -1;
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return -1;
	}

	vp.clear();
	if (unpack_sdp_attrs(nvl, vp) == -1)
	{
		nvlist_destroy(nvl);
		return -1;
	}

	nvlist_destroy(nvl);

	return 0;
}

static int impl_sdp_search(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{

	uint64_t handle;
	std::vector<uint16_t> pp;
	std::vector<uint32_t> ap;
	std::vector<sdp_attr> vp;
	std::vector<sdp_attr_t> vp_c_type;

	handle = nvlist_get_number(nvlin, "handle");
	pp = to_T_vector<uint16_t>(nvlin, "pp");
	ap = to_T_vector<uint32_t>(nvlin, "ap");
	if (unpack_sdp_attrs(nvlin, vp) == -1)
		return EINVAL;

	if (sdp_handles.count(handle) == 0)
		return EINVAL;
	void *xs = sdp_handles[handle];

	// Create a list of C API compatible types. The buffers are still
	// owned by the objects in the vp list.
	std::copy(vp.begin(), vp.end(), std::back_inserter(vp_c_type));

	if (sdp_search(xs, pp.size(), pp.data(), ap.size(), ap.data(),
	               vp_c_type.size(), vp_c_type.data()) == -1)
	{
		return sdp_error(xs);
	}

	// sdp_search will have updated the fields of our C type array.
	// Copy the updated vlen values back to our C++ type array.
	for (size_t i = 0; i < vp.size() && i < vp_c_type.size(); i++)
	{
		vp[i].flags = vp_c_type[i].flags;
		vp[i].attr = vp_c_type[i].attr;
		vp[i].vlen = vp_c_type[i].vlen;
	}

	if (pack_sdp_attrs(nvlout, vp) == -1)
		return ENOMEM;

	return 0;
}

int cap_sdp_close(util::shared_cap_chan chan, uint64_t handle)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "sdp_close");
	nvlist_add_number(nvl, "handle", handle);

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return -1;
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return -1;
	}

	nvlist_destroy(nvl);
	return 0;
}

static int impl_sdp_close(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	uint64_t handle = nvlist_get_number(nvlin, "handle");
	void *h;

	if (sdp_handles.count(handle) == 0)
		return EINVAL;
	h = sdp_handles[handle];
	sdp_handles.erase(handle);

	if (sdp_close(h) != 0)
		return EINVAL;

	return 0;
}

static int sdp_command(const char *cmd, const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int error = EINVAL;
	const std::string cmd_str(cmd);

	if (cmd_str == "sdp_open")
		error = impl_sdp_open(limits, nvlin, nvlout);
	else if (cmd_str == "sdp_search")
		error = impl_sdp_search(limits, nvlin, nvlout);
	else if (cmd_str == "sdp_close")
		error = impl_sdp_close(limits, nvlin, nvlout);

	return error;
}

static int sdp_limit(const nvlist_t *oldlimits, const nvlist_t *newlimits)
{
	return 0;
}

CREATE_SERVICE("blue.sdp", sdp_limit, sdp_command, 0);
} // namespace blue::sdp
