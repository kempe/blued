#include "btdevice.h"

#include <algorithm>
#include <sstream>
#include <string.h>

namespace blue
{

btdevice::btdevice(device_info info, hci_socket *s) :
	hci_observer(),
	current_state(device_state::IDLE),
	addr(info.addr),
	connected(false),
	con_handle(INVALID_HANDLE),
	paired(false),
	pin("0000"),
	page_scan_rep_mode(info.page_scan_rep_mode),
	page_scan_period_mode(info.page_scan_period_mode),
	page_scan_mode(info.page_scan_mode),
	clock_offset(info.clock_offset),
	name_request_running(false),
	connection_request_running(false),
	socket(s),
	observer(nullptr)
{
	socket->register_observer(this);
}

btdevice::~btdevice()
{
	socket->unregister_observer(this);
}

btdevice &btdevice::operator=(btdevice &&other)
{
	current_state = other.current_state;
	addr = std::move(other.addr);
	other.addr = bdaddr{};
	name = std::move(other.name);
	other.name = "";
	connected = other.connected;
	connection_request_running = other.connection_request_running;
	con_handle = other.con_handle;
	other.con_handle = INVALID_HANDLE;
	paired = other.paired;
	link_key = std::move(other.link_key);
	pin = std::move(other.pin);
	socket = other.socket;
	observer = other.observer;
	other.observer = nullptr;
	page_scan_rep_mode = other.page_scan_rep_mode;
	page_scan_period_mode = other.page_scan_period_mode;
	page_scan_mode = other.page_scan_mode;
	clock_offset = other.clock_offset;

	socket->register_observer(this);

	return *this;
}

btdevice::btdevice(btdevice &&other)
{
	*this = std::move(other);
}

void btdevice::register_observer(device_observer *observer)
{
	this->observer = observer;
}

void btdevice::set_paired(const key &link_key)
{
	paired = true;
	this->link_key = link_key;
}

const bdaddr &btdevice::get_addr() const
{
	return addr;
}

uint16_t btdevice::get_con_handle() const
{
	return con_handle;
}

bool btdevice::is_paired() const
{
	return paired;
}

const std::array<uint8_t, NG_HCI_KEY_SIZE> &btdevice::get_link_key() const
{
	return link_key;
}

const std::string &btdevice::get_pin() const
{
	return pin;
}

void btdevice::set_pin(std::string new_pin)
{
	if (new_pin.size() > NG_HCI_PIN_SIZE)
		throw std::runtime_error("PIN is too long");
	pin = new_pin;
}

void btdevice::connect()
{

	if (connection_request_running)
	{
		syslog(LOG_DEBUG, "%s: a connection request is already in progress",
		       addr.str().c_str());
		return;
	}
	connection_request_running = true;

	socket->connect_device(this, addr);

	process_state(device_event::CONNECT);
}

void btdevice::disconnect()
{
	socket->disconnect_device(this, con_handle);
}

bool btdevice::is_connected() const
{
	return connected;
}

void btdevice::send_auth_req()
{
	if (!connected)
		throw std::runtime_error("Must be connected to send auth request");

	socket->send_auth_request(this, con_handle);
}

void btdevice::enable_encryption()
{
	socket->set_encryption(this, con_handle, true);
}

bool btdevice::pair()
{
	if (current_state != device_state::IDLE)
		throw std::runtime_error("Operation already in progress");

	if (paired)
		return false;

	process_state(device_event::PAIR);

	return true;
}

void btdevice::request_name()
{
	// Only allow one request at a time.
	if (name_request_running)
		return;

	socket->request_name(this, addr, page_scan_mode, page_scan_rep_mode,
	                     clock_offset);

	name_request_running = true;
}

const std::string &btdevice::get_name() const
{
	return name;
}

void btdevice::set_name(std::string new_name)
{
	if (name.size() > NG_HCI_UNIT_NAME_SIZE)
		throw std::runtime_error("Device name too long");

	name = new_name;
}

void btdevice::set_page_scan_rep_mode(uint8_t mode)
{
	page_scan_rep_mode = mode;
}

void btdevice::encryption_change(bool success, bool enabled)
{
	const std::string s_addr = addr.str();
	if (success)
	{
		if (enabled)
			syslog(LOG_DEBUG, "Encryption enabled for %s", s_addr.c_str());
		else
			syslog(LOG_DEBUG, "Encryption disabled for %s", s_addr.c_str());
	}
	else
		syslog(LOG_ERR, "Encryption change failed for %s", s_addr.c_str());
}

void btdevice::neg_link_key_reply(bool success)
{
	if (!success && current_state == device_state::PAIRING)
	{
		syslog(LOG_ERR, "Failed to send Negative Link Key Reply. "
		                "Aborting pairing.");
		current_state = device_state::IDLE;
	}
}

void btdevice::link_key_reply(bool success)
{
	if (!success)
		syslog(LOG_ERR, "Failed to send Link Key Reply.");
}

void btdevice::pin_code_reply(bool success)
{
	if (!success)
		syslog(LOG_ERR, "Failed to send PIN Code Reply.");
}

void btdevice::set_page_scan_period_mode(uint8_t mode)
{
	page_scan_period_mode = mode;
}

void btdevice::set_page_scan_mode(uint8_t mode)
{
	page_scan_mode = mode;
}

void btdevice::set_clock_offset(uint16_t offset)
{
	clock_offset = offset;
}

void btdevice::con_compl(bool success, uint16_t handle)
{
	connection_request_running = false;

	if (success)
	{
		con_handle = handle;
		process_state(device_event::CONNECTION_COMPLETED);
	}
	else
	{
		con_handle = INVALID_HANDLE;
		process_state(device_event::CONNECTION_FAILED);
	}

	emit_event<&device_observer::device_event>(*this, NG_HCI_EVENT_CON_COMPL,
	                                           success);
}

void btdevice::discon_compl(bool success)
{
	if (success)
		process_state(device_event::DISCONNECT_COMPLETED);

	emit_event<&device_observer::device_event>(*this, NG_HCI_EVENT_DISCON_COMPL,
	                                           success);
	emit_event<&device_observer::disconnect_complete>(*this, success);
}

void btdevice::link_key_req()
{
	process_state(device_event::LINK_KEY_REQUEST);

	emit_event<&device_observer::device_event>(*this, NG_HCI_EVENT_LINK_KEY_REQ,
	                                           0x00);
}

void btdevice::link_key_notification(blue::key new_link_key)
{
	if (paired || current_state == device_state::PAIRING)
		link_key = new_link_key;

	if (observer)
		observer->device_event(*this, NG_HCI_EVENT_LINK_KEY_NOTIFICATION, 0x00);
	emit_event<&device_observer::device_event>(*this,
		NG_HCI_EVENT_LINK_KEY_NOTIFICATION, 0x00);
}

void btdevice::io_capability_request()
{
	if (current_state == device_state::PAIRING)
	{
		uint8_t io_capability = 0x03; /* 0x03 - NoInputNoOutput */
		uint8_t oob_data_present = 0x00; /* 0x00 - OOB authentication data not present */
		 /* 0x03 - MITM protection required - Dedicated Bonding. Use IO
		  * Capabilities to determine authentication procedure. */
		uint8_t authentication_requirements = 0x03;
		socket->io_capability_request_reply(this, addr, io_capability,
		                                    oob_data_present,
		                                    authentication_requirements);
	}

	emit_event<&device_observer::device_event>(*this,
		NG_HCI_EVENT_IO_CAPABILITY_REQUEST, 0x00);

}

void btdevice::io_capability_request_reply(bool success)
{
	if (!success)
		process_state(device_event::IO_CAPABILITY_REQUEST_REPLY_FAILED);

	emit_event<&device_observer::device_event>(*this,
		NG_HCI_IO_CAPABILITY_REQUEST_REPLY, success);
}

void btdevice::user_confirmation_request(uint32_t numeric_value)
{
	if (current_state == device_state::PAIRING)
		socket->user_confirmation_request_reply(this, addr);

	emit_event<&device_observer::device_event>(*this,
			NG_HCI_EVENT_USER_CONFIRMATION_REQUEST, 0x00);
}

void btdevice::user_confirmation_request_reply(bool success)
{
	if (!success)
		process_state(device_event::USER_CONFIRMATION_REQUEST_REPLY_FAILED);
}

void btdevice::auth_compl(bool success)
{
	if (success)
		process_state(device_event::AUTH_SUCCESSFUL);
	else
		process_state(device_event::AUTH_FAILED);

	emit_event<&device_observer::device_event>(*this,
			NG_HCI_EVENT_AUTH_COMPL, success);
}

void btdevice::pin_code_req()
{
	process_state(device_event::PIN_REQUEST);
}

void btdevice::remote_name(bool success, std::string name)
{
	name_request_running = false;

	if (success)
	{
		this->name = name;
	}

	emit_event<&device_observer::remote_name>(*this, success);
}

void btdevice::page_scan_mode_change(uint8_t page_scan_mode)
{
	set_page_scan_mode(page_scan_mode);
}
void btdevice::page_scan_rep_mode_change(uint8_t page_scan_rep_mode)
{
	set_page_scan_rep_mode(page_scan_rep_mode);
}

void btdevice::pairing_finished(bool success)
{
	current_state = device_state::IDLE;
	paired = success;

	if (success)
		enable_encryption();

	emit_event<&device_observer::pairing_finished>(*this, success);
}

void btdevice::process_state(device_event event)
{
	switch (event)
	{
		case device_event::CONNECT:
			// The connection attempt is part of the pairing process.
			if (current_state == device_state::PAIRING)
				break;

			current_state = device_state::CONNECTING;
			break;
		case device_event::CONNECTION_COMPLETED:
			connected = true;

			if (current_state == device_state::PAIRING)
				send_auth_req();
			else
				current_state = device_state::IDLE;

			emit_event<&device_observer::connection_complete>(*this, true);
			break;
		case device_event::CONNECTION_FAILED:
			connected = false;

			if (current_state == device_state::PAIRING)
				pairing_finished(false);

			emit_event<&device_observer::connection_complete>(*this, false);
			break;
		case device_event::PAIR:
			current_state = device_state::PAIRING;
			if (!connected)
				connect();
			else
				send_auth_req();
			break;
		case device_event::AUTH_SUCCESSFUL:
			if (current_state == device_state::PAIRING)
				pairing_finished(true);
			break;
		case device_event::AUTH_FAILED:
			if (current_state == device_state::PAIRING)
				pairing_finished(false);
			break;
		case device_event::LINK_KEY_REQUEST:
			if (current_state == device_state::PAIRING)
				socket->send_negative_link_key_reply(this, addr);
			else
				socket->send_link_key_reply(this, addr, link_key);
			break;
		case device_event::PIN_REQUEST:
			socket->send_pin_code_reply(this, addr, pin);
			break;
		case device_event::DISCONNECT_COMPLETED:
			connected = false;
			con_handle = INVALID_HANDLE;
			if (current_state == device_state::PAIRING)
				pairing_finished(false);
			break;
		case device_event::IO_CAPABILITY_REQUEST_REPLY_FAILED:
			if (current_state == device_state::PAIRING)
				pairing_finished(false);
			break;
		case device_event::USER_CONFIRMATION_REQUEST_REPLY_FAILED:
			if (current_state == device_state::PAIRING)
				pairing_finished(false);
			break;
	};
}
} // namespace blue
