#ifndef TEST_H
#define TEST_H

#include <functional>
#include <iostream>

class test_case
{
public:
	test_case(std::string name, std::function<void()> test) : name(name)
	{
		try
		{
			std::cout << name << ": ";
			test();
			std::cout << "OK!" <<std::endl;
		}
		catch (const std::exception &e)
		{
			std::cout << e.what() << std::endl;
			exit(1);
		}
	}

	~test_case()
	{
	}

private:
	std::string name;
};

#define TEST_CASE(name) \
	void name(); \
	static test_case t_##name(#name, name); \
	void name()

#endif // TEST_H
