#include "test.h"

#include <assert.h>
#include <sys/endian.h>

#include <sstream>

#include "sdp_parser.h"

using namespace blue::sdp;

TEST_CASE(test_parse_uint8)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uint | parser::size_idx::one_byte;
	uint8_t value = 0xAA;

	// Pack a uint8_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	attr.value[1] = value;

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte);

	// Cast and check value
	const parser::numeric_entry<uint8_t> &entry =
		dynamic_cast<const parser::numeric_entry<uint8_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_int8)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::sint | parser::size_idx::one_byte;
	int8_t value = -16;

	// Pack an int8_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	attr.value[1] = value;

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::sint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte);

	// Cast and check value
	const parser::numeric_entry<int8_t> &entry =
		dynamic_cast<const parser::numeric_entry<int8_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uint16)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uint | parser::size_idx::two_bytes;
	uint16_t value = 0xAAAA;

	// Pack a uint16_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<uint16_t &>(attr.value[1]) = htobe16(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::two_bytes);

	// Cast and check value
	const parser::numeric_entry<uint16_t> &entry =
		dynamic_cast<const parser::numeric_entry<uint16_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_int16)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::sint | parser::size_idx::two_bytes;
	int16_t value = -345;

	// Pack a int16_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<int16_t &>(attr.value[1]) = htobe16(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::sint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::two_bytes);

	// Cast and check value
	const parser::numeric_entry<int16_t> &entry =
		dynamic_cast<const parser::numeric_entry<int16_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uint32)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uint | parser::size_idx::four_bytes;
	uint32_t value = 0xAAAABBBB;

	// Pack a uint32_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<uint32_t &>(attr.value[1]) = htobe32(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::four_bytes);

	// Cast and check value
	const parser::numeric_entry<uint32_t> &entry =
		dynamic_cast<const parser::numeric_entry<uint32_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_int32)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::sint | parser::size_idx::four_bytes;
	int32_t value = -0xAAABBBB;

	// Pack a uint32_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<int32_t &>(attr.value[1]) = htobe32(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::sint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::four_bytes);

	// Cast and check value
	const parser::numeric_entry<int32_t> &entry =
		dynamic_cast<const parser::numeric_entry<int32_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uint64)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uint | parser::size_idx::eight_bytes;
	uint64_t value = 0xAAAABBBBCCCCDDDD;

	// Pack a uint64_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<uint64_t &>(attr.value[1]) = htobe64(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::eight_bytes);

	// Cast and check value
	const parser::numeric_entry<uint64_t> &entry =
		dynamic_cast<const parser::numeric_entry<uint64_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_int64)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::sint | parser::size_idx::eight_bytes;
	int64_t value = -0x0AAABBBBCCCCDDDD;

	// Pack an int64_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<int64_t &>(attr.value[1]) = htobe64(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::sint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::eight_bytes);

	// Cast and check value
	const parser::numeric_entry<int64_t> &entry =
		dynamic_cast<const parser::numeric_entry<int64_t> &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uint128)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uint | parser::size_idx::sixteen_bytes;
	parser::uint128_t value =
		(static_cast<parser::uint128_t>(0xAAAABBBBCCCCDDDD) << 64) +
		0xAAAABBBBCCCCDDDD;

	// Pack a uint128_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
#if _BYTE_ORDER == _LITTLE_ENDIAN
	uint64_t upper = value >> 64;
	uint64_t lower = value & 0xFFFFFFFFFFFFFFFF;

	uint64_t &low_ref =
		reinterpret_cast<uint64_t &>(attr.value[1]);
	uint64_t &high_ref =
		reinterpret_cast<uint64_t &>(attr.value[1 + sizeof(uint64_t)]);
	 low_ref = htobe64(upper);
	 high_ref = htobe64(lower);
#else
	reinterpret_cast<parser::uint128_t &>(attr.value[1]) = value;
#endif

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::sixteen_bytes);

	// Cast and check value
	const parser::numeric_entry<parser::uint128_t> &entry =
		dynamic_cast<const parser::numeric_entry<parser::uint128_t> &>(
			pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_int128)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::sint | parser::size_idx::sixteen_bytes;
	parser::int128_t value = static_cast<parser::int128_t>(0xBBBBCCCCDDDD) +
	                          0xAAAABBBBCCCCDDDD;
	value *= -1;

	// Pack a uint64_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
#if _BYTE_ORDER == _LITTLE_ENDIAN
	uint64_t upper = value >> 64;
	uint64_t lower = value & 0xFFFFFFFFFFFFFFFF;

	uint64_t &low_ref =
		reinterpret_cast<uint64_t &>(attr.value[1]);
	uint64_t &high_ref =
		reinterpret_cast<uint64_t &>(attr.value[1 + sizeof(uint64_t)]);
	 low_ref = htobe64(upper);
	 high_ref = htobe64(lower);
#else
	reinterpret_cast<parser::int128_t &>(attr.value[1]) = value;
#endif

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::sint);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::sixteen_bytes);

	// Cast and check value
	const parser::numeric_entry<parser::int128_t> &entry =
		dynamic_cast<const parser::numeric_entry<parser::int128_t> &>(
			pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uuid16)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uuid | parser::size_idx::two_bytes;
	uint16_t value = 0xDCBA;

	// Pack a uint16_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<uint16_t &>(attr.value[1]) = htobe16(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uuid);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::two_bytes);

	// Cast and check value
	const parser::uuid16 &entry =
		dynamic_cast<const parser::uuid16 &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uuid32)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uuid | parser::size_idx::four_bytes;
	uint32_t value = 0xBBBBAAAA;

	// Pack a uint32_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	reinterpret_cast<uint32_t &>(attr.value[1]) = htobe32(value);

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uuid);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::four_bytes);

	// Cast and check value
	const parser::uuid32 &entry =
		dynamic_cast<const parser::uuid32 &>(pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_uuid128)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::uuid | parser::size_idx::sixteen_bytes;
	parser::uint128_t value =
		(static_cast<parser::uint128_t>(0xAAAABBBBCCCCDDDD) << 64) +
		0xAAAABBBBCCCCDDDD;

	// Pack a uint128_t in an attribute
	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
#if _BYTE_ORDER == _LITTLE_ENDIAN
	uint64_t upper = value >> 64;
	uint64_t lower = value & 0xFFFFFFFFFFFFFFFF;

	uint64_t &low_ref =
		reinterpret_cast<uint64_t &>(attr.value[1]);
	uint64_t &high_ref =
		reinterpret_cast<uint64_t &>(attr.value[1 + sizeof(uint64_t)]);
	 low_ref = htobe64(upper);
	 high_ref = htobe64(lower);
#else
	reinterpret_cast<parser::uint128_t &>(attr.value[1]) = value;
#endif

	parser::attr pa(attr);

	// Validate parsed size and type
	assert(pa.get_entry().get_type() == parser::types::uuid);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::sixteen_bytes);

	// Cast and check value
	const parser::uuid128 &entry =
		dynamic_cast<const parser::uuid128 &>(
			pa.get_entry());
	assert(entry.get_value() == value);
}

TEST_CASE(test_parse_string_one_byte_len)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::string | parser::size_idx::one_byte_len;
	std::string s("Hej du glade!");
	uint8_t size_field = s.size();

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(size_field) + s.size();
	attr.value[0] = hdr;
	attr.value[1] = size_field;
	std::copy(s.begin(), s.end(), &attr.value[2]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::string);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte_len);

	const parser::string &entry =
		dynamic_cast<const parser::string &>(pa.get_entry());
	assert(entry.get_string() == s);
}

TEST_CASE(test_parse_string_two_byte_len)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::string | parser::size_idx::two_byte_len;
	std::string s("Hej du glade!");
	s.resize(255, 'b');
	uint16_t size_field = s.size();

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(size_field) + s.size();
	attr.value[0] = hdr;
	reinterpret_cast<uint16_t &>(attr.value[1]) = htobe16(size_field);
	std::copy(s.begin(), s.end(), &attr.value[3]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::string);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::two_byte_len);

	const parser::string &entry =
		dynamic_cast<const parser::string &>(pa.get_entry());
	assert(entry.get_string() == s);
}

TEST_CASE(test_parse_string_four_byte_len)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::string | parser::size_idx::four_byte_len;
	std::string s("Hej du glade!");
	// We are limited to the internal buffer size of
	// blue::sdp::sdp_attr which is, when writing this test, not large
	// enough to need a uint32_t to describe the string length.
	s.resize(255, 'b');
	uint32_t size_field = s.size();

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(size_field) + s.size();
	attr.value[0] = hdr;
	reinterpret_cast<uint32_t &>(attr.value[1]) = htobe32(size_field);
	std::copy(s.begin(), s.end(), &attr.value[5]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::string);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::four_byte_len);

	const parser::string &entry =
		dynamic_cast<const parser::string &>(pa.get_entry());
	assert(entry.get_string() == s);
}

TEST_CASE(test_parse_boolean_true)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::boolean | parser::size_idx::one_byte;
	uint8_t value = 1; // true

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(value);
	attr.value[0] = hdr;
	attr.value[1] = value;

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::boolean);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte);

	const parser::boolean &entry =
		dynamic_cast<const parser::boolean &>(pa.get_entry());
	assert(entry.get_value() == true);
}

TEST_CASE(test_parse_empty_seq)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::seq | parser::size_idx::one_byte_len;
	uint8_t length = 0;

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(length);
	attr.value[0] = hdr;
	attr.value[1] = length;

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::seq);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte_len);

	const parser::sequence &entry =
		dynamic_cast<const parser::sequence &>(pa.get_entry());
	assert(entry.get_sequence().empty());
}

TEST_CASE(test_parse_seq_one_uint16)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::seq | parser::size_idx::one_byte_len;
	uint8_t length = 3;

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(length) + length;
	attr.value[0] = hdr;
	attr.value[1] = length;
	attr.value[2] = parser::types::uint | parser::size_idx::two_bytes;
	reinterpret_cast<uint16_t &>(attr.value[3]) = htobe16(0xAB);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::seq);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte_len);

	const parser::sequence &entry =
		dynamic_cast<const parser::sequence &>(pa.get_entry());
	assert(entry.get_sequence().size() == 1);

	assert(entry.get_sequence()[0]->get_type() == parser::types::uint);
	assert(entry.get_sequence()[0]->get_size_idx() == parser::size_idx::two_bytes);

	const parser::numeric_entry<uint16_t> &u16 =
		dynamic_cast<const parser::numeric_entry<uint16_t> &>(*entry.get_sequence()[0]);
	assert(u16.get_value() == 0xAB);
}

TEST_CASE(test_parse_seq_select_one_uint16_one_string)
{
	std::stringstream buf;
	std::string s("Hej du glade!");

	blue::sdp::sdp_attr attr;
	attr.vlen = 2 + 5 + s.size();
	attr.value[0] = parser::types::seq_select | parser::size_idx::one_byte_len;
	attr.value[1] = 5 + s.size();
	attr.value[2] = parser::types::uint | parser::size_idx::two_bytes;
	reinterpret_cast<uint16_t &>(attr.value[3]) = htobe16(0xBEEF);
	attr.value[5] = parser::types::string | parser::size_idx::one_byte_len;
	attr.value[6] = s.size();
	std::copy(s.begin(), s.end(), &attr.value[7]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::seq_select);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte_len);

	const parser::sequence &entry =
		dynamic_cast<const parser::sequence &>(pa.get_entry());
	assert(entry.get_sequence().size() == 2);

	// Validate uint16_t list element
	assert(entry.get_sequence()[0]->get_type() == parser::types::uint);
	assert(entry.get_sequence()[0]->get_size_idx() == parser::size_idx::two_bytes);

	const parser::numeric_entry<uint16_t> &u16 =
		dynamic_cast<const parser::numeric_entry<uint16_t> &>(*entry.get_sequence()[0]);
	assert(u16.get_value() == 0xBEEF);

	// Validate string list element
	assert(entry.get_sequence()[1]->get_type() == parser::types::string);
	assert(entry.get_sequence()[1]->get_size_idx() == parser::size_idx::one_byte_len);

	const parser::string &str_element =
		dynamic_cast<const parser::string &>(*entry.get_sequence()[1]);
	assert(str_element.get_string() == s);
}

TEST_CASE(test_parse_seq_one_uint16_one_string)
{
	std::stringstream buf;
	std::string s("Hej du glade!");

	blue::sdp::sdp_attr attr;
	attr.vlen = 2 + 5 + s.size();
	attr.value[0] = parser::types::seq | parser::size_idx::one_byte_len;
	attr.value[1] = 5 + s.size();
	attr.value[2] = parser::types::uint | parser::size_idx::two_bytes;
	reinterpret_cast<uint16_t &>(attr.value[3]) = htobe16(0xBEEF);
	attr.value[5] = parser::types::string | parser::size_idx::one_byte_len;
	attr.value[6] = s.size();
	std::copy(s.begin(), s.end(), &attr.value[7]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::seq);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte_len);

	const parser::sequence &entry =
		dynamic_cast<const parser::sequence &>(pa.get_entry());
	assert(entry.get_sequence().size() == 2);

	// Validate uint16_t list element
	assert(entry.get_sequence()[0]->get_type() == parser::types::uint);
	assert(entry.get_sequence()[0]->get_size_idx() == parser::size_idx::two_bytes);

	const parser::numeric_entry<uint16_t> &u16 =
		dynamic_cast<const parser::numeric_entry<uint16_t> &>(*entry.get_sequence()[0]);
	assert(u16.get_value() == 0xBEEF);

	// Validate string list element
	assert(entry.get_sequence()[1]->get_type() == parser::types::string);
	assert(entry.get_sequence()[1]->get_size_idx() == parser::size_idx::one_byte_len);

	const parser::string &str_element =
		dynamic_cast<const parser::string &>(*entry.get_sequence()[1]);
	assert(str_element.get_string() == s);
}

TEST_CASE(test_parse_url_one_byte_len)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::url| parser::size_idx::one_byte_len;
	std::string s("https://ftp.lysator.liu.se/");
	uint8_t size_field = s.size();

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(size_field) + s.size();
	attr.value[0] = hdr;
	attr.value[1] = size_field;
	std::copy(s.begin(), s.end(), &attr.value[2]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::url);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::one_byte_len);

	const parser::url &entry =
		dynamic_cast<const parser::url &>(pa.get_entry());
	assert(entry.get_url() == s);
}

TEST_CASE(test_parse_url_two_byte_len)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::url | parser::size_idx::two_byte_len;
	std::string s("http://example.com/");
	uint16_t size_field = s.size();

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(size_field) + s.size();
	attr.value[0] = hdr;
	reinterpret_cast<uint16_t &>(attr.value[1]) = htobe16(size_field);
	std::copy(s.begin(), s.end(), &attr.value[3]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::url);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::two_byte_len);

	const parser::url &entry =
		dynamic_cast<const parser::url &>(pa.get_entry());
	assert(entry.get_url() == s);
}

TEST_CASE(test_parse_url_four_byte_len)
{
	std::stringstream buf;
	uint8_t hdr = parser::types::url | parser::size_idx::four_byte_len;
	std::string s("Hej du glade!");
	// We are limited to the internal buffer size of
	// blue::sdp::sdp_attr which is, when writing this test, not large
	// enough to need a uint32_t to describe the string length.
	s.resize(255, 'b');
	uint32_t size_field = s.size();

	blue::sdp::sdp_attr attr;
	attr.vlen = sizeof(hdr) + sizeof(size_field) + s.size();
	attr.value[0] = hdr;
	reinterpret_cast<uint32_t &>(attr.value[1]) = htobe32(size_field);
	std::copy(s.begin(), s.end(), &attr.value[5]);

	parser::attr pa(attr);

	assert(pa.get_entry().get_type() == parser::types::url);
	assert(pa.get_entry().get_size_idx() == parser::size_idx::four_byte_len);

	const parser::url &entry =
		dynamic_cast<const parser::url &>(pa.get_entry());
	assert(entry.get_url() == s);
}

int main()
{
	/*
	 * Intentionally left empty. Test cases are run by the TEST_CASE
	 * macro and class.
	 */
}
