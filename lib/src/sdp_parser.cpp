#include "sdp_parser.h"

#include <assert.h>
#include <sys/endian.h>

namespace blue::sdp::parser
{

static const entry *parse_attr(std::stringstream &buf)
{
	uint8_t hdr, type, size_idx;

	// Extract the header and mask out the type
	hdr = extract_type<uint8_t>(buf);
	type = hdr & types::mask;
	size_idx = hdr & size_idx::mask;

	switch (type)
	{
	case types::nil:
		return new nil_entry();
	case types::uint:
		switch (size_idx)
		{
		case size_idx::one_byte:
			return new numeric_entry<uint8_t>(buf);
		case size_idx::two_bytes:
			return new numeric_entry<uint16_t>(buf);
		case size_idx::four_bytes:
			return new numeric_entry<uint32_t>(buf);
		case size_idx::eight_bytes:
			return new numeric_entry<uint64_t>(buf);
		case size_idx::sixteen_bytes:
			return new numeric_entry<uint128_t>(buf);
		default:
			throw std::runtime_error("Incorrect size_idx for uint");
		}
		break;
	case types::sint:
		switch (size_idx)
		{
		case size_idx::one_byte:
			return new numeric_entry<int8_t>(buf);
		case size_idx::two_bytes:
			return new numeric_entry<int16_t>(buf);
		case size_idx::four_bytes:
			return new numeric_entry<int32_t>(buf);
		case size_idx::eight_bytes:
			return new numeric_entry<int64_t>(buf);
		case size_idx::sixteen_bytes:
			return new numeric_entry<int128_t>(buf);
		default:
			throw std::runtime_error("Incorrect size_idx for sint");
		}
		break;
	case types::uuid:
		switch (size_idx)
		{
		case size_idx::two_bytes:
			return new uuid16(buf);
		case size_idx::four_bytes:
			return new uuid32(buf);
		case size_idx::sixteen_bytes:
			return new uuid128(buf);
		default:
			throw std::runtime_error("Incorrect size_idx for uuid");
		}
		break;
	case types::string:
	{
		size_t size_field;

		// Extract the size field if one is provided
		switch (size_idx)
		{
		case size_idx::one_byte_len:
			size_field = extract_type<uint8_t>(buf);
			return new string(buf, size_field, size_idx);
		case size_idx::two_byte_len:
			size_field = extract_type<uint16_t>(buf);
			return new string(buf, size_field, size_idx);
		case size_idx::four_byte_len:
			size_field = extract_type<uint32_t>(buf);
			return new string(buf, size_field, size_idx);
		default:
			throw std::runtime_error("Incorrect size_idx for string");
		}
		break;
	}
	case types::boolean:
		if (size_idx == size_idx::one_byte)
			return new boolean(buf);
		else
			throw std::runtime_error("Incorrect size_idx for boolean");
		break;
	case types::seq:
	{
		size_t size_field;

		// Extract the size field if one is provided
		switch (size_idx)
		{
		case size_idx::one_byte_len:
			size_field = extract_type<uint8_t>(buf);
			return new sequence(buf, size_field, size_idx);
		case size_idx::two_byte_len:
			size_field = extract_type<uint16_t>(buf);
			return new sequence(buf, size_field, size_idx);
		case size_idx::four_byte_len:
			size_field = extract_type<uint32_t>(buf);
			return new sequence(buf, size_field, size_idx);
		default:
			throw std::runtime_error("Incorrect size_idx for seq");
		}
		break;
	}
	case types::seq_select:
	{
		size_t size_field;

		// Extract the size field if one is provided
		switch (size_idx)
		{
		case size_idx::one_byte_len:
			size_field = extract_type<uint8_t>(buf);
			return new sequence_select(buf, size_field, size_idx);
		case size_idx::two_byte_len:
			size_field = extract_type<uint16_t>(buf);
			return new sequence_select(buf, size_field, size_idx);
		case size_idx::four_byte_len:
			size_field = extract_type<uint32_t>(buf);
			return new sequence_select(buf, size_field, size_idx);
		default:
			throw std::runtime_error("Incorrect size_idx for seq");
		}
		break;
	}
	case types::url:
	{
		size_t size_field;

		// Extract the size field if one is provided
		switch (size_idx)
		{
		case size_idx::one_byte_len:
			size_field = extract_type<uint8_t>(buf);
			return new url(buf, size_field, size_idx);
		case size_idx::two_byte_len:
			size_field = extract_type<uint16_t>(buf);
			return new url(buf, size_field, size_idx);
		case size_idx::four_byte_len:
			size_field = extract_type<uint32_t>(buf);
			return new url(buf, size_field, size_idx);
		default:
			throw std::runtime_error("Incorrect size_idx for string");
		}
		break;
	}
	default:
		throw std::range_error("Incorrect SDP attribute type");
	}

	throw std::runtime_error("Malformed or unsupported SDP attribute");
}

attr::attr(const blue::sdp::sdp_attr &a)
{
	if (a.vlen > a.value.size())
		throw std::runtime_error("Corrupt SDP attribute: vlen is too large.");

	buf.write(reinterpret_cast<const char *>(a.value.data()), a.vlen);

	e = parse_attr(buf);
	if (!e)
		throw std::runtime_error("Failed to parse attribute");
}

attr::~attr()
{
	delete e;
}

attr::attr(attr &&other)
{
	e = nullptr;
	*this = std::move(other);
}

attr &attr::operator=(attr &&other)
{
	std::swap(e, other.e);

	return *this;
}

const entry &attr::get_entry() const
{
	// e should always be a valid pointer
	assert(e);
	return *e;
}

entry::entry()
{
}

std::string entry::str(uint8_t width) const
{
	std::stringstream ss;

	ss << std::string(width, ' ') << "type = "
	   << static_cast<uint32_t>(get_type() >> 3)
	   << ", size_idx = " << static_cast<uint32_t>(get_size_idx())
	   << std::endl;

	return ss.str();
}

std::vector<uint8_t> entry::bin() const
{
	throw std::runtime_error("entry type does not have a binary representation");
}

nil_entry::nil_entry()
{
}

uint8_t nil_entry::get_type() const
{
	return parser::types::nil;
}

uint8_t nil_entry::get_size_idx() const
{
	return parser::size_idx::one_byte;
}

uuid16::uuid16(std::stringstream &buf) :
	numeric_entry<uint16_t>(buf)
{
}

uint8_t uuid16::get_type() const
{
	return parser::types::uuid;
}

uuid32::uuid32(std::stringstream &buf) :
	numeric_entry<uint32_t>(buf)
{
}

uint8_t uuid32::get_type() const
{
	return parser::types::uuid;
}

uuid128::uuid128(std::stringstream &buf) :
	numeric_entry<uint128_t>(buf)
{
}

uint8_t uuid128::get_type() const
{
	return parser::types::uuid;
}

string::string(std::stringstream &buf, size_t len, uint8_t size_idx) :
	size_idx(size_idx)
{
	// 10 MiB should be enough
	if (len > 10 * 1024 * 1024)
		throw std::runtime_error("String length is > 10 MiB");

	str.resize(len, '\0');
	buf.read(str.data(), len);
	if (buf.fail() && buf.gcount() != len)
		throw std::runtime_error("Malformed SDP string field");
}

uint8_t string::get_type() const
{
	return parser::types::string;
}

uint8_t string::get_size_idx() const
{
	return size_idx;
}

std::vector<uint8_t> string::bin() const
{
	std::vector<uint8_t> ret;

	std::copy(str.begin(), str.end(), std::back_inserter(ret));

	return ret;
}

const std::string &string::get_string() const
{
	return str;
}

boolean::boolean(std::stringstream &buf)
{
	char tmp;

	buf.read(&tmp, 1);
	if (buf.fail() && buf.gcount() != 1)
		throw std::runtime_error("Malformed SDP boolean field");

	// If the value is not zero, it is true as per the compatability
	// requirement in the Bluetooth Core Specification v5.2, section
	// 3.2.
	value = (tmp != 0);
}

uint8_t boolean::get_type() const
{
	return parser::types::boolean;
}

uint8_t boolean::get_size_idx() const
{
	return parser::size_idx::one_byte;
}

std::vector<uint8_t> boolean::bin() const
{
	std::vector<uint8_t> ret;

	if (value)
		ret.push_back(1);
	else
		ret.push_back(0);

	return ret;
}

bool boolean::get_value() const
{
	return value;
}

sequence::sequence(std::stringstream &buf, size_t len, uint8_t size_idx) :
	size_idx(size_idx)
{
	if (len == 0)
		return;

	try
	{
		auto start = buf.tellg();
		while (!buf.eof() && (buf.tellg() - start) < len)
		{
			entries.push_back(parse_attr(buf));
		}
	}
	catch (const std::runtime_error &e)
	{
		for (const entry *e : entries)
			delete e;
		throw e;
	}
}

sequence::~sequence()
{
	for (const entry *e : entries)
		delete e;
	entries.clear();
}

sequence::sequence(sequence &&other)
{
	*this = std::move(other);
}

sequence &sequence::operator=(sequence &&other)
{
	std::swap(entries, other.entries);

	size_idx = other.size_idx;

	return *this;
}

uint8_t sequence::get_type() const
{
	return parser::types::seq;
}

uint8_t sequence::get_size_idx() const
{
	return size_idx;
}

std::vector<uint8_t> sequence::bin() const
{
	std::vector<uint8_t> ret;

	for (const entry *e : entries)
	{
		std::vector<uint8_t> entry_bin = e->bin();
		std::copy(entry_bin.begin(), entry_bin.end(), std::back_inserter(ret));
	}

	return ret;
}

std::string sequence::str(uint8_t width) const
{
	std::stringstream ss;

	ss << std::string(width, ' ') << "elements = " << entries.size() << ", "
	   << entry::str();
	for (const entry *e : entries)
		ss << e->str(width + 4);

	return ss.str();
}

const std::vector<const entry *> &sequence::get_sequence() const
{
	return entries;
}

sequence_select::sequence_select(std::stringstream &buf, size_t len,
		uint8_t size_idx) :
	sequence(buf, len, size_idx)
{
}

uint8_t sequence_select::get_type() const
{
	return parser::types::seq_select;
}

url::url(std::stringstream &buf, size_t len, uint8_t size_idx) :
	string(buf, len, size_idx)
{
}

uint8_t url::get_type() const
{
	return parser::types::url;
}

const std::string &url::get_url() const
{
	return get_string();
}

} // namespace blue::sdp::parser
