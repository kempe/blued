#include "cap_kbd.h"

#include <sys/fcntl.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <errno.h>
#include <syslog.h>
#include <stdlib.h>

extern "C"
{
#include <sys/nv.h>
#include <libcasper.h>
#include <libcasper_service.h>
} // extern "C"

#include <string>
#include <vector>

#include "util/unique_fd.h"

namespace blue::kbd
{

// Stores the names of the obtained vkbdctl devices to allow for reuse
// and avoiding accumulating more and more of them.
static std::vector<std::string> vkbdctl = { "vkbdctl0" };

util::unique_fd cap_open_vkbdctl(util::shared_cap_chan chan)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "open_vkbdctl");

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return util::make_unique_fd(-1);
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return util::make_unique_fd(-1);
	}

	int fd = dup(nvlist_get_descriptor(nvl, "fd"));

	nvlist_destroy(nvl);

	return util::make_unique_fd(fd);
}

static int open_vkbd(std::string dev)
{
	int fd = -1;

	if ((fd = open(dev.c_str(), O_RDWR)) == -1)
		return -1;

	return fd;
}

static int try_reuse_vkbd()
{
	int fd = -1;

	for (const std::string &d : vkbdctl)
	{
		if ((fd = open_vkbd("/dev/" + d)) == -1)
		{
			if (errno != EBUSY && errno != ENOENT)
				break;
		}
	}

	return fd;
}

static int impl_open_vkbdctl(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int fd = -1;

	// Try to reuse previous devices since opening /dev/vkbdctl spawns
	// a new device every time.
	if ((fd = try_reuse_vkbd()) == -1)
	{
		if (errno == EBUSY || errno == ENOENT)
		{
			// Could not open a previously used device, open a new one
			// and save the name.
			fd = open_vkbd("/dev/vkbdctl");
			if (fd != -1)
			{
				const char *dev_name = fdevname(fd);
				if (dev_name)
					vkbdctl.emplace_back(dev_name);
			}
		}
	}
	if (fd == -1)
		return errno;


	nvlist_add_descriptor(nvlout, "fd", fd);

	close(fd);

	return 0;
}

util::unique_fd cap_open_uinput(util::shared_cap_chan chan)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "open_uinput");

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return util::make_unique_fd(-1);
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return util::make_unique_fd(-1);
	}

	int fd = dup(nvlist_get_descriptor(nvl, "fd"));

	nvlist_destroy(nvl);

	return util::make_unique_fd(fd);
}

static int impl_open_uinput(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int fd = -1;

	if ((fd = open("/dev/uinput", O_RDWR | O_NONBLOCK)) == -1)
		return errno;

	nvlist_add_descriptor(nvlout, "fd", fd);

	close(fd);

	return 0;
}

int cap_get_rcpt_mask(util::shared_cap_chan chan)
{
	nvlist_t *nvl = nvlist_create(0);

	nvlist_add_string(nvl, "cmd", "get_rcpt_mask");

	nvl = cap_xfer_nvlist(chan.get(), nvl);
	if (nvl == nullptr)
	{
		errno = ENOMEM;
		return -1;
	}
	errno = static_cast<int>(nvlist_get_number(nvl, "error"));
	if (errno != 0)
	{
		nvlist_destroy(nvl);
		return -1;
	}

	int32_t mask = static_cast<int32_t>(nvlist_get_number(nvl, "mask"));

	nvlist_destroy(nvl);

	return mask;
}

static int impl_get_rcpt_mask(const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int32_t mask = 0;
	size_t len = sizeof(mask);

	if (sysctlbyname("kern.evdev.rcpt_mask", &mask, &len, NULL, 0) < 0)
		return errno;

	nvlist_add_number(nvlout, "mask", mask);

	return 0;
}

static int kbd_command(const char *cmd, const nvlist_t *limits,
		nvlist_t *nvlin, nvlist_t *nvlout)
{
	int error = EINVAL;
	const std::string cmd_str(cmd);

	if (cmd_str == "open_vkbdctl")
		error = impl_open_vkbdctl(limits, nvlin, nvlout);
	else if (cmd_str == "open_uinput")
		error = impl_open_uinput(limits, nvlin, nvlout);
	else if (cmd_str == "get_rcpt_mask")
		error = impl_get_rcpt_mask(limits, nvlin, nvlout);

	if (error)
		syslog(LOG_ERR, "cap_kbd: error: %s", strerror(errno));

	return error;
}

static int kbd_limit(const nvlist_t *oldlimits, const nvlist_t *newlimits)
{
	return 0;
}

CREATE_SERVICE("blue.kbd", kbd_limit, kbd_command, 0);

} // namespace blue::kbd
