#include "util/shared_cap_chan.h"

namespace blue::util
{

struct cap_chan_deleter
{
	void operator()(cap_channel_t *chan)
	{
		if (chan)
			cap_close(chan);
	}
};

shared_cap_chan init_cap_chan()
{
	return shared_cap_chan(cap_init(), cap_chan_deleter());
}

shared_cap_chan make_cap_chan(shared_cap_chan chan, const char *service)
{
	return shared_cap_chan(cap_service_open(chan.get(), service),
	                       cap_chan_deleter());
}

}; // namespace blue::util
