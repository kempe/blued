#include "util/unique_fd.h"

#include <utility>

namespace blue::util
{

unique_fd::unique_fd() :
	fd(-1)
{
}

unique_fd::unique_fd(int fd) :
	fd(fd)
{
}

unique_fd::~unique_fd()
{
	if (fd != -1)
		close(fd);
}

unique_fd::unique_fd(unique_fd &&other)
{
	fd = -1;
	*this = std::move(other);
}

unique_fd& unique_fd::operator=(unique_fd &&other)
{
	std::swap(fd, other.fd);

	return *this;
}

unique_fd::operator int() const
{
	return fd;
}

unique_fd make_unique_fd(int fd)
{
	return unique_fd(fd);
}

} // namespace blue::util
