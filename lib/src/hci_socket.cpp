#include "hci_socket.h"

#include <bluetooth.h>
#include <netgraph/ng_message.h>
#include <netgraph/bluetooth/include/ng_hci.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <bitstring.h>
#include <syslog.h>

#include <exception>
#include <sstream>
#include <vector>

#include "bdaddr.h"
#include "hci_parser.h"

#define RAW_EVENT(event, status) \
	emit_socket_event<&hci_socket_observer::hci_event>((event), (status))

/*
 * Turn a link cotrol request into an opcode.
 */
#define LINK_CONTROL_OP(code) NG_HCI_OPCODE(NG_HCI_OGF_LINK_CONTROL, code)

namespace blue
{

constexpr uint8_t MAX_NODE_NUM = 16;

hci_socket::hci_error::hci_error(uint16_t opcode, uint8_t status) :
	std::exception()
{
	std::stringstream ss;

	ss << "HCI command with opcode 0x" << std::hex
	   << static_cast<int>(opcode) << " failed with error 0x"
	   << static_cast<int>(status);
	msg = ss.str();
}

const char *hci_socket::hci_error::what() const noexcept
{
	return msg.c_str();
}

hci_socket::hci_socket(ev::loop_ref loop) :
	fd(socket(PF_BLUETOOTH, SOCK_RAW, BLUETOOTH_PROTO_HCI)),
	cmd_count(1) // assume at least one command can be sent at start-up
{
	if (fd < 0)
		throw std::runtime_error("Could not open HCI socket: " +
				std::string(strerror(errno)));

	set_event_filter({
		NG_HCI_EVENT_COMMAND_COMPL,
		NG_HCI_EVENT_COMMAND_STATUS,
		NG_HCI_EVENT_INQUIRY_COMPL,
		NG_HCI_EVENT_INQUIRY_RESULT,
		NG_HCI_EVENT_CON_COMPL,
		NG_HCI_EVENT_DISCON_COMPL,
		NG_HCI_EVENT_LINK_KEY_REQ,
		NG_HCI_EVENT_LINK_KEY_NOTIFICATION,
		NG_HCI_EVENT_IO_CAPABILITY_REQUEST,
		NG_HCI_EVENT_USER_CONFIRMATION_REQUEST,
		NG_HCI_EVENT_AUTH_COMPL,
		NG_HCI_EVENT_PIN_CODE_REQ,
		NG_HCI_EVENT_REMOTE_NAME_REQ_COMPL,
		NG_HCI_EVENT_PAGE_SCAN_MODE_CHANGE,
		NG_HCI_EVENT_PAGE_SCAN_REP_MODE_CHANGE,
		NG_HCI_EVENT_ENCRYPTION_CHANGE,
	});
	unmask_events();
	enable_simple_pairing();

	hci_watcher.set<hci_socket, &hci_socket::io_callback>(this);
	hci_watcher.set(fd, ev::READ);
	hci_watcher.start();

}

void hci_socket::bind(const std::string &node)
{
	sockaddr_hci addr{};

	addr.hci_len = sizeof(addr);
	addr.hci_family = AF_BLUETOOTH;
	strncpy(addr.hci_node, node.c_str(), sizeof(addr.hci_node));
	if (::bind(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) < 0)
		throw std::runtime_error("Could not bind HCI socket " + node + ": " +
				std::string(strerror(errno)));
	syslog(LOG_DEBUG, "Bound HCI node: %s", addr.hci_node);
}

void hci_socket::connect(const std::string &node)
{
	sockaddr_hci addr{};

	addr.hci_len = sizeof(addr);
	addr.hci_family = AF_BLUETOOTH;
	strncpy(addr.hci_node, node.c_str(), sizeof(addr.hci_node));
	if (::connect(fd, reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) < 0)
		throw std::runtime_error("Could not connect HCI socket to " + node +
				": " + std::string(strerror(errno)));
	this->node = node;
}

std::vector<nodeinfo> hci_socket::find_hci_nodes()
{
	std::vector<nodeinfo> nodes(MAX_NODE_NUM);
	ng_btsocket_hci_raw_node_list_names r;

	r.num_names = MAX_NODE_NUM;
	r.names = nodes.data();

	if (ioctl(fd, SIOC_HCI_RAW_NODE_LIST_NAMES, &r, sizeof(r)) < 0)
		throw std::runtime_error("Could not get HCI node list: " +
				std::string(strerror(errno)));

	nodes.resize(r.num_names);

	return nodes;
}

bdaddr hci_socket::get_address() const
{
	ng_btsocket_hci_raw_node_bdaddr addr;
	if (ioctl(fd, SIOC_HCI_RAW_NODE_GET_BDADDR, &addr, sizeof(addr)) < 0)
		throw std::runtime_error("Could not get HCI address: " +
		                         std::string(strerror(errno)));

	return addr.bdaddr;
}

void hci_socket::set_event_filter(const std::vector<int> &filters)
{
	ng_btsocket_hci_raw_filter filter{};

	for (int bit : filters)
		bit_set(filter.event_mask, bit - 1);

	if (setsockopt(fd, SOL_HCI_RAW, SO_HCI_RAW_FILTER,
				reinterpret_cast<void * const>(&filter), sizeof(filter)) < 0)
		throw std::runtime_error("Could not set HCI socket filter: " +
				std::string(strerror(errno)));
}

void hci_socket::unmask_events()
{
	ng_hci_set_event_mask_cp cp;

	std::fill(cp.event_mask, cp.event_mask + NG_HCI_EVENT_MASK_SIZE, 0xFF);

	hci_cmd(NG_HCI_OPCODE(NG_HCI_OGF_HC_BASEBAND, NG_HCI_OCF_SET_EVENT_MASK),
			cp);
}

void hci_socket::enable_simple_pairing()
{
	ng_hci_write_simple_pairing_cp cp;

	cp.simple_pairing = 1;

	hci_cmd(
		NG_HCI_OPCODE(NG_HCI_OGF_HC_BASEBAND, NG_HCI_OCF_WRITE_SIMPLE_PAIRING),
		cp);
}

void hci_socket::register_socket_observer(hci_socket_observer *observer)
{
	socket_observer = observer;
}

void hci_socket::register_observer(hci_observer *observer)
{
	observers[observer->get_addr()] = observer;
}

void hci_socket::clear_observer_requests(const hci_observer *observer)
{
	// Loop through all queues and build new queues where observer is
	// removed.
	for (auto &[code, observers] : running_reqs)
	{
		std::deque<hci_observer *> tmp;
		while (!observers.empty())
		{
			/*
			 * If the current observer is not the one we're removing,
			 * move it to the new temporary queue.
			 * If the current observer is the one we're removing,
			 * we push a nullptr in the new queue. We need to do this
			 * to avoid modifying the order in the queue which could
			 * cause observers to get a report of the status for a
			 * command it didn't issue.
			 */
			if (observers.back() != observer)
				tmp.push_back(observers.back());
			else
				tmp.push_back(nullptr);
			observers.pop_back();
		}
		// Overwrite the existing queue with our new one.
		running_reqs[code] = std::move(tmp);
	}
}

void hci_socket::unregister_observer(hci_observer *observer)
{
	observers.erase(observer->get_addr());
	clear_observer_requests(observer);
}

const std::vector<device_info> &hci_socket::get_devices()
{
	return devices;
}

void hci_socket::query_devices()
{
	ng_hci_inquiry_cp cp;

	devices.clear();

	// 0x9E8B33 - General/Unlimited Inquiry Access Code (GIAC)
	// See https://lost-contact.mit.edu/afs/nada.kth.se/misc/cas/documentation/bluetooth/bluetooth_app_c8.pdf
	cp.lap[2] = 0x9e;
	cp.lap[1] = 0x8b;
	cp.lap[0] = 0x33;
	cp.inquiry_length = 5; // 5 * 1.28 s = 6.4 s
	cp.num_responses = 255;  // max 255 responses

	send(NG_HCI_OPCODE(NG_HCI_OGF_LINK_CONTROL, NG_HCI_OCF_INQUIRY), cp);
}

void hci_socket::connect_device(hci_observer *o, bdaddr addr)
{
	ng_hci_create_con_cp cp;

	cp.pkt_type = htole16(NG_HCI_PKT_DM1 | NG_HCI_PKT_DH1 |
				NG_HCI_PKT_DM3 | NG_HCI_PKT_DH3 |
				NG_HCI_PKT_DM5);
	cp.page_scan_rep_mode = NG_HCI_SCAN_REP_MODE0;
	cp.page_scan_mode = NG_HCI_MANDATORY_PAGE_SCAN_MODE;
	cp.clock_offset = 0;
	cp.accept_role_switch = 1;
	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);

	send(LINK_CONTROL_OP(NG_HCI_OCF_CREATE_CON), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_OCF_CREATE_CON));
}

void hci_socket::request_name(hci_observer *o, bdaddr addr,
		uint8_t page_scan_mode, uint8_t page_scan_rep_mode,
		uint16_t clock_offset)
{
	ng_hci_remote_name_req_cp cp;

	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);
	cp.page_scan_mode = page_scan_mode;
	cp.page_scan_rep_mode = page_scan_rep_mode;
	cp.clock_offset = htole16(clock_offset);

	send(LINK_CONTROL_OP(NG_HCI_OCF_REMOTE_NAME_REQ), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_OCF_REMOTE_NAME_REQ));
}

void hci_socket::io_capability_request_reply(hci_observer *o, bdaddr addr,
		uint8_t io_capability, uint8_t oob_data_present,
		uint8_t authentication_requirements)
{
	ng_hci_io_capability_request_reply_cp cp;

	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);
	cp.io_capability = io_capability;
	cp.oob_data_present = oob_data_present;
	cp.authentication_requirements = authentication_requirements;

	send(LINK_CONTROL_OP(NG_HCI_IO_CAPABILITY_REQUEST_REPLY), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_IO_CAPABILITY_REQUEST_REPLY));
}

void hci_socket::user_confirmation_request_reply(hci_observer *o, bdaddr addr)
{
	ng_hci_user_confirmation_request_reply_cp cp;

	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);

	send(LINK_CONTROL_OP(NG_HCI_USER_CONFIRMATION_REQUEST_REPLY), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_USER_CONFIRMATION_REQUEST_REPLY));
}

void hci_socket::disconnect_device(hci_observer *o, uint16_t con_handle)
{
	ng_hci_discon_cp cp;
	cp.con_handle = htole16(con_handle);
	cp.reason = 0x13; // 0x13 = user terminated connection

	send(LINK_CONTROL_OP(NG_HCI_OCF_DISCON), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_OCF_DISCON));
}

void hci_socket::send_auth_request(hci_observer *o, uint16_t con_handle)
{
	ng_hci_auth_req_cp cp;

	cp.con_handle = con_handle;

	send(LINK_CONTROL_OP(NG_HCI_OCF_AUTH_REQ), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_OCF_AUTH_REQ));
}

void hci_socket::set_encryption(hci_observer *o, uint16_t con_handle,
                                bool enable)
{
	ng_hci_set_con_encryption_cp cp;

	cp.con_handle = htole16(con_handle);
	cp.encryption_enable = enable ? 0x01 : 0x00;

	send(LINK_CONTROL_OP(NG_HCI_OCF_SET_CON_ENCRYPTION), cp);
	push_running(o, LINK_CONTROL_OP(NG_HCI_OCF_SET_CON_ENCRYPTION));
}

void hci_socket::send_negative_link_key_reply(hci_observer *o, bdaddr addr)
{
	ng_hci_link_key_neg_rep_cp cp;

	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);

	send(NG_HCI_OPCODE(NG_HCI_OGF_LINK_CONTROL,
	                   NG_HCI_OCF_LINK_KEY_NEG_REP), cp);
}

void hci_socket::send_link_key_reply(hci_observer *o, bdaddr addr, key link_key)
{
	ng_hci_link_key_rep_cp cp;

	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);
	std::copy(link_key.begin(), link_key.end(), cp.key);

	send(NG_HCI_OPCODE(NG_HCI_OGF_LINK_CONTROL,
	                   NG_HCI_OCF_LINK_KEY_REP), cp);
}

void hci_socket::send_pin_code_reply(hci_observer *o, bdaddr addr,
		std::string pin)
{
	ng_hci_pin_code_rep_cp cp;

	if (pin.size() > NG_HCI_PIN_SIZE)
	{
		std::stringstream ss;

		pin = "0000";

		ss << "Device PIN code is longer than allowed (>"
		   << NG_HCI_PIN_SIZE
		   <<   ") for device " << addr
		   << ". Code set to default 0000.";
		throw std::runtime_error(ss.str());
	}

	std::copy(addr.cbegin(), addr.cend(), cp.bdaddr.b);
	std::copy(pin.begin(), pin.end(), cp.pin);
	cp.pin_size = pin.length();

	send(NG_HCI_OPCODE(NG_HCI_OGF_LINK_CONTROL,
	                   NG_HCI_OCF_PIN_CODE_REP), cp);
}

static void notify_observer_error(hci_observer *o, hci_event_code code)
{
	switch (code)
	{
	case LINK_CONTROL_OP(NG_HCI_OCF_CREATE_CON):
		o->con_compl(false, INVALID_HANDLE);
		break;
	case LINK_CONTROL_OP(NG_HCI_OCF_REMOTE_NAME_REQ):
		o->remote_name(false, "");
		break;
	case LINK_CONTROL_OP(NG_HCI_IO_CAPABILITY_REQUEST_REPLY):
		o->io_capability_request_reply(false);
		break;
	case LINK_CONTROL_OP(NG_HCI_USER_CONFIRMATION_REQUEST_REPLY):
		o->user_confirmation_request_reply(false);
		break;
	case LINK_CONTROL_OP(NG_HCI_OCF_DISCON):
		o->discon_compl(false);
		break;
	case LINK_CONTROL_OP(NG_HCI_OCF_AUTH_REQ):
		o->auth_compl(false);
		break;
	case LINK_CONTROL_OP(NG_HCI_OCF_SET_CON_ENCRYPTION):
		o->encryption_change(false, false);
		break;
	};
}

void hci_socket::socket_recv()
{
	try
	{
		hci_parser::hci_msg msg;

		msg = hci_parser::read_msg(fd);
		switch (msg.event)
		{
		case NG_HCI_EVENT_COMMAND_COMPL:
		{
			const ng_hci_command_compl_ep ep =
				hci_parser::consume<ng_hci_command_compl_ep>(msg);

			cmd_count = ep.num_cmd_pkts;

			syslog(LOG_DEBUG, "HCI Event Command Complete: opcode = 0x%x",
			       ep.opcode);
			switch (ep.opcode)
			{
				case LINK_CONTROL_OP(NG_HCI_OCF_LINK_KEY_NEG_REP):
				{
					const ng_hci_link_key_neg_rep_rp rp =
						hci_parser::consume<ng_hci_link_key_neg_rep_rp>(msg);

					bool success = rp.status == 0x00;
					emit_hci_event<&hci_observer::neg_link_key_reply>(rp.bdaddr,
					                                                  success);
					break;
				}
				case LINK_CONTROL_OP(NG_HCI_OCF_LINK_KEY_REP):
				{
					const ng_hci_link_key_rep_rp rp =
						hci_parser::consume<ng_hci_link_key_rep_rp>(msg);

					bool success = rp.status == 0x00;
					emit_hci_event<&hci_observer::link_key_reply>(rp.bdaddr,
					                                              success);
					break;
				}
				case LINK_CONTROL_OP(NG_HCI_OCF_PIN_CODE_REP):
				{
					const ng_hci_pin_code_rep_rp rp =
						hci_parser::consume<ng_hci_pin_code_rep_rp>(msg);

					bool success = rp.status == 0x00;
					emit_hci_event<&hci_observer::pin_code_reply>(rp.bdaddr,
					                                              success);
					break;
				}
			}

			flush_queue();

			break;
		}
		case NG_HCI_EVENT_COMMAND_STATUS:
		{
			const ng_hci_command_status_ep ep =
				hci_parser::consume<ng_hci_command_status_ep>(msg);

			uint16_t host_opcode = le16toh(ep.opcode);
			try
			{
				if (host_opcode == LINK_CONTROL_OP(NG_HCI_OCF_ACCEPT_CON))
				{
					if (ep.status !=0)
						syslog(LOG_ERR, "Failed accepting HCI connection.");
				}
				else if (host_opcode != LINK_CONTROL_OP(NOOP_EVENT) &&
				         host_opcode != LINK_CONTROL_OP(NG_HCI_OCF_INQUIRY))
				{
					// Extracts the HCI command from the opcode.
					hci_observer *o = pop_running(host_opcode);

					/*
					 * Only notify on error. If the command is being
					 * performed, the command specific completion message
					 * will call the callback.
					 * If o is nullptr, the observer disappeared
					 * before it could be notified of the command's
					 * status.
					 */
					if (o && ep.status != 0)
					{
						syslog(LOG_DEBUG, "HCI command %x failed with error: %x",
							   host_opcode, ep.status);
						notify_observer_error(o, host_opcode);
					}
				}
			}
			catch (const std::runtime_error &e)
			{
				syslog(LOG_WARNING, "blued might not work correctly: %s",
					   e.what());
			}

			cmd_count = ep.num_cmd_pkts;

			flush_queue();

			break;
		}
		case NG_HCI_EVENT_INQUIRY_RESULT:
		{
			const ng_hci_inquiry_result_ep ep =
				hci_parser::consume<ng_hci_inquiry_result_ep>(msg);
			for (int i = 0; i < ep.num_responses; i++)
			{
				const ng_hci_inquiry_response &r =
					hci_parser::consume<ng_hci_inquiry_response>(msg);

				device_info info = {
					r.bdaddr,
					r.page_scan_rep_mode,
					r.page_scan_period_mode,
					r.page_scan_mode,
					le16toh(r.clock_offset),
				};
				devices.push_back(info);
			}

			break;
		}
		case NG_HCI_EVENT_INQUIRY_COMPL:
		{
			const ng_hci_inquiry_compl_ep ep =
				hci_parser::consume<ng_hci_inquiry_compl_ep>(msg);
			RAW_EVENT(NG_HCI_EVENT_INQUIRY_COMPL, ep.status);
			emit_socket_event<&hci_socket_observer::query_complete>(!ep.status,
			                                                 devices);
			break;
		}
		case NG_HCI_EVENT_CON_COMPL:
		{
			const ng_hci_con_compl_ep ep =
				hci_parser::consume<ng_hci_con_compl_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_CON_COMPL, ep.status);
			bool success = ep.status == 0x00;
			emit_hci_event<&hci_observer::con_compl>(ep.bdaddr, success,
			                                         ep.con_handle);

			break;
		}
		case NG_HCI_EVENT_DISCON_COMPL:
		{
			const ng_hci_discon_compl_ep ep =
				hci_parser::consume<ng_hci_discon_compl_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_DISCON_COMPL, ep.status);
			bool success = ep.status == 0x00;
			emit_hci_event<&hci_observer::discon_compl>(ep.con_handle, success);

			break;
		}
		case NG_HCI_EVENT_LINK_KEY_REQ:
		{
			const ng_hci_link_key_req_ep ep =
				hci_parser::consume<ng_hci_link_key_req_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_LINK_KEY_REQ, 0);
			emit_hci_event<&hci_observer::link_key_req>(ep.bdaddr);

			break;
		}
		case NG_HCI_EVENT_LINK_KEY_NOTIFICATION:
		{
			const ng_hci_link_key_notification_ep ep =
				hci_parser::consume<ng_hci_link_key_notification_ep>(msg);

			key link_key;
			std::copy(ep.key, ep.key + NG_HCI_KEY_SIZE, link_key.data());

			RAW_EVENT(NG_HCI_EVENT_LINK_KEY_NOTIFICATION, 0x00);
			emit_hci_event<&hci_observer::link_key_notification>(ep.bdaddr,
			                                                     link_key);
			break;
		}
		case NG_HCI_EVENT_IO_CAPABILITY_REQUEST:
		{
			const ng_hci_io_capability_request_ep ep =
				hci_parser::consume<ng_hci_io_capability_request_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_IO_CAPABILITY_REQUEST, 0x00);
			emit_hci_event<&hci_observer::io_capability_request>(ep.bdaddr);

			break;
		}
		case NG_HCI_EVENT_USER_CONFIRMATION_REQUEST:
		{
			const ng_hci_user_confirmation_request_ep ep =
				hci_parser::consume<ng_hci_user_confirmation_request_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_USER_CONFIRMATION_REQUEST, 0x00);
			emit_hci_event<&hci_observer::user_confirmation_request>(ep.bdaddr,
				ep.numeric_value);

			break;
		}
		case NG_HCI_EVENT_AUTH_COMPL:
		{
			const ng_hci_auth_compl_ep ep =
				hci_parser::consume<ng_hci_auth_compl_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_USER_CONFIRMATION_REQUEST, ep.status);
			bool success = ep.status == 0x00;
			emit_hci_event<&hci_observer::auth_compl>(ep.con_handle, success);

			break;
		}
		case NG_HCI_EVENT_PIN_CODE_REQ:
		{
			const ng_hci_pin_code_req_ep ep =
				hci_parser::consume<ng_hci_pin_code_req_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_PIN_CODE_REQ, 0x00);
			emit_hci_event<&hci_observer::pin_code_req>(ep.bdaddr);

			break;
		}
		case NG_HCI_EVENT_REMOTE_NAME_REQ_COMPL:
		{
			const ng_hci_remote_name_req_compl_ep ep =
				hci_parser::consume<ng_hci_remote_name_req_compl_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_REMOTE_NAME_REQ_COMPL, ep.status);
			bool success = ep.status == 0x00;
			std::string name;
			size_t len = strnlen(ep.name, sizeof(ep.name));
			std::copy(ep.name, ep.name + len, std::back_inserter(name));
			emit_hci_event<&hci_observer::remote_name>(ep.bdaddr, success,
			                                           name);

			break;
		}
		case NG_HCI_EVENT_PAGE_SCAN_MODE_CHANGE:
		{
			const ng_hci_page_scan_mode_change_ep ep =
				hci_parser::consume<ng_hci_page_scan_mode_change_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_PAGE_SCAN_MODE_CHANGE, 0x00);
			emit_hci_event<&hci_observer::page_scan_mode_change>(ep.bdaddr,
			                                                 ep.page_scan_mode);

			break;
		}
		case NG_HCI_EVENT_PAGE_SCAN_REP_MODE_CHANGE:
		{
			const ng_hci_page_scan_rep_mode_change_ep ep =
				hci_parser::consume<ng_hci_page_scan_rep_mode_change_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_PAGE_SCAN_REP_MODE_CHANGE, 0x00);
			emit_hci_event<&hci_observer::page_scan_rep_mode_change>(ep.bdaddr,
			                                             ep.page_scan_rep_mode);
			break;
		}
		case NG_HCI_EVENT_ENCRYPTION_CHANGE:
		{
			const ng_hci_encryption_change_ep ep =
				hci_parser::consume<ng_hci_encryption_change_ep>(msg);

			RAW_EVENT(NG_HCI_EVENT_ENCRYPTION_CHANGE, ep.status);
			bool success = ep.status == 0x00;
			bool enabled = ep.encryption_enable == 0x01;
			emit_hci_event<&hci_observer::encryption_change>(ep.con_handle,
			                                                 success, enabled);
			break;
		}
		}
	}
	catch (const std::runtime_error &e)
	{
		syslog(LOG_ERR, "Malformed HCI data received: %s", e.what());
	}
}

bool hci_socket::dec_cmd_count()
{
	if (cmd_count >  0)
	{
		cmd_count--;
		return true;
	}
	else
		return false;
}

void hci_socket::flush_queue()
{
	while (!pkt_queue.empty())
	{
		// If dec_cmd_count() is false, we had more packets to flush
		// than we were allowed to flush.
		if (!dec_cmd_count())
		{
			syslog(LOG_DEBUG, "%lu packets still delayed", pkt_queue.size());
			break;
		}

		hci_pkt &p = pkt_queue.front();

		syslog(LOG_DEBUG, "Flushing HCI command, opcode = %x, %u slots left",
		       p.opcode, cmd_count);

		void *d_ptr = reinterpret_cast<void *>(p.data.data());
		if (bt_devsend(fd, htole16(p.opcode), d_ptr, p.data.size()) < 0)
			throw std::runtime_error("Failed sending queued HCI request: " +
					std::string(strerror(errno)));
		pkt_queue.pop();
	}
}

void hci_socket::io_callback(ev::io &/*watcher*/, int events)
{
	if (events & EV_READ)
		socket_recv();
	else
		syslog(LOG_DEBUG, "Got unhandled event from libev: %i", events);
}

void hci_socket::push_running(hci_observer *o, hci_event_code code)
{
	running_reqs[code].push_back(o);
	syslog(LOG_DEBUG, "Waiting for HCI event for %x. Queue size: %lu",
	       code, running_reqs[code].size());
}

hci_observer *hci_socket::pop_running(hci_event_code code)
{
	std::deque<hci_observer *> &queue = running_reqs[code];

	if (queue.empty())
	{
		std::stringstream ss;
		ss << "No observers waiting for request: ";
		ss << std::hex << static_cast<int>(code);
		throw std::runtime_error(ss.str());
	}

	// The Bluetooth specification does not include any ID to map a
	// request to a status report. We assume that status reports are
	// delivered in the order the commands were executed and simply
	// pick the oldest pushed observer from our stack and assume that
	// the status report is for that command.
	hci_observer *ret = queue.front();
	queue.pop_front();

	syslog(LOG_DEBUG, "Found observer for HCI event for %x. Queue size: %lu",
	       code, running_reqs[code].size());

	return ret;
}

} // namespace blued::lib
