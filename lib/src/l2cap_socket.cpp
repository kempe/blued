#include "l2cap_socket.h"

#include <sys/socket.h>
#include <bluetooth.h>
#include <syslog.h>

#include <stdio.h>
#include <unistd.h>

#include <exception>
#include <string>

#include "cap_l2cap.h"
#include "bdaddr.h"

namespace blue::l2cap
{

l2cap_listen_socket::l2cap_listen_socket(util::shared_cap_chan chan,
		ev::loop_ref loop, l2cap_listen_observer *parent, uint16_t psm) :
	parent(parent),
	fd(blue::l2cap::cap_l2cap_listen(chan, NG_HCI_BDADDR_ANY, psm)),
	l2cap_watcher(loop)
{
	if (fd == -1)
		throw std::runtime_error("Could not listen on L2CAP socket:" +
				std::string(strerror(errno)));

	l2cap_watcher.set<l2cap_listen_socket,
	                  &l2cap_listen_socket::io_callback>(this);
	l2cap_watcher.set(fd, ev::READ);
	l2cap_watcher.start();
}

bool l2cap_listen_socket::operator==(const l2cap_listen_socket &other) const
{
	return fd == other.fd;
}

void l2cap_listen_socket::io_callback(ev::io &watcher, int events)
{
	util::unique_fd client_fd;
	struct sockaddr_l2cap l2addr{};
	socklen_t len = sizeof(l2addr);
	sockaddr *sa = reinterpret_cast<sockaddr *>(&l2addr);

	if ((client_fd = util::make_unique_fd(accept(watcher.fd, sa, &len))) == -1)
	{
		syslog(LOG_ERR, "Failed to accept L2CAP control connection");
		return;
	}

	parent->incoming_connection(*this, std::move(client_fd),
	                            blue::bdaddr(l2addr.l2cap_bdaddr));
}

} // namespace blue::l2cap
