#ifndef HCI_PARSER_H
#define HCI_PARSER_H

#include <bluetooth.h>
#include <netgraph/ng_message.h>
#include <netgraph/bluetooth/include/ng_hci.h>
#include <syslog.h>

#include <stdint.h>
#include <stdio.h>

#include <algorithm>
#include <stdexcept>
#include <string>
#include <sstream>
#include <vector>

#include "hci_socket.h"

namespace blue
{
namespace hci_parser
{
	struct hci_msg
	{
		hci_event_code event;
		std::vector<uint8_t> data;
	};

	void recv_data(int fd, std::vector<uint8_t> &buf, size_t length)
	{
		ssize_t res = 0;

		buf.resize(length);
		res = recv(fd, buf.data(), buf.size(), MSG_DONTWAIT);
		if (res == -1)
			throw std::runtime_error("recv failed on HCI socket: " +
					std::string(strerror(errno)));
		buf.resize(res);
	}

	// consume data from buffer
	template<class T>
	void consume(std::vector<uint8_t> &buf, size_t amount, T dst)
	{
		if (buf.size() < amount)
		{
			std::stringstream ss;
			ss << "HCI message larger than available data. ";
			ss << amount << " > " << buf.size();
			throw std::runtime_error(ss.str());
		}

		auto end = std::next(buf.begin(), amount);
		std::copy(buf.begin(), end, dst);
		buf.erase(buf.begin(), end);
	}

	// consume an event message from a HCI message
	template<class M>
	const M consume(hci_msg &msg)
	{
		M event;

		if (sizeof(M) > msg.data.size())
		{
			std::stringstream ss;
			ss << "Message, event = 0x" << std::hex << msg.event
			   << ", is larger than available data. " << sizeof(M)
			   << " > " << std::dec << msg.data.size();
			throw std::runtime_error(ss.str());
		}

		consume(msg.data, sizeof(M), reinterpret_cast<uint8_t *>(&event));

		return event;
	}

	hci_msg read_msg(int fd)
	{
		hci_msg new_msg;
		ng_hci_event_pkt_t event_pkt;
		std::vector<uint8_t> buf;

		/*
		 * The length field should be an uint8_t according to the BT
		 * specification, add a check to warn if the OS implementation
		 * does not conform or the specification changes.
		 */
		static_assert(sizeof(ng_hci_event_pkt_t::length) == sizeof(uint8_t),
		              "Expected ng_hci_event_t::length to be the same size as "
					  "an uint8_t, OS implementation has changed!");

		// pre-allocate the maximum packet size
		size_t max_msg_size = NG_HCI_EVENT_PKT_SIZE + sizeof(event_pkt);
		buf.reserve(max_msg_size);

		// receive the HCI packet and extract the header
		recv_data(fd, buf, max_msg_size);
		consume(buf, sizeof(event_pkt), reinterpret_cast<uint8_t *>(&event_pkt));

		syslog(LOG_DEBUG, "Got HCI event 0x%02x, length %u",
		       event_pkt.event, event_pkt.length);

		// extract the event information
		new_msg.event = event_pkt.event;
		consume(buf, event_pkt.length, std::back_inserter(new_msg.data));

		return new_msg;
	}
} // namespace hci_parser
} // namespace blue

#endif // HCI_PARSER_H
