#include "bdaddr.h"

#include <stdio.h>

#include <sstream>
#include <iomanip>

namespace blue
{
bdaddr::bdaddr() : a{}
{
}

bdaddr::bdaddr(bdaddr_t addr) :
	a{addr.b[0], addr.b[1], addr.b[2],
	  addr.b[3], addr.b[4], addr.b[5]}
{
}

bdaddr::bdaddr(const char *addr)
{
	int res;

	res = sscanf(addr, "%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx",
	             &a[5], &a[4], &a[3], &a[2], &a[1], &a[0]);
	if (res != 6)
		throw std::runtime_error("Incorrect address format");
}

bdaddr::bdaddr(const std::string &addr)
{
	*this = bdaddr(addr.c_str());
}

std::string bdaddr::str() const
{
	std::string s;

	s << *this;

	return s;
}

bdaddr::operator const bdaddr_t *()
{
	return reinterpret_cast<const bdaddr_t *>(a.data());
}

bool bdaddr::operator<(const bdaddr &other) const
{
	return a < other.a;
}

bool bdaddr::operator==(const bdaddr &other) const
{
	return a == other.a;
}

bool bdaddr::operator!=(const bdaddr &other) const
{
	return !(*this == other);
}

bdaddr::storage_type::const_iterator bdaddr::cbegin() const
{
	return a.cbegin();
}

bdaddr::storage_type::const_iterator bdaddr::cend() const
{
	return a.cend();
}

} // namespace blue

std::ostream &operator<<(std::ostream &os, const blue::bdaddr &addr)
{
	std::stringstream ss;

	ss << std::hex << std::setfill('0');
	for (int i = NG_HCI_BDADDR_SIZE - 1; i >= 1; i--)
		ss << std::setw(2) << static_cast<int>(addr.a.at(i)) << ":";
	ss << std::setw(2) << static_cast<int>(addr.a.at(0));

	os << ss.str();

	return os;
}

std::string &operator<<(std::string &s, const blue::bdaddr &addr)
{
	std::stringstream ss;

	ss << addr;
	s.append(ss.str());

	return s;
}
