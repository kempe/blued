#ifndef HCI_TYPES_H
#define HCI_TYPES_H

#include <cstdint>

namespace blue
{
	// Invalid HCI connection handle.
	constexpr uint16_t INVALID_HANDLE = 0xFFFF;

	// Type for the opcode field of a HCI event packet.
	using hci_event_code = uint16_t;

	// HCI event that only reports flow control status.
	constexpr uint16_t NOOP_EVENT = 0x00;

	// HCI link key type.
	using key = std::array<uint8_t, NG_HCI_KEY_SIZE>;

}; // namespace blue

#endif // HCI_TYPES_H
