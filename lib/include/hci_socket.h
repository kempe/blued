#ifndef HCI_SOCKET_H
#define HCI_SOCKET_H

#include <netgraph.h>
#include <netgraph/ng_message.h>
#include <netgraph/bluetooth/include/ng_hci.h>
#include <syslog.h>
#include <ev++.h>

#include <string.h>

#include <functional>
#include <map>
#include <queue>
#include <stdexcept>
#include <string>
#include <vector>

#include "bdaddr.h"
#include "hci_types.h"
#include "util/unique_fd.h"

namespace blue
{

class hci_socket;

/*
 * hci_observer represents a specific device on the network being
 * observed and describes an interface for receiving bluetooth events
 * from the remote device.
 *
 * class btdevice implements this interface to facilitate
 * communication with network devices.
 */
class hci_observer
{
public:
	/*
	 * Return the remote address of the observer.
	 *
	 * hci_socket uses the remote address to map events to specific
	 * observers.
	 */
	virtual const bdaddr &get_addr() const = 0;
	/*
	 * Return the connection handle for the observer.
	 *
	 * hci_socket uses the connection to map events to specific
	 * observers.
	 */
	virtual uint16_t get_con_handle() const = 0;

	/*
	 * A connection request has completed.
	 */
	virtual void con_compl(bool success, uint16_t handle) = 0;
	/*
	 * A disconnection request has completed.
	 */
	virtual void discon_compl(bool success) = 0;
	/*
	 * A remote device is requesting a link key for the link.
	 */
	virtual void link_key_req() = 0;
	/*
	 * A remote device is providing a new link key.
	 */
	virtual void link_key_notification(key link_key) = 0;
	/*
	 * A remote device is requesting remote capabilities of the
	 * observer.
	 */
	virtual void io_capability_request() = 0;
	/*
	 * A remote device is requesting user confirmation. Used by the
	 * secure simple pairing procedure.
	 */
	virtual void user_confirmation_request(uint32_t numeric_value) = 0;
	/*
	 * A remote device is indicating that authentication is completed.
	 */
	virtual void auth_compl(bool success) = 0;

	/*
	 * A remote device is requesting a PIN code to be sent.
	 */
	virtual void pin_code_req() = 0;

	/*
	 * A remote device has reported its name.
	 */
	virtual void remote_name(bool success, std::string name) = 0;

	/*
	 * A remote device has reported a change of page scan mode.
	 */
	virtual void page_scan_mode_change(uint8_t page_scan_mode) = 0;

	/*
	 * A remote device has reported a change of page scan rep mode.
	 */
	virtual void page_scan_rep_mode_change(uint8_t page_scan_rep_mode) = 0;
	/*
	 * An IO capability request reply was issued.
	 */
	virtual void io_capability_request_reply(bool success) = 0;

	/*
	 * A user confirmation request reply was issued.
	 */
	virtual void user_confirmation_request_reply(bool success) = 0;

	/*
	 * Encryption was enabled/disabled.
	 */
	virtual void encryption_change(bool success, bool enabled) = 0;

	/*
	 * The status of a negative link key reply.
	 */
	virtual void neg_link_key_reply(bool success) = 0;

	/*
	 * The status of a link key reply.
	 */
	virtual void link_key_reply(bool success) = 0;

	/*
	 * The status of a pin code reply.
	 */
	virtual void pin_code_reply(bool success) = 0;

protected:
	~hci_observer() = default;
};

/*
 * Device information returned by the device inquiry passed to the
 * hci_observer in query_complete().
 */
struct device_info
{
	bdaddr addr;
	uint8_t page_scan_rep_mode = NG_HCI_SCAN_REP_MODE0;
	uint8_t page_scan_period_mode;
	uint8_t page_scan_mode = NG_HCI_MANDATORY_PAGE_SCAN_MODE;
	uint16_t clock_offset;
};

/*
 * Interface for listening to HCI events.
 */
class hci_socket_observer
{
public:
	/*
	 * Called when a HCI event is received from the network.
	 *
	 * operation indicates what the event is, ex
	 * NG_HCI_EVENT_INQUIRY_COMPL, and status is the result of the
	 * operation. Normally, status = 0 means success.
	 *
	 * As an example, the NG_HCI_EVENT_INQUIRY_COMPL that is generated
	 * when hci_socket::query_devices() finishes.
	 */
	virtual void hci_event(hci_socket &s, uint8_t operation, uint8_t status) = 0;

	/*
	 * Called when hci_socket::query_devices() finishes.
	 *
	 * success - true on success, false on error
	 * devices - contains a list of detected devices
	 */
	virtual void query_complete(hci_socket &s, bool success,
			const std::vector<device_info>& devices) = 0;

protected:
	~hci_socket_observer() = default;
};

/*
 * hci_socket provides an interface for sending and receiving HCI
 * events on the bluetooth network.
 *
 * Example:
 * 	Create a socket and scan for bluetooth devices.
 *
 *  class observer : public blue::hci_socket_observer
 *  {
 *  	void query_complete(hci_socket &s, bool success,
 *  		std::vector<btaddr_t> devices) override
 * 		{
 *  		std::cout << "Inquiry completed with status = " <<
 *  			success << std::endl;
 *  	}
 *  };
 *
 * 	ev::default_loop loop;
 * 	blue::hci_socket s(loop);
 * 	observer o;
 *
 * 	s.bind("ubt0hci");
 * 	s.connect("ubt0hci");
 * 	s.register_socket_observer(&observer);
 * 	s.query_devices();
 *
 *  loop.run();
 */
class hci_socket
{
public:
	/*
	 * Exception thrown by socket_recv() when a command is rejected by the
	 * HCI controller. This should generally not happen and would most
	 * likely be due to a program bug or hardware error.
	 */
	class hci_error : public std::exception
	{
	public:
		hci_error(uint16_t opcode, uint8_t status);

		const char *what() const noexcept override;

	private:
		std::string msg;
	};

	/*
	 * Contruct a HCI socket processing IO events from the main loop
	 * passed in loop.
	 *
	 * throws std::runtime_error() on error
	 */
	hci_socket(ev::loop_ref loop);
	~hci_socket() = default;

	/*
	 * Make the socket non-moveable and non-copyable since it
	 * shouldn't be needed. If more sockets are needed, open them
	 * instead of copying an existing one.
	 */
	hci_socket &operator=(hci_socket &&) = delete;
	hci_socket &operator=(const hci_socket &) = delete;
	hci_socket(hci_socket &&) = delete;
	hci_socket(const hci_socket &) = delete;


	/*
	 * Bind the HCI socket to a bluetooth ubt node.
	 *
	 * thows std::runtime_error() on error
	 */
	void bind(const std::string &node);
	/*
	 * Connect the HCI socket to a bluetooth ubt node.
	 *
	 * throws std::runtime_error() on error
	 */
	void connect(const std::string &node);
	/*
	 * Find available HCI nodes.
	 *
	 * Ex:
	 * std::vector<nodeinfo> nodes;
	 *
	 * blue::hci_socket qs;
	 * qs.bind("hci0ubt");
	 * nodes = qs.find_hci_nodes();
	 *
	 * throws std::runtime_error() on error
	 */
	std::vector<nodeinfo> find_hci_nodes();
	/*
	 * Get the bdaddr of the HCI socket.
	 *
	 * throws std::runtime_error() on error.
	 */
	blue::bdaddr get_address() const;

	/*
	 * Send an HCI request out on the network.
	 * Ex: Send auth request
	 *
	 * ng_hci_auth_req_cp cp;
	 *
	 * if (!connected)
	 *	throw std::runtime_error("Must be connected to send auth request");
	 *
	 * cp.con_handle = con_handle;
	 *
	 * socket->send(NG_HCI_OPCODE(NG_HCI_OGF_LINK_CONTROL, NG_HCI_OCF_AUTH_REQ),
	 *		cp);
	 *
	 * throws std::runtime_error() on error
	 */
	template<class T>
	void send(uint16_t opcode, T cp);

	/*
	 * Register an observer for HCI events. The socket does not take
	 * ovnership of the observer.
	 */
	void register_socket_observer(hci_socket_observer *observer);
	/*
	 * Register an hci_observer representing a remote device. The
	 * socket does not take ownership of the observer.
	 */
	void register_observer(hci_observer *observer);
	/*
	 * Unregister a previously registered observer.
	 *
	 * Function does noting if the observer doesn't exist.
	 */
	void unregister_observer(hci_observer *observer);

	/*
	 * Get devices discovered by a call to query_devices().
	 */
	const std::vector<device_info> &get_devices();
	/*
	 * Scan for discoverable devices on the bluetooth network.
	 * Generates a NG_HCI_EVENT_INQUIRY_COMPL event on completion and
	 * found devices can be retrieved by calling get_devices().
	 *
	 * throws std::runtime_error() on error
	 */
	void query_devices();
	/*
	 * Connect to a bluetooth device.
	 * Generates a hci_observer::con_compl callback on completion.
	 */
	void connect_device(hci_observer *o, bdaddr addr);
	/*
	 * Request a human readable name from a bluetooth device.
	 * Generates a hci_observer::remote_name callback on completion.
	 */
	void request_name(hci_observer *o, bdaddr addr, uint8_t page_scan_mode,
	                  uint8_t page_scan_rep_mode, uint16_t clock_offset);
	/*
	 * Send an IO capability respone to a bluetooth device.
	 */
	void io_capability_request_reply(hci_observer *o,
	                                 bdaddr addr,
	                                 uint8_t io_capability,
	                                 uint8_t oob_data_present,
	                                 uint8_t authentication_requirements);
	/*
	 * Send a user confirmation request reply.
	 */
	void user_confirmation_request_reply(hci_observer *o, bdaddr addr);

	/*
	 * Send a disconnect request to a bluetooth device.
	 */
	void disconnect_device(hci_observer *o, uint16_t con_handle);

	/*
	 * Send authentication request to device.
	 */
	void send_auth_request(hci_observer *o, uint16_t con_handle);

	/*
	 * Enable or disable encryption for a connection.
	 */
	void set_encryption(hci_observer *o, uint16_t con_handle, bool enabled);

	/*
	 * Send negative link key reply.
	 */
	void send_negative_link_key_reply(hci_observer *o, bdaddr addr);

	/*
	 * Send a link key as a reply to a link key request.
	 */
	void send_link_key_reply(hci_observer *o, bdaddr addr, key link_key);

	/*
	 * Send a pin code reply to a pin code request.
	 *
	 * throws std::runtime_error() if the PIN is longer than
	 * NG_HCI_PIN_SIZE or on other error.
	 */
	void send_pin_code_reply(hci_observer *o, bdaddr addr, std::string pin);

private:
	void socket_recv();

	void set_event_filter(const std::vector<int> &filters);
	void unmask_events();
	void enable_simple_pairing();
	void clear_observer_requests(const hci_observer *observer);

	template<class T>
	void hci_cmd(uint16_t opcode, T cp);

	/*
	 * The emit functions are simple helper functions that check
	 * whether any observers are present and call the appropiate
	 * callbacks.
	 */
	template<auto (hci_socket_observer::*f), typename ...A>
	void emit_socket_event(A... args);
	template<auto (hci_observer::*f), typename ...A>
	void emit_hci_event(const bdaddr_t &addr, A... args);
	template<auto (hci_observer::*f), typename ...A>
	void emit_hci_event(uint16_t con_handle, A... args);

	/*
	 * Flow control handling
	 */

	struct hci_pkt
	{
		uint16_t opcode;
		std::vector<uint8_t> data;
	};

	/*
	 * dec_cmd_count() decrements the send allowance counter and
	 * returns true if the packet can be sent.
	 */
	bool dec_cmd_count();

	/*
	 * flush_queue() tries to send commands that have been throttled.
	 */
	void flush_queue();

	/*
	 * Callback on IO activity on the HCI socket.
	 */
	void io_callback(ev::io &watcher, int events);

	/*
	 * Keep track of running commands to be able to track command
	 * failures and notify the observer.
	 */
	void push_running(hci_observer *o, hci_event_code code);

	/*
	 * Get the pointer to an observer registered by push_running() as
	 * currently executing the command given by the code argument.
	 *
	 * throws std::runtime_error() if no observer exists
	 */
	hci_observer *pop_running(hci_event_code code);

	/*
	 * Member variables
	 */
	const util::unique_fd fd;
	std::string node;
	std::vector<device_info> devices;
	hci_socket_observer *socket_observer;
	std::map<bdaddr, hci_observer *> observers;

	/*
	 * Keeps a mapping between requests in progress and the observer
	 * that made the request. Used for error reporting.
	 */
	std::map<hci_event_code, std::deque<hci_observer *>> running_reqs;

	/*
	 * Used in flow control. cmd_count indicates how many messages are
	 * allowed to be sent to the controller. The value is decremented
	 * on HCI command send and updated by EVENT_COMMAND_COMPL and
	 * EVENT_COMMAND_STATUS.
	 */
	uint8_t cmd_count;
	std::queue<hci_pkt> pkt_queue;

	/*
	 * libev IO watcher to react to activity on the HCI socket
	 */
	ev::io hci_watcher;
};

/*
 * template header implementations, see above for documentation
 */

template<class T>
void hci_socket::send(uint16_t opcode, T cp)
{
	if (dec_cmd_count())
	{
		syslog(LOG_DEBUG, "Sending HCI command: opcode = %x", opcode);
		if (bt_devsend(fd, htole16(opcode), &cp, sizeof(cp)) < 0)
			throw std::runtime_error("Failed sending HCI request: " +
					std::string(strerror(errno)));
	}
	else // Sending is throttled, queue packet for later sending.
	{
		hci_pkt p;

		syslog(LOG_DEBUG, "HCI command throttled; delaying: opcode = %x",
		       opcode);

		p.opcode = opcode;

		uint8_t *cp_ptr = reinterpret_cast<uint8_t *>(&cp);
		std::copy(cp_ptr, cp_ptr + sizeof(cp), std::back_inserter(p.data));
		pkt_queue.push(p);
	}
}

template<class U, class T>
void append_data(U data, std::vector<T> &v)
{
	T *ptr = reinterpret_cast<T *>(&data);
	std::copy(ptr, ptr + sizeof(data), std::back_inserter(v));
}

template<class T>
void hci_socket::hci_cmd(uint16_t opcode, T cp)
{
	std::vector<uint8_t> buffer;
	ng_hci_cmd_pkt_t cmd{};
	sockaddr_hci addr{};

	addr.hci_len = sizeof(addr);
	addr.hci_family = AF_BLUETOOTH;
	strncpy(addr.hci_node, node.c_str(), sizeof(addr.hci_node));

	cmd.type = NG_HCI_CMD_PKT;
	cmd.opcode = htole16(opcode);
	cmd.length = sizeof(cp);

	append_data(cmd, buffer);
	append_data(cp, buffer);

	if (sendto(fd, buffer.data(), buffer.size(), 0,
			reinterpret_cast<sockaddr *>(&addr), sizeof(addr)) < 0)
		throw std::runtime_error("Could not send HCI CMD: " +
				std::string(strerror(errno)));
}

template<auto (hci_socket_observer::*f), typename ...A>
void hci_socket::emit_socket_event(A... args)
{
	if (socket_observer)
		(socket_observer->*f)(*this, args...);
}

template<auto (hci_observer::*f), typename ...A>
void hci_socket::emit_hci_event(const bdaddr_t &addr, A... args)
{

	if (observers.count(addr) > 0)
		(observers[addr]->*f)(args...);
}

template<auto (hci_observer::*f), typename ...A>
void hci_socket::emit_hci_event(uint16_t con_handle, A... args)
{

	auto itr = std::find_if(observers.begin(), observers.end(),
	[con_handle](std::pair<bdaddr, hci_observer *> p)
	{
		return p.second->get_con_handle() == con_handle;
	});

	if (itr != observers.end())
		(itr->second->*f)(args...);
}

} // namespace blued::lib

#endif // HCI_SOCKET_H
