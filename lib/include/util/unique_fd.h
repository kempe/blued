#ifndef UNIQUE_FD_H
#define UNIQUE_FD_H

#include <unistd.h>

namespace blue::util
{

/*
 * Type that allows automatic life-time management of a file
 * descriptor. The unique_fd will own the file descriptor and call
 * close on it when the unique_fd object is destroyed.
 *
 * A conversion operator to int is provided to allow it to be passed
 * to C functions and library functions that require an int. It is
 * also useful for functions that return -1, an invalid descriptor, on
 * error since the unique_fd can be compared for equality with -1.
 */
class unique_fd
{
public:
	unique_fd();
	explicit unique_fd(int fd);
	~unique_fd();

	unique_fd(unique_fd &&other);
	unique_fd &operator=(unique_fd &&other);

	operator int() const;

private:
	unique_fd(const unique_fd &other) = delete;
	unique_fd &operator=(const unique_fd &other) = delete;

	int fd;
};

/*
 * Wrap the fd provided as an argument in a unique_fd object.
 */
unique_fd make_unique_fd(int fd);

}; // namespace blue::util
#endif // UNIQUE_FD_H
