#ifndef SHARED_CAP_CHAN_H
#define SHARED_CAP_CHAN_H

#include <memory>
#include <libcasper.h>

namespace blue::util
{

/*
 * Wraps a cap_channel_t object and automatically closes it when the
 * object is destroyed.
 */
using shared_cap_chan =
	std::shared_ptr<cap_channel_t>;

/*
 * Acquire a managed application capability, see man 3 cap_init.
 */
shared_cap_chan init_cap_chan();
/*
 * Acquire a managed service capability, see man 3 cap_service_open.
 */
shared_cap_chan make_cap_chan(shared_cap_chan chan, const char *service);

}; // namespace blue::util
#endif // UNIQUE_CAP_CHAN_H
