#ifndef SDP_PARSER_H
#define SDP_PARSER_H

#include <math.h>

#include <sstream>
#include <string>

#include "cap_sdp.h"

/*
 * This namespace contains the class attr that takes a sdp_attr and
 * turns it into an AST of C++ classes.
 *
 * See lib/src/hid/bthid.cpp for example code using this.
 */
namespace blue::sdp::parser
{

// Use the non-standard clang extension of __uint128_t to represent
// the widest integers available in the SDP attributes.
using uint128_t = __uint128_t;
using int128_t = __int128_t;


// From Bluetooth Core Specification v5.2
// Section 3.2 DATA ELEMENT TYPE DESCRIPTOR
namespace types
{
	// The type is shifted left to the top 5 bits.
	static constexpr uint8_t TS(uint8_t val)
	{
		return val << 3;
	}

	// Nil, the null type
	constexpr uint8_t nil = 0x00;
	// Unsigned Integer
	constexpr uint8_t uint = TS(1);
	// Signed twos-complement integer
	constexpr uint8_t sint = TS(2);
	// UUID, a universally unique identifier
	constexpr uint8_t uuid = TS(3);
	// Text string
	constexpr uint8_t string = TS(4);
	// Boolean, false = 0, true = 1, compat: true >= 1
	constexpr uint8_t boolean = TS(5);
	// Data element sequence, a data element whose data field is a
	// sequence of data elements
	constexpr uint8_t seq = TS(6);
	// Data element alternative, data element whose data field is a
	// sequence of data elementsn from which one data element is to be
	// selected.
	constexpr uint8_t seq_select = TS(7);
	// URL, a uniform resource locator
	constexpr uint8_t url = TS(8);
	// Mask to extract the type
	constexpr uint8_t mask = 0xF8;
}

// From Bluetooth Core Specification v5.2
// Section 3.3 DATA ELEMENT TYPE DESCRIPTOR
namespace size_idx
{
	// 1 byte. Exception: if the data element type is nil, the data
	// size is 0 bytes.
	constexpr uint8_t one_byte = 0x00;
	// 2 bytes
	constexpr uint8_t two_bytes = 0x01;
	// 4 bytes
	constexpr uint8_t four_bytes = 0x02;
	// 8 bytes
	constexpr uint8_t eight_bytes = 0x03;
	// 16 bytes
	constexpr uint8_t sixteen_bytes = 0x04;
	// The data size is contained in the additional 8 bits, which are
	// interpreted as an unsigned integer.
	constexpr uint8_t one_byte_len = 0x05;
	// The data size is contained in the additional 16 bits, which are
	// interpreted as an unsigned integer.
	constexpr uint8_t two_byte_len = 0x06;
	// The data size is contained in the additional 32 bits, which are
	// interpreted as an unsigned integer.
	constexpr uint8_t four_byte_len = 0x07;
	// Mask to extract the size index
	constexpr uint8_t mask = 0x7;
}

class entry;

/*
 * By creating an attr object from an sdp_attr, it is parsed into an
 * AST. Each node in the AST is represented by a class entry object,
 * from which specialised types are inherited.
 *
 * all operators can throw std::range_error() and std::runtime_error()
 * if the SDP attribute being parsed is invalid
 */
class attr
{
public:
	attr(const blue::sdp::sdp_attr &a);
	~attr();

	attr(const attr &other) = delete;
	attr &operator=(const attr &other) = delete;

	attr(attr &&other);
	attr &operator=(attr &&other);

	/*
	 * Returns a reference to the top node of the tree. Unless a
	 * container type has been parsed, this will be the only node in
	 * the tree.
	 */
	const entry &get_entry() const;

protected:
	const entry *e;

	std::stringstream buf;
};

/*
 * Generic class representing an SDP attribute. More specialised types
 * inherit from this class.
 */
class entry
{
public:
	virtual ~entry() = default;

	/*
	 * Returns the type of the attribute from among
	 * blue::sdp::parser:types.
	 */
	virtual uint8_t get_type() const = 0;

	/*
	 * Returns the size index of the attribute from among
	 * blue::sdp::parser:size_idx.
	 */
	virtual uint8_t get_size_idx() const = 0;

	/*
	 * Returns a human readable string representation of the AST.
	 */
	virtual std::string str(uint8_t width = 0) const;

	/*
	 * Returns a flat binary array of all the data values in the
	 * attributes in the tree with all type information stripped.
	 */
	virtual std::vector<uint8_t> bin() const;

protected:
	// The base entry type should not be constructed and the derived
	// types should be used instead.
	entry();
};

/*
 * The empty SDP attribute without any value.
 */
class nil_entry : public entry
{
public:
	nil_entry();
	virtual ~nil_entry() = default;

	uint8_t get_type() const override;
	uint8_t get_size_idx() const override;
};

template<class T>
T extract_type(std::stringstream &buf)
{
	// Only support up to uint128_t
	static_assert(sizeof(T) <= sizeof(uint128_t));

	T tmp;
	buf.read(reinterpret_cast<char *>(&tmp), sizeof(tmp));
	if (buf.fail() && buf.gcount() != sizeof(T))
		throw std::runtime_error("extract_type: malformed SDP type field");

	switch (sizeof(tmp))
	{
	case 1:
		// No conversion needed.
		break;
	case 2:
		tmp = be16toh(tmp);
		break;
	case 4:
		tmp = be32toh(tmp);
		break;
	case 8:
		tmp = be64toh(tmp);
		break;
#if _BYTE_ORDER == _LITTLE_ENDIAN
	case 16:
	{
		// Convert the two 64 bit words to little endian
		uint64_t upper = be64toh(tmp >> 64);
		uint64_t lower = be64toh(tmp & std::numeric_limits<uint64_t>::max());

		// Swap the two 64 bit words to convert the 128 bit type
		tmp = (static_cast<uint128_t>(lower) << 64) + upper;
	}
#else
	case 16:
		// Intentionally empty on BE systems.
		break;
#endif
	}

	return tmp;
}

/*
 * Generic class for representing all the numeric types, i.e.
 * (u)int8_t, ..., (u)int128_t
 *
 * It provides the function get_value() to extract the value of the
 * number.
 */
template<class T>
class numeric_entry : public entry
{
public:
	numeric_entry(std::stringstream &buf)
	{
		value = extract_type<T>(buf);
	}

	virtual ~numeric_entry() = default;

	template<class U>
	uint8_t get_numeric_type() const
	{
		if (std::is_unsigned<U>())
			return types::uint;
		else
			return types::sint;
	}

	uint8_t get_type() const override
	{
		return get_numeric_type<T>();
	}

	template<class U>
	uint8_t get_numeric_size() const
	{
		uint32_t power = 0;

		// The index value is the two's logarithm of the size for the
		// integral types. Since we know we have power of two sizes,
		// we shift untill we get zero and the times shifted is the
		// integer value of log2() + 1.
		while (sizeof(U) >> ++power);

		return power - 1;
	}

	uint8_t get_size_idx() const override
	{
		return get_numeric_size<T>();
	}

	std::vector<uint8_t> bin() const override
	{
		std::vector<uint8_t> ret;

		const uint8_t *p = reinterpret_cast<const uint8_t *>(&value);
		std::copy(p, p + sizeof(value), std::back_inserter(ret));

		return ret;
	}

	T get_value() const
	{
		return value;
	}


private:
	T value;
};

/*
 * Represents a 16 bit uuid.
 *
 * Use get_value() to extract its value as a uint16_t.
 */
class uuid16 : public numeric_entry<uint16_t>
{
public:
	uuid16(std::stringstream &buf);
	virtual ~uuid16() = default;

	uint8_t get_type() const override;
};

/*
 * Represents a 32 bit uuid.
 *
 * Use get_value() to extract its value as a uint32_t.
 */
class uuid32 : public numeric_entry<uint32_t>
{
public:
	uuid32(std::stringstream &buf);
	virtual ~uuid32() = default;

	uint8_t get_type() const override;
};

/*
 * Represents a 128 bit uuid.
 *
 * Use get_value() to extract its value as a uint128_t.
 */
class uuid128 : public numeric_entry<uint128_t>
{
public:
	uuid128(std::stringstream &buf);
	virtual ~uuid128() = default;

	uint8_t get_type() const override;
};

/*
 * Represents a string value
 *
 * Use get_string() to extract its string data.
 */
class string : public entry
{
public:
	string(std::stringstream &buf, size_t len, uint8_t size_idx);
	virtual ~string() = default;

	uint8_t get_type() const override;
	uint8_t get_size_idx() const override;
	std::vector<uint8_t> bin() const override;

	const std::string &get_string() const;

private:
	std::string str;
	uint8_t size_idx;
};

/*
 * Represents a boolean value.
 *
 * Use get_value() to extract its value.
 */
class boolean : public entry
{
public:
	boolean(std::stringstream &buf);
	virtual ~boolean() = default;

	uint8_t get_type() const override;
	uint8_t get_size_idx() const override;
	std::vector<uint8_t> bin() const override;

	bool get_value() const;

private:
	bool value;
};

/*
 * Represents a container containing other attributes.
 *
 * Use get_sequence() to get a reference to a vector<const entry *>
 * containing its attributes.
 */
class sequence : public entry
{
public:
	sequence(std::stringstream &buf, size_t len, uint8_t size_idx);
	virtual ~sequence();

	sequence(sequence &&other);
	sequence &operator=(sequence &&other);

	uint8_t get_type() const override;
	uint8_t get_size_idx() const override;
	std::string str(uint8_t width = 0) const override;
	std::vector<uint8_t> bin() const override;

	const std::vector<const entry *> &get_sequence() const;

private:
	sequence(const sequence &) = delete;
	sequence &operator=(const sequence &) = delete;

	std::vector<const entry *> entries;
	uint8_t size_idx;
};

/*
 * Specialisation of sequence
 *
 * It behaves exactly the same as sequence apart from having a
 * different type ID. The SDP specification provides this type to
 * indicate that one of the contained attributes should be chosen.
 *
 * It could, for example, enumerate different mutually exclusive
 * options.
 */
class sequence_select : public sequence
{
public:
	sequence_select(std::stringstream &buf, size_t len, uint8_t size_idx);
	virtual ~sequence_select() = default;

	uint8_t get_type() const override;
};

/*
 * This type represents a URL
 *
 * Get a string representation of the URL using get_url()
 */
class url : public string
{
public:
	url(std::stringstream &buf, size_t len, uint8_t size_idx);
	virtual ~url() = default;

	uint8_t get_type() const override;

	const std::string &get_url() const;
};

} // namespace blue::sdp::parser
#endif // SDP_PARSER_H
