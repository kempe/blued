#ifndef BTHID_H
#define BTHID_H

// FIXME: File bug upstream that include guards are missing
#ifndef USBHID_H
#define USBHID_H
#include <usbhid.h>
#endif // USBHID_H

#include <optional>

#include "bdaddr.h"
#include "util/shared_cap_chan.h"

/*
 * This namespace provides types and function for handling Bluetooth
 * HID descriptors.
 */
namespace blue::hid
{
	// HID attribute IDs
	constexpr uint16_t HIDReconnectInitiate = 0x0205;
	constexpr uint16_t HIDDescriptorList = 0x0206;
	constexpr uint16_t HIDBatteryPower = 0x0209;
	constexpr uint16_t HIDNormallyConnectable = 0x020D;

	struct hid_descriptor
	{
		uint16_t control_psm;
		uint16_t interrupt_psm;
		uint16_t vendor_id;
		uint16_t product_id;
		uint16_t version;
		bool reconnect_initiate;
		bool battery_power;
		bool normally_connectable;
		bool keyboard;
		bool mouse;
		bool has_wheel;
		bool has_hwheel;
		bool has_cons;
		report_desc_t desc;

		hid_descriptor();
		~hid_descriptor();

		hid_descriptor(hid_descriptor &&other);
		hid_descriptor &operator=(hid_descriptor &&other);

		hid_descriptor(const hid_descriptor &) = delete;
		hid_descriptor &operator=(const hid_descriptor &) = delete;
	};

	/*
	 * Request a Bluetooth HID descriptor from the remote device with
	 * address addr. The request is sent from local device with
	 * address local_addr.
	 *
	 * sdp_chan is a channel to blue.sdp opened using
	 * cap_service_open()
	 *
	 * throws std::runtime_error() on error
	 *
	 * If the device is not a HID device, the return value is empty.
	 */
	std::optional<hid_descriptor> get_descriptor(util::shared_cap_chan sdp_chan,
			blue::bdaddr local_addr, blue::bdaddr addr);

	// Mandatory device ID fields as per the Bluetooth DeviceID_SPEC V13.
	constexpr uint16_t SpecificationID = 0x0200;
	constexpr uint16_t VendorID = 0x0201;
	constexpr uint16_t ProductID = 0x0202;
	constexpr uint16_t Version = 0x0203;
	constexpr uint16_t PrimaryRecord = 0x0204;
	constexpr uint16_t VendorIDSource = 0x0205;

	struct device_id
	{
		uint16_t specification_id;
		uint16_t vendor_id;
		uint16_t product_id;
		uint16_t version;
		bool primary_record;
		uint16_t vendor_id_source;
	};

	/*
	 * Request a Bluetooth device ID descriptor from the remote device
	 * with address addr. The request is made from local HCI device
	 * with address local_addr.
	 *
	 * sdp_chan is a channel to blue.sdp opened using
	 * cap_service_open()
	 *
	 * throws std::runtime_error() on error
	 *
	 * If the device does not return a device_id, the return value is
	 * empty.
	 */
	std::optional<device_id> get_devid(util::shared_cap_chan sdp_chan,
			blue::bdaddr local_addr, blue::bdaddr addr);

} // namespace blue::bthid

#endif // BTHID_H
