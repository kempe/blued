#ifndef HID_DEVICE_H
#define HID_DEVICE_H

#include <bdaddr.h>

#include <ev++.h>
#include <sys/mouse.h>

#include <bitset>
#include <cstdint>
#include <chrono>

#include "bthid.h"
#include "kbd.h"
#include "util/unique_fd.h"

namespace blue::hid
{

/*
 * hid_watcher is the parent of a hid_device responsible for creating
 * and cleaning up instances of hid_device.
 */
class hid_watcher
{
public:
	hid_watcher() = default;
	virtual ~hid_watcher() = default;

	/*
	 * disconnect() is called by the hid_device when the physical
	 * bluetooth device it represents disconnects and the hid_device
	 * can be deleted.
	 */
	virtual void disconnect(bdaddr addr) = 0;
};

/*
 * hid_device represents a bluetooth HID device and reads, parses and
 * passes the data onto the operating system for processing.
 */
class hid_device
{
using time_point = std::chrono::time_point<std::chrono::steady_clock>;

public:
	/*
	 * Construct hid_device representing a physical bluetooth HID
	 * device.
	 *
	 * An l2cap control connection is expected to already be open to
	 * the device when it is created. When the interrupt channel is
	 * opened, it should be passed to the intr_opened() member
	 * function.
	 *
	 * The Bluetooth HID specification specifies that the control
	 * connection should always be opened first.
	 *
	 * If the hid_device object is destroyed, the l2cap connection to
	 * the HID device will be closed.
	 *
	 * Config:
	 *  kbd_chan   - service channel to blue.kbd
	 *  sdp_chan   - service channel to blue.sdp
	 *  parent     - pointer to parent inheriting hid_watcher
	 *  cons_fd    - fd pointing to /dev/consolectl
	 *  uinput     - true if events should be passed to X via uinput
	 *  addr       - bluetooth address of the HID device
	 *  ctrl       - fd pointing to a l2cap control connection
	 *  loop       - libev event loop for IO processing
	 *  d          - HID descriptor for the device
	 *
	 *  throws std::runtime_error() on error
	 */
	struct hid_device_config
	{
		util::shared_cap_chan kbd_chan;
		util::shared_cap_chan sdp_chan;
		hid_watcher *parent;
		int cons_fd;
		bool uinput;
		blue::bdaddr local_addr;
		blue::bdaddr addr;
		util::unique_fd ctrl;
		ev::loop_ref loop;
	};
	hid_device(hid_device_config &&config, blue::hid::hid_descriptor &&d);
	~hid_device();

	/*
	 * intr_opened() takes an kbd capability and an fd pointing to an
	 * interrupt channel for the device.
	 *
	 * The device will not be functional until the interrupt channel
	 * has been opened.
	 *
	 * throws std::runtime_error() on error
	 */
	void intr_opened(util::unique_fd &&intr_fd);


private:
	hid_device(const hid_device &) = delete;
	hid_device& operator=(const hid_device &) = delete;
	hid_device(hid_device &&) = delete;
	hid_device& operator=(hid_device &&) = delete;

	void init_state();
	void populate_devid(util::shared_cap_chan sdp_chan,
	                    blue::bdaddr local_addr);
	void populate_default_devid();

	const blue::bdaddr &get_addr() const;

	void disconnect();

	int32_t hid_control(uint8_t *data, int32_t len);
	int32_t hid_interrupt(uint8_t *data, int32_t len);

	int32_t kbd_process_keys();
	int32_t kbd_status_changed(uint8_t *data, int32_t len);

	int32_t uinput_rep_key(int fd, int32_t key, int32_t make);
	int32_t uinput_rep_leds(int fd, int state, int mask);
	int32_t uinput_rep_cons(int fd, int32_t key, int32_t make);
	int32_t uinput_rep_mouse(int fd, int32_t x, int32_t y, int32_t z,
			int32_t t, int32_t buttons, int32_t obuttons);
	void uinput_kbd_write(const std::bitset<xsize> &m, int32_t fb,
			int32_t make, int fd);
	util::unique_fd uinput_open_common(blue::bdaddr local, std::string name);
	util::unique_fd uinput_open_mouse(blue::bdaddr local);
	util::unique_fd uinput_open_keyboard(blue::bdaddr local);
	int32_t uinput_kbd_status_changed(uint8_t *data, int32_t len);
	int32_t uinput_get_rcpt_mask();

	void ctrl_callback(ev::io &watcher, int events);
	void intr_callback(ev::io &watcher, int events);

	util::shared_cap_chan kbd_chan;

	blue::bdaddr addr;
	blue::hid::hid_descriptor desc;

	/*
	 * Information queried from the device.
	 */
	uint16_t vendor_id;
	uint16_t product_id;
	uint16_t version;

	util::unique_fd ctrl;	/* control channel */
	ev::io ctrl_watcher;

	util::unique_fd intr;	/* interrupt channel */
	ev::io intr_watcher;

	blue::util::unique_fd vkbd;	/* virual keyboard */

	//void				*ctx;   /* product specific dev state */
	util::unique_fd ukbd;  /* evdev user input */
	util::unique_fd umouse;/* evdev user input */
	int obutt; /* previous mouse buttons */

	/*
	 * Previously pressed mouse buttons for the console to track
	 * changes to the pressed buttons.
	 *
	 * initial_click is the point in time when an initial click
	 * was made. This is used to keep track of multiple clicks within
	 * a short period of time to keep track of double-clicking. clicks
	 * is keeping track of how many clicks have been registered during
	 * the multi-click period.
	 */
	int obutt_cons;
	std::array<time_point, MOUSE_MAXBUTTON> initial_click;
	std::array<int, MOUSE_MAXBUTTON> clicks;

	int32_t consk; /* last consumer page key */
	std::bitset<xsize> keys1;	/* keys map (new) */
	std::bitset<xsize> keys2;	/* keys map (old) */

	hid_watcher *parent;

	// fd to /dev/consolectl
	int cons_fd;

	// whether uinput should be used
	bool uinput;
};

} // namespace blue::hid

#endif // HID_DEVICE_H
