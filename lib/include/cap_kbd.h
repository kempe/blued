#ifndef CAP_KBD_H
#define CAP_KBD_H

#include "util/shared_cap_chan.h"
#include "util/unique_fd.h"

/*
 * This namespace provides functions for performing HID related
 * operations while in capsicum mode.
 */
namespace blue::kbd
{

	/*
	 * Returns a file descriptor to a /dev/vkbdctl device.
	 *
	 * chan is a channel to the blue.kbd service acquired using
	 * cap_service_open()
	 *
	 * returns -1 on error
	 */
	util::unique_fd cap_open_vkbdctl(util::shared_cap_chan chan);

	/*
	 * Returns a file descriptor to /dev/uinput.
	 *
	 * chan is a channel to the blue.kbd service acquired using
	 * cap_service_open()
	 *
	 * returns -1 on error
	 */
	util::unique_fd cap_open_uinput(util::shared_cap_chan chan);

	/*
	 * Returns the value of the kern.evdev.rcpt_mask sysctl.
	 *
	 * chan is a channel to the blue.kbd service acquired using
	 * cap_service_open()
	 *
	 * returns -1 on error
	 */
	int32_t cap_get_rcpt_mask(util::shared_cap_chan chan);

} // namespace blue::kbd

#endif // CAP_KBD_H
