#ifndef CAP_SDP_H
#define CAP_SDP_H

#include <bluetooth.h>
#include <sdp.h>

#include <array>
#include <vector>

#include "util/shared_cap_chan.h"

/*
 * This namespace provides functions for querying SDP attributes while
 * in capsicum mode.
 */
namespace blue::sdp
{
	struct sdp_attr
	{
		constexpr static uint32_t BUF_SIZE = 1024;

		uint16_t flags = SDP_ATTR_INVALID;
		uint16_t attr = 0;
		uint32_t vlen = BUF_SIZE;
		std::array<uint8_t, BUF_SIZE> value;

		operator sdp_attr_t ();
	};

	/*
	 * Opens a SDP session to remote device with address r from local
	 * address l.
	 *
	 * handle is an output parameter that gives an opaque handle for
	 * passing to cap_sdp_search(). It should be freed by
	 * cap_sdp_close() when done.
	 *
	 * chan is a channel to the blue.sdp service acquired using
	 * cap_service_open()
	 *
	 * returns -1 on error, 0 on success
	 */
	int cap_sdp_open(util::shared_cap_chan chan, const bdaddr_t *l,
	                 const bdaddr_t *r, uint64_t &handle);

	/*
	 * Queries the device represented by handle acquired from
	 * cap_sdp_open(). Takes a vector of service IDs, SDP attribute
	 * IDs and sdp_attr objects.
	 *
	 * The sdp_attr objects provided in argument vp will be updated
	 * with the attributes queried from the device.
	 *
	 * NOTE: A maximum of 12 services can be provided in pp at one
	 *       time.
	 *
	 * Example:
	 *
	 * if (blue::sdp::cap_sdp_open(sdp_chan, addr, handle) == -1)
	 * 	throw std::runtime_error("Could not open SDP connection.");
	 *
	 * std::vector<uint16_t> pp =
	 * {
	 * 	SDP_SERVICE_CLASS_PNP_INFORMATION,
	 * };
	 * std::vector<uint32_t> ap =
	 * {
	 * 	SDP_SINGLE_ATTR(SpecificationID),
	 * 	SDP_SINGLE_ATTR(VendorID),
	 * 	SDP_SINGLE_ATTR(ProductID),
	 * 	SDP_SINGLE_ATTR(Version),
	 * 	SDP_SINGLE_ATTR(PrimaryRecord),
	 * 	SDP_SINGLE_ATTR(VendorIDSource),
	 * };
	 * std::vector<blue::sdp::sdp_attr> vp(ap.size());
	 *
	 * To check whether an attribute was fetched okay,
	 * vp[idx].flags == SDP_ATTR_OK can be used.
	 *
	 * chan is a channel to the blue.sdp service acquired using
	 * cap_service_open()
	 *
	 * returns -1 on error, 0 on success
	 */
	int cap_sdp_search(util::shared_cap_chan chan, uint64_t handle,
			std::vector<uint16_t> pp, std::vector<uint32_t> ap,
			std::vector<sdp_attr> &vp);

	/*
	 * Free a handle acquired with cap_sdp_open()
	 *
	 * chan is a channel to the blue.sdp service acquired using
	 * cap_service_open()
	 *
	 * returns -1 on error, 0 on success
	 */
	int cap_sdp_close(util::shared_cap_chan chan, uint64_t handle);
}

#endif // CAP_SDP_H
