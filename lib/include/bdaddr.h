#ifndef BDADDR_H
#define BDADDR_H

#include <bluetooth.h>
#include <netgraph/bluetooth/include/ng_hci.h>

#include <array>
#include <ostream>

namespace blue { class bdaddr; }
std::ostream &operator<<(std::ostream &os, const blue::bdaddr &addr);
std::string &operator<<(std::string &s, const blue::bdaddr &addr);

namespace blue
{

/*
 * bdaddr is a class that represents the bluetooth address of a
 * device. It provides an interface that simplifies its usage with C++
 * code.
 *
 * It is for example possible to use bdaddr as the key in a
 * std::map<bdaddr, V> and it can be converted to a string
 * representation by streaming it. Ex: std::cout << bdaddr();
 */
class bdaddr
{
	friend std::ostream &::operator<<(std::ostream &os,
			const blue::bdaddr &addr);
	using storage_type = std::array<uint8_t, NG_HCI_BDADDR_SIZE>;
public:
	bdaddr();

	/*
	 * Wrap the C type bdaddr_t in class bdaddr. addr is copied.
	 *
	 * throws std::runtime_error() on format error
	 */
	bdaddr(bdaddr_t addr);

	/*
	 * Create an address object from a human readable address on the
	 * format XX:XX:XX:XX:XX:XX.
	 */
	bdaddr(const char *addr);

	/*
	 * Create an address object from a human readable address on the
	 * format XX:XX:XX:XX:XX:XX.
	 */
	bdaddr(const std::string &addr);

	/*
	 * Return a string representation of the address on the
	 * XX:XX:XX:XX:XX:XX format.
	 */
	std::string str() const;

	operator const bdaddr_t *();
	bool operator<(const bdaddr &other) const;
	bool operator==(const bdaddr &other) const;
	bool operator!=(const bdaddr &other) const;

	storage_type::const_iterator cbegin() const;
	storage_type::const_iterator cend() const;

private:
	storage_type a;
};
} // namespace blue

#endif // BDADDR_H
