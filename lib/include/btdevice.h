#ifndef BTDEVICE_H
#define BTDEVICE_H

#include "bdaddr.h"
#include "hci_socket.h"
#include "hci_types.h"

#include <bluetooth.h>

#include <stdint.h>

#include <array>
#include <functional>

namespace blue
{

class btdevice;

/*
 * device_observer is an interface that allows for listening to
 * bluetooth events delivered to a btdevice instance by implemnting
 * the interface and registering the object via
 * btdevice::register_observer().
 */
class device_observer
{
public:
	/*
	 * device_event receives bluetooth events for device, with
	 * operation containing a NG_HCI_EVENT_*, ex
	 * NG_HCI_EVENT_CON_COMPL, and status representing the result of
	 * the operation.
	 *
	 * status is message specific but it can generally be assumed that
	 * status = 0 means the operation was successful.
	 */
	virtual void device_event(btdevice &device, uint8_t operation,
			uint8_t status) = 0;

	/*
	 * Callback called when a device pairing has finished for the
	 * device.
	 */
	virtual void pairing_finished(btdevice &device, bool success) = 0;

	/*
	 * Callback called when a remote device reports its name.
	 */
	virtual void remote_name(btdevice &device, bool success) = 0;

	/*
	 * Callback called when a connection attempt finishes.
	 */
	virtual void connection_complete(btdevice &device, bool success) = 0;

	/*
	 * Callback called when a disconnect attempt finishes.
	 */
	virtual void disconnect_complete(btdevice &device, bool success) = 0;

protected:
	~device_observer() = default;
};

/*
 * btdevice represents a bluetooth device and provides functions for
 * interacting with said device.
 *
 * Functions that fail will throw on failure.
 *
 * Ex: Connect to a device with address addr
 *
 * 	blue::hci_socket s;
 * 	s.bind("ubt0hci");
 * 	hci_socket.connect("ubt0hci");
 * 	blue::btdevice device(addr, &s);
 * 	device.connect();
 *
 * 	device.connect() is an asynchronous call. To receive the result of
 * 	the operation, device_observer() needs to be implemented by some
 * 	class and the hci_socket should be put into an event loop that
 * 	calls s.socket_recv() when s.get_fd() signals that data is
 * 	available.
 */
class btdevice : public hci_observer
{
public:

	/*
	 * Create a device with specific device info. addr is the only
	 * mandatory field to set in info. Needs a reference to a
	 * hci_socket on which to connect to addr.
	 *
	 * The class does not take ownership of the hci_socket. The socket
	 * needs to be kept alive until btdevice is destroyed.
	 */
	btdevice(device_info info, hci_socket *s);
	virtual ~btdevice();

	/*
	 * btdevice is moveable but not copyable since one device on the
	 * network should only be represented by one set of state data.
	 */
	btdevice &operator=(btdevice &&other);
	btdevice(btdevice &&other);

	/* It doesn't make sense to have more than one instance of a
	 * btdevice. Allow it to be moved but not copied. */
	btdevice &operator=(const btdevice &other) = delete;
	btdevice(const btdevice &other) = delete;

	/*
	 * Register an observer that receives device events for this
	 * device.
	 *
	 * btdevice does not take ownership of the observer. The observer
	 * needs to be kept alive until btdevice is destroyed.
	 */
	void register_observer(device_observer *observer);
	/*
	 * Mark the device as being paired by providing a link key to send
	 * to the remote device. This is useful for initialising a
	 * btdevice instance for an already paired device.
	 */
	void set_paired(const key &link_key);

	/*
	 * Get the remote address of btdevice.
	 */
	const bdaddr &get_addr() const override;
	/*
	 * Return a connection handle for the device. A value of 0xFFFF
	 * means there is no valid connection.
	 */
	uint16_t get_con_handle() const override;
	/*
	 * Return the current pairing state of the device.
	 * true  - device is paired
	 * false - device is not paired
	 */
	bool is_paired() const;
	/*
	 * Get the link key for the device. Useful when wanting to save
	 * the pairing info to persistent memory.
	 *
	 * The link key can only be set through set_paired().
	 */
	const key &get_link_key() const;
	/*
	 * Get the pin code for the device.
	 */
	const std::string &get_pin() const;
	/*
	 * Set the pin code for the device.
	 *
	 * The PIN must be shorter than NG_HCI_PIN_SIZE, a
	 * std::runtime_error() is thrown if the provided PIN is too long.
	 */
	void set_pin(std::string pin);

	/*
	 * Initiate a connection to the device. Results in a
	 * NG_HCI_EVENT_CON_COMPL operation event to the observer when the
	 * attept is finished.
	 */
	void connect();
	/*
	 * Disconnect the device.
	 */
	void disconnect();
	/*
	 * Returns true if the device is connected, false otherwise.
	 */
	bool is_connected() const;
	/*
	 * Send a bluetooth authentication request to the device.
	 * Typically handled internally during pairing.
	 *
	 * throws std::runtime_error() on error
	 */
	void send_auth_req();
	/*
	 * Enable encryption for the connection to the device.
	 * Typically handled internally during pairing.
	 *
	 * throws std::runtime_error() on error
	 */
	void enable_encryption();
	/*
	 * Initiate a secure simple pairing with the device.
	 * Pairing completion is indicated by pairing_finished() being
	 * called on the observer.
	 *
	 * Return
	 * 	true  - pairing started
	 * 	false - device is already paired
	 *
	 * 	throws std::runtime_error() if the device is busy or the
	 * 	operation fails to start
	 */
	bool pair();
	/*
	 * Request a human readable name from the remote device. Results
	 * in device_observer::remote_name() being called upon completion.
	 * On success the name can be retrieved by calling
	 * btdevice::get_name().
	 *
	 * throws std::runtime_error() on error
	 */
	void request_name();
	/*
	 * Return the human readable device named queried by
	 * request_name().
	 */
	const std::string &get_name() const;
	/*
	 * Set a human readable name for the device.
	 *
	 * The name must be shorter or equal to NG_HCI_UNIT_NAME_SIZE. If
	 * a name that is too long is given, a std::runtime_error() will
	 * be thrown.
	 */
	void set_name(std::string name);

	/*
	 * The three functions below are used to set connection parameters
	 * obtained from the device itself. These values are reported by
	 * the result of hci_socket::query_devices().
	 * page_scan_rep_mode is updated by the
	 * HCI_Page_Scan_Repetition_Mode_Change event and page_scan_mode
	 * is updated by the HCI_Page_Scan_mode_Change
	 */
	void set_page_scan_rep_mode(uint8_t mode);
	void set_page_scan_period_mode(uint8_t mode);
	void set_page_scan_mode(uint8_t mode);
	void set_clock_offset(uint16_t offset);

private:
	// Internal hci_observer callbacks
	void con_compl(bool success, uint16_t handle) override;
	void discon_compl(bool success) override;
	void link_key_req() override;
	void link_key_notification(blue::key new_link_key) override;
	void io_capability_request() override;
	void io_capability_request_reply(bool success) override;
	void user_confirmation_request(uint32_t numeric_value) override;
	void user_confirmation_request_reply(bool success) override;
	void auth_compl(bool success) override;
	void pin_code_req() override;
	void remote_name(bool success, std::string name) override;
	void page_scan_mode_change(uint8_t page_scan_mode) override;
	void page_scan_rep_mode_change(uint8_t page_scan_rep_mode) override;
	void encryption_change(bool success, bool enabled) override;
	void neg_link_key_reply(bool success) override;
	void link_key_reply(bool success) override;
	void pin_code_reply(bool success) override;

	// Helper function
	template<auto (device_observer::*f), typename ...A>
	void emit_event(btdevice &device, A... args);

	// Handle pairing result
	void pairing_finished(bool success);


	/*
	 * State machine for handling different states the device can be
	 * in during bluetooth operation.
	 */
	enum class device_state
	{
		IDLE,
		CONNECTING,
		PAIRING,
	};
	enum class device_event
	{
		CONNECT,
		CONNECTION_COMPLETED,
		CONNECTION_FAILED,
		PAIR,
		AUTH_SUCCESSFUL,
		AUTH_FAILED,
		LINK_KEY_REQUEST,
		PIN_REQUEST,
		DISCONNECT_COMPLETED,
		IO_CAPABILITY_REQUEST_REPLY_FAILED,
		USER_CONFIRMATION_REQUEST_REPLY_FAILED,
	};
	void process_state(device_event current);
	device_state current_state;

	bdaddr addr;
	std::string name;

	bool connected;
	uint16_t con_handle;

	bool paired;
	key link_key;
	std::string pin;

	/*
	 * Provided by the device in the inquiry result.
	 * page_scan_rep_mode is updated by
	 * the HCI_Page_Scan_Repetition_Mode_Change event and
	 * page_scan_mode is updated by the HCI_Page_Scan_mode_Change
	 * event.
	 */
	uint8_t page_scan_rep_mode;
	uint8_t page_scan_period_mode;
	uint8_t page_scan_mode;
	uint16_t clock_offset;

	// Keep track of whether a name request is running.
	bool name_request_running;

	// Keep track of whether a connection request is running.
	bool connection_request_running;

	hci_socket *socket;

	device_observer *observer;
};

/*
 * Header implementation of template functions.
 */

template<auto (device_observer::*f), typename ...A>
void btdevice::emit_event(btdevice &device, A... args)
{
	if (observer)
		(observer->*f)(device, args...);
}

} // namespace blued::lib

#endif // BTDEVICE_H
