#ifndef CAP_L2CAP_H
#define CAP_L2CAP_H

#include <bluetooth.h>
extern "C"
{
#include <libcasper.h>
} // extern "C"

#include <cstdint>

#include "util/shared_cap_chan.h"
#include "util/unique_fd.h"

/*
 * This namespace provides functions for opening L2CAP sockets while
 * in capsicum mode.
 */
namespace blue::l2cap
{

	/*
	 * Returns an fd to an L2CAP listening socket
	 *
	 * chan is a channel to the blue.l2cap service acquired using
	 * make_cap_chan() from util/shared_cap_chan.h
	 *
	 * returns -1 on error
	 */
	util::unique_fd cap_l2cap_listen(util::shared_cap_chan chan,
	                                 const bdaddr_t *listen_addr,
	                                 uint16_t psm);

	/*
	 * Returns an fd to an L2CAP socket connected to remote addr
	 *
	 * chan is a channel to the blue.l2cap service acquired using
	 * make_cap_chan() from util/shared_cap_chan.h
	 *
	 * returns -1 on error
	 */
	util::unique_fd cap_l2cap_connect(util::shared_cap_chan chan,
	                                  const bdaddr_t *local_addr,
	                                  uint16_t psm,
	                                  const bdaddr_t *addr);

} // namespace blue::l2cap

#endif // CAP_L2CAP_H
