#ifndef L2CAP_SOCKET_H
#define L2CAP_SOCKET_H

#include <bluetooth.h>
#include <libcasper.h>
#include <ev++.h>

#include <cstdint>

#include "bdaddr.h"
#include "util/shared_cap_chan.h"
#include "util/unique_fd.h"

namespace blue::l2cap
{

constexpr uint16_t PSM_HID_CONTROL = 0x0011;
constexpr uint16_t PSM_HID_INTERRUPT = 0x0013;

class l2cap_listen_socket;

/*
 * The owner of an l2cap_listen_socket is expected to inherit this
 * watcher to receive callbacks from the l2cap_socket.
 */
class l2cap_listen_observer
{
public:
	virtual ~l2cap_listen_observer() = default;

	/*
	 * Callback called when an incoming connection is accepted by the
	 * socket.
	 *
	 * Arguments:
	 *     skt - pointer to the l2cap_listen_socket that accepted the
	 *           connection
	 *     client_fd   - file descriptor to the incoming client
	 *     client_addr - address of the incoming client
	 *
	 * The l2cap_listen_watcher is expected to take ownership of the
	 * client file descriptor passed in client_fd and is responsible
	 * for closing the fd when it is no longer needed.
	 */
	virtual void incoming_connection(l2cap_listen_socket &skt,
	                                 util::unique_fd client_fd,
	                                 blue::bdaddr client_addr) = 0;
};

/*
 * Opens an L2CAP listening socket listening on a specific PSM.
 *
 * When a client connects on the PSM, parent->incoming_connection() is
 * called with the client file descriptor.
 *
 * Example:
 *     Create a socket and wait for connections on PSM 19.
 *
 *     class observer : public blue::l2cap::l2cap_listen_observer
 *     {
 *         void incoming_connection(blue::l2cap::l2cap_listen_socket &skt,
 *                                  int client_fd,
 *                                  blue::bdaddr addr) override
 *            {
 *             std::cout << "Incoming connection from " <<
 *                 addr << " on fd " << client_fd << std::endl;
 *         }
 *     };
 *
 *     ev::default_loop loop;
 *     cap_channel_t *c = cap_init();
 *     cap_channel_t *l2cap_chan = cap_service_open(c, "blue.l2cap");
 *     observer o;
 *     blue::l2cap::l2cap_listen_socket  s(l2cap_chan, loop, &o, 19);
 *
 *     loop.run();
 */
class l2cap_listen_socket
{
public:
	/*
	 * Construct a listening socket on the PSM given in psm.
	 *
	 * chan is a handle for the blue.l2cap service acquired using
	 * make_cap_chan() in util/shared_cap_chan.h.
	 *
	 * throws std::runtime_error() on error
	 */
	l2cap_listen_socket(util::shared_cap_chan chan, ev::loop_ref loop,
	                    l2cap_listen_observer *parent, uint16_t psm);
	~l2cap_listen_socket() = default;

	/*
	 * Returns true if the two sockets represent the same underlying
	 * L2CAP OS socket.
	 */
	bool operator==(const l2cap_listen_socket &other) const;

private:
	l2cap_listen_socket(const l2cap_listen_socket &) = delete;
	l2cap_listen_socket(l2cap_listen_socket &&) = delete;
	l2cap_listen_socket& operator=(const l2cap_listen_socket &) = delete;
	l2cap_listen_socket& operator=(l2cap_listen_socket &&) = delete;

	void io_callback(ev::io &watcher, int events);

	l2cap_listen_observer *parent;

	const util::unique_fd fd;
	ev::io l2cap_watcher;
};

} // namespace blue::l2cap

#endif // L2CAP_SOCKET
