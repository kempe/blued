#ifndef API_HELPER_H
#define API_HELPER_H

#include "api.h"

#include <errno.h>
#include <sys/socket.h>

#include <msgpack.hpp>

namespace blue
{

template<class T>
bool send_reply(int fd, T msg)
{
	msgpack::sbuffer sbuf;
	msghdr hdr{};

	msgpack::pack(sbuf, msg);
	if (sbuf.size() > blue::MAX_RPC_MSG_SIZE)
	{
		errno = EMSGSIZE;
		return false;
	}

	iovec vec = { sbuf.data(), sbuf.size() };
	hdr.msg_iov = &vec;
	hdr.msg_iovlen = 1;

	if (sendmsg(fd, &hdr, 0) == -1)
		return false;

	return true;
}

}

#endif // API_HELPER_H
