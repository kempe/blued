function(add_manpage manpage source)
	add_custom_command(OUTPUT ${manpage}
		COMMAND sh -c "mandoc -T man ${CMAKE_CURRENT_SOURCE_DIR}/${source} >${manpage}"
		DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/${source}
	)
	add_custom_target(compile_${manpage} ALL DEPENDS ${manpage})
	install(FILES ${CMAKE_BINARY_DIR}/${manpage}
		DESTINATION share/man/man8
	)
endfunction()

