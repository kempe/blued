#include "db.h"

#include <syslog.h>

#include <map>
#include <memory>
#include <sstream>

#include "bdaddr.h"
#include "hci_socket.h"

class sql_stmt
{
public:
	sql_stmt(ksql *db, const char *sql)
	{
		int ret = ksql_stmt_alloc(db, &s, sql, 0);
		if (ret != KSQL_OK)
		{
			syslog(LOG_ERR, "Could not alloc SQL statement: %i",
				   ret);
			throw std::runtime_error("Could not alloc SQL statement.");
		}
	}

	~sql_stmt()
	{
		if (ksql_stmt_free(s) != KSQL_OK)
			syslog(LOG_ERR, "Could not free SQL statement. Memory leaked.");
	}

	operator ksqlstmt*() const
	{
		return s;
	}

private:
	sql_stmt(const sql_stmt &) = delete;
	sql_stmt(sql_stmt &&) = delete;

	ksqlstmt *s;
};

DB::DB(const std::string &path)
{
	ksqlcfg cfg;

	ksql_cfg_defaults(&cfg);
	cfg.flags |= KSQL_FOREIGN_KEYS;

	db = ksql_alloc_child(&cfg, NULL, NULL);
	if (!db)
		throw std::runtime_error("Could not spawn DB process");

	if (ksql_open(db, path.c_str()) != KSQL_OK)
		throw std::runtime_error("Failed to open database");

	initialise_tables();
}

DB::~DB()
{
	ksql_close(db);
	ksql_free(db);
}

void DB::save_device(const blue::btdevice &device)
{
	std::stringstream ss;
	const char save_stmt[] =
		"INSERT INTO device(address) VALUES(?) ON CONFLICT(address) DO NOTHING;";
	sql_stmt s(db, save_stmt);

	ss << device.get_addr();
	if (ksql_bind_str(s, 0, ss.str().c_str()) != KSQL_OK)
		throw std::runtime_error(
				"Could not bind device address to statement.");

	if (ksql_stmt_step(s) != KSQL_DONE)
		throw std::runtime_error("Executing save statement failed:");

	save_link_key(device);
	save_pin(device);
	save_name(device);
}

void DB::save_link_key(const blue::btdevice &device)
{
	const char *save_key_stmt =
		"INSERT INTO link_key(device_id, link_key) VALUES(?,?) "
		"ON CONFLICT(device_id) DO UPDATE SET link_key = excluded.link_key;";
	sql_stmt s(db, save_key_stmt);

	if (ksql_bind_int(s, 0, get_device_db_id(device)) != KSQL_OK)
		throw std::runtime_error("Could not bind db id for link key statement.");

	const blue::key &k = device.get_link_key();

	// ksql does not like blobs with size 0
	if (k.size() == 0)
	{
		syslog(LOG_DEBUG, "DB: link key with length 0 ignored");
		return;
	}

	if (ksql_bind_blob(s, 1, k.data(), k.size()) != KSQL_OK)
		throw std::runtime_error("Could not bind link key to statement.");

	if (ksql_stmt_step(s) != KSQL_DONE)
		throw std::runtime_error("Could not save link key");
}

void DB::save_pin(const blue::btdevice &device)
{
	const char *save_key_stmt =
		"INSERT INTO pin(device_id, pin) VALUES(?,?) "
		"ON CONFLICT(device_id) DO UPDATE SET pin = excluded.pin;";
	sql_stmt s(db, save_key_stmt);

	if (ksql_bind_int(s, 0, get_device_db_id(device)) != KSQL_OK)
		throw std::runtime_error("Could not bind db id for link key statement.");

	const std::string &k = device.get_pin();

	// ksql does not like blobs with size 0
	if (k.size() == 0)
	{
		syslog(LOG_DEBUG, "DB: PIN with length 0 ignored");
		return;
	}

	if (ksql_bind_blob(s, 1, k.data(), k.size()) != KSQL_OK)
		throw std::runtime_error("Could not bind link key to statement.");

	if (ksql_stmt_step(s) != KSQL_DONE)
		throw std::runtime_error("Could not save link key");
}

void DB::save_name(const blue::btdevice &device)
{
	const char *save_name_stmt =
		"INSERT INTO name(device_id, name) VALUES(?,?) "
		"ON CONFLICT(device_id) DO UPDATE SET name = excluded.name;";
	sql_stmt s(db, save_name_stmt);

	// There is no need to save an empty name.
	if (device.get_name() == "")
		return;

	if (ksql_bind_int(s, 0, get_device_db_id(device)) != KSQL_OK)
		throw std::runtime_error("Could not bind db id for name statement.");

	if (ksql_bind_str(s, 1, device.get_name().c_str()) != KSQL_OK)
		throw std::runtime_error("Could not bind device name to statement.");

	if (ksql_stmt_step(s) != KSQL_DONE)
		throw std::runtime_error("Could not save name");
}

std::map<blue::bdaddr, blue::btdevice> DB::load_devices(blue::hci_socket *skt)
{
	std::map<blue::bdaddr, blue::btdevice> devices;
	const char *load_stmt =
		"SELECT address, link_key.link_key, pin.pin, name.name FROM device "
		"INNER JOIN link_key ON device.id = link_key.device_id "
		"INNER JOIN pin ON device.id = pin.device_id "
		"LEFT JOIN name ON device.id = name.device_id;";
	sql_stmt s(db, load_stmt);

	ksqlc ret;
	while ((ret = ksql_stmt_step(s)) == KSQL_ROW)
	{
		const char *addr, *name;
		const char *key, *pin;
		size_t key_size, pin_size;
		blue::bdaddr addr_class;
		blue::key key_class;
		std::string pin_class;
		int is_null;

		if (ksql_result_str(s, &addr, 0) != KSQL_OK)
		{
			syslog(LOG_ERR, "Could not get the address from the SQL statment.");
			throw std::runtime_error("Could not get the address "
			                         "from the SQL statement");
		}
		const void **key_ptr = reinterpret_cast<const void **>(&key);
		if (ksql_result_blob(s, key_ptr, &key_size, 1) != KSQL_OK)
		{
			syslog(LOG_ERR, "Could not get the link key from the SQL statement"
			       " for the device with address %s.", addr);
			throw std::runtime_error("Could not get the link key "
			                         "from the SQL statement");
		}
		const void **pin_ptr = reinterpret_cast<const void **>(&pin);
		if (ksql_result_blob(s, pin_ptr, &pin_size, 2) != KSQL_OK)
		{
			syslog(LOG_ERR, "Could not get the PIN from the SQL statement "
			       "for the device with address %s.", addr);
			throw std::runtime_error("Could not get the PIN "
			                         "from the SQL statement");
		}

		try
		{
			blue::bdaddr tmp(addr);
			addr_class = tmp;
		}
		catch (const std::runtime_error &e)
		{
			syslog(LOG_ERR, "Got malformed device address "
			       "from database: %s", addr);
			throw e;
		}

		if (key_size != key_class.size())
		{
			syslog(LOG_ERR, "Device %s has a link key with an incorrect "
			       "length in the database: %lu != %lu",
			       addr, key_size, key_class.size());
			throw std::range_error("Link key in DB is not the correct size");
		}
		std::copy(key, key + key_size, key_class.data());

		pin_class = std::string(pin, pin_size);
		if (pin_class.size() > NG_HCI_PIN_SIZE)
		{
			syslog(LOG_ERR, "Device %s has a PIN with that is too long"
			       "in the database: %lu > %u",
			       addr, pin_class.size(), NG_HCI_PIN_SIZE);
			throw std::range_error("PIN in DB is too long");
		}

		if (ksql_result_isnull(s, &is_null, 3) != KSQL_OK)
		{
			syslog(LOG_ERR, "Could not check if name is NULL. "
			                "Unable to load device name.");
			// Skip loading the name if we couldn't tell whether it exists.
			is_null = 1;
		}

		if (!is_null)
		{
			if (ksql_result_str(s, &name, 3) != KSQL_OK)
			{
				syslog(LOG_ERR, "Could not get the name from the SQL statment.");
				throw std::runtime_error("Could not get the name "
				                         "from the SQL statement");
			}
		}
		else
			name = nullptr;

		blue::device_info info = {
			.addr = addr_class,
		};
		blue::btdevice d(info, skt);
		d.set_paired(key_class);
		d.set_pin(pin_class);
		if (name)
			d.set_name(name);
		devices.emplace(addr_class, std::move(d));

		syslog(LOG_NOTICE, "Loaded device %s from DB.", addr);
	}

	if (ret != KSQL_DONE)
	{
		syslog(LOG_WARNING, "Database loading finished "
		       "with unexpected status: %u", ret);
	}

	return devices;
}

void DB::delete_device(const blue::btdevice &device)
{
	const char *delete_stmt = "DELETE FROM device WHERE id = ?";
	sql_stmt s(db, delete_stmt);

	if (ksql_bind_int(s, 0, get_device_db_id(device)) != KSQL_OK)
		throw std::runtime_error("Could not bind db id for delete statement.");

	if (ksql_stmt_step(s) != KSQL_DONE)
		throw std::runtime_error("Something went wrong deleting device.");
}

void DB::initialise_tables()
{
	const char *tables =
		"CREATE TABLE IF NOT EXISTS db_version ("
			"version INT NOT NULL);"
		"CREATE TABLE IF NOT EXISTS device("
			"id INTEGER PRIMARY KEY,"
			"address CHAR(17) UNIQUE NOT NULL);"
		"CREATE TABLE IF NOT EXISTS link_key ("
			"id INTEGER PRIMARY KEY,"
			"device_id INT UNIQUE REFERENCES device(id) ON DELETE CASCADE,"
			"link_key BLOB NOT NULL);"
		"CREATE TABLE IF NOT EXISTS pin ("
			"id INTEGER PRIMARY KEY,"
			"device_id INT UNIQUE REFERENCES device(id) ON DELETE CASCADE,"
			"pin BLOB NOT NULL);"
		"CREATE TABLE IF NOT EXISTS name ("
			"id INTEGER PRIMARY KEY,"
			"device_ID INT UNIQUE REFERENCES device(id) ON DELETE CASCADE,"
			"name CHAR(248) NOT NULL);"; // 248 = NG_HCI_UNIT_NAME_SIZE

	static_assert(NG_HCI_UNIT_NAME_SIZE == 248,
	              "NG_HCI_UNIT_NAME_SIZE is expected to be 248 bytes. "
	              "OS implementation has changed!");

	if (ksql_exec(db, tables, 0) != KSQL_OK)
	{
		throw std::runtime_error("DB init error");
	}
}

int64_t DB::get_device_db_id(const blue::btdevice &device)
{
	int64_t id;
	std::stringstream ss;
	const char *id_stmt =
		"SELECT id FROM device WHERE address = ?;";
	sql_stmt s(db, id_stmt);

	ss << device.get_addr();
	if (ksql_bind_str(s, 0, ss.str().c_str()) != KSQL_OK)
		throw std::runtime_error("Could not bind address for fetching db id.");

	if (ksql_stmt_step(s) != KSQL_ROW)
		throw std::runtime_error("Could not fetch the device db id.");

	if (ksql_result_int(s, &id, 0) != KSQL_OK)
		throw std::runtime_error("Could not get id column from row.");

	return id;
}
