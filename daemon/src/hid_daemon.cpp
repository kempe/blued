#include "hid_daemon.h"

#include <syslog.h>
#include <sys/fcntl.h>

#include <cap_l2cap.h>
#include <hid/bthid.h>

#include "blue_daemon.h"

using blue::util::make_cap_chan;
using blue::util::unique_fd;
using blue::util::make_unique_fd;
using blue::l2cap::cap_l2cap_connect;

hid_daemon::hid_daemon(blue::util::shared_cap_chan chan, blue_daemon *hci,
		bool uinput, ev::loop_ref loop) :
	kbd_chan(make_cap_chan(chan, "blue.kbd")),
	l2cap_chan(make_cap_chan(chan, "blue.l2cap")),
	sdp_chan(make_cap_chan(chan, "blue.sdp")),
	hci(hci),
	loop(loop),
	uinput(uinput),
	ctrl(l2cap_chan, loop, this, blue::l2cap::PSM_HID_CONTROL),
	intr(l2cap_chan, loop, this, blue::l2cap::PSM_HID_INTERRUPT)
{
	if (!sdp_chan || !l2cap_chan || !kbd_chan)
	{
		if (!sdp_chan)
			syslog(LOG_ERR, "Failed to init SDP capability: %s",
				   strerror(errno));

		if (!l2cap_chan)
			syslog(LOG_ERR, "Failed to init L2CAP capability: %s",
				   strerror(errno));

		if (!kbd_chan)
			syslog(LOG_ERR, "Failed to init kbd capability: %s",
				   strerror(errno));

		throw std::runtime_error("capabilitiy initialisation error");
	}

	cons = make_unique_fd(open("/dev/consolectl", O_RDWR));
	if (cons == -1)
	{
		syslog(LOG_ERR, "Could not open /dev/consolectl");
		throw std::runtime_error("Could not open /dev/consolectl");
	}
}

void hid_daemon::disconnect(blue::bdaddr addr)
{
	devices.erase(addr);
}

void hid_daemon::connect(blue::bdaddr addr)
{
	const bdaddr_t *bdaddr = addr;

	try
	{
		std::optional<blue::hid::hid_descriptor> d =
			blue::hid::get_descriptor(sdp_chan, hci->get_local_address(), addr);
		if (!d)
		{
			syslog(LOG_DEBUG, "No HID descriptor could be queried for %s",
				   addr.str().c_str());
			return;
		}

		unique_fd ctrl_fd =
			cap_l2cap_connect(l2cap_chan, hci->get_local_address(),
			                  d->control_psm, bdaddr);
		if (ctrl_fd == -1)
		{
			syslog(LOG_ERR, "Failed to open control connection to %s: %s",
				   addr.str().c_str(), strerror(errno));
			return;
		}

		unique_fd intr_fd =
			cap_l2cap_connect(l2cap_chan, hci->get_local_address(),
			                  d->interrupt_psm, bdaddr);
		if (intr_fd == -1)
		{
			syslog(LOG_ERR, "Failed to open interrupt connection to %s: %s",
				   addr.str().c_str(), strerror(errno));
			return;
		}

		blue::hid::hid_device::hid_device_config config =
		{
			kbd_chan,
			sdp_chan,
			this,
			cons,
			uinput,
			hci->get_local_address(),
			addr,
			std::move(ctrl_fd),
			loop
		};
		devices.try_emplace(addr, std::move(config), std::move(d.value()));
		devices.at(addr).intr_opened(std::move(intr_fd));
	}
	catch (const std::runtime_error &e)
	{
		syslog(LOG_ERR, "Could not perform HID config for %s: %s",
		       addr.str().c_str(), e.what());
	}
}

void hid_daemon::incoming_connection(l2cap_listen_socket &skt,
		unique_fd client_fd,
		blue::bdaddr client_addr)
{
	if (skt == ctrl)
		ctrl_callback(std::move(client_fd), client_addr);
	else if (skt == intr)
		intr_callback(std::move(client_fd), client_addr);
	else
	{
		syslog(LOG_ERR, "Got incoming connection on unknown socket. "
		                "This is a bug!");
	}
}

void hid_daemon::ctrl_callback(unique_fd client_fd, blue::bdaddr remote)
{
	syslog(LOG_NOTICE, "%s: connected control on fd %i",
	       remote.str().c_str(), static_cast<int>(client_fd));

	try
	{
		if (hci->paired(remote))
		{
			std::optional<blue::hid::hid_descriptor> d =
				blue::hid::get_descriptor(sdp_chan, hci->get_local_address(),
				                          remote);
			if (d)
			{
				blue::hid::hid_device::hid_device_config config =
				{
					kbd_chan,
					sdp_chan,
					this,
					cons,
					uinput,
					hci->get_local_address(),
					remote,
					std::move(client_fd),
					loop
				};
				devices.try_emplace(remote, std::move(config), std::move(d.value()));
			}
			else
			{
				syslog(LOG_DEBUG, "No HID descriptor could be queried for incoming %s",
					   remote.str().c_str());
				return;
			}
		}
		else
		{
			syslog(LOG_NOTICE, "%s is not paired. Closing connection!",
				   remote.str().c_str());
		}
	}
	catch (const std::exception &e)
	{
		syslog(LOG_ERR, "Could not configure device %s: %s",
		       remote.str().c_str(), e.what());
	}
}

void hid_daemon::intr_callback(unique_fd client_fd, blue::bdaddr remote)
{
	syslog(LOG_NOTICE, "%s: connected interrupt on fd %i",
	       remote.str().c_str(), static_cast<int>(client_fd));

	if (devices.count(remote) > 0)
		devices.at(remote).intr_opened(std::move(client_fd));
	else
	{
		syslog(LOG_ERR, "Device %s opened intr channel without having "
		       "ctrl opened. Closing connection!", remote.str().c_str());
	}
}
