#include <cxxopts.hpp>
#include <ev++.h>
#include <sys/capsicum.h>
#include <sys/signal.h>
#include <syslog.h>
#include <ucl++.h>

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#include <exception>
#include <iostream>
#include <fstream>

#include <util/shared_cap_chan.h>

#include "blue_daemon.h"
#include "conf.h"

#ifndef BLUED_VERSION_STRING
#define BLUED_VERSION_STRING "unknown version"
#endif

class sig_handler
{
public:
	sig_handler(ev::loop_ref loop) : loop(loop) {}

	void handle(ev::sig &s, int event)
	{
		loop.break_loop(ev::ALL);
	}

private:
	ev::loop_ref loop;
};

static uid_t get_uid(std::string user)
{
	passwd *p = getpwnam(user.c_str());
	if (!p)
		throw std::runtime_error("uid could not be found for " + user);

	return p->pw_uid;
}

static gid_t get_gid(std::string group_name)
{
	group *p = getgrnam(group_name.c_str());
	if (!p)
		throw std::runtime_error("gid could not be found for " + group_name);

	return p->gr_gid;
}

static conf_options parse_config(std::string path)
{
	conf_options opts;
	std::string err;

	ucl::Ucl obj = ucl::Ucl::parse_from_file(path, err);
	if (obj)
	{
		opts.socket_dir =
			obj.lookup("socket_dir").string_value("/var/run/blued");
		opts.socket_name =
			obj.lookup("socket_name").string_value("rpc");
		opts.db_path = obj.lookup("db_path").string_value("/var/db/blued.db");
		opts.hci_node = obj.lookup("hci_node").string_value("ubt0hci");

		try
		{
			std::string user = obj.lookup("user").string_value("root");
			opts.user = get_uid(user);

			std::string group = obj.lookup("group").string_value("wheel");
			opts.group = get_gid(group);
		}
		catch (const std::runtime_error &e)
		{
			syslog(LOG_ERR, "User/group parse error: %s", e.what());
			exit(1);
		}

		std::string perm =
			obj.lookup("socket_permissions").string_value("770");
		try
		{
			opts.socket_permissions = std::stoi(perm, nullptr, 8);
		}
		catch (const std::exception &e)
		{
			syslog(LOG_ERR, "Incorrect socket permissions: %s", perm.c_str());
			exit(1);
		}

		opts.uinput = obj.lookup("uinput").bool_value(true);
	}
	else
	{
		syslog(LOG_ERR, "Failed to read config file: %s", err.c_str());
		exit(1);
	}

	return opts;
}

void drop_privileges(const conf_options &opts)
{
	syslog(LOG_DEBUG, "Dropping to group with id %u", opts.group);
	if (setgid(opts.group) == -1)
	{
		syslog(LOG_ERR, "Could not drop to group with id %u: %s",
			   opts.group, strerror(errno));
		exit(1);
	}

	syslog(LOG_DEBUG, "Dropping to user with id %u", opts.user);
	if (setuid(opts.user) == -1)
	{
		syslog(LOG_ERR, "Could not drop to user with id %u: %s",
			   opts.user, strerror(errno));
		exit(1);
	}

	syslog(LOG_DEBUG, "Entering capabilities mode");
	if (cap_enter())
	{
		syslog(LOG_ERR, "Failed to enter capabilities mode: %s",
		       strerror(errno));
		exit(1);
	}
}

int main(int argc, char* argv[])
{
	int exit_code = 0;
	cxxopts::Options options("blued", "blued bluetooth daemon");

	options.add_options()
		("d", "fork to background; daemon mode")
		("f,config_file", "blued configuration file",
		 cxxopts::value<std::string>()->default_value("/usr/local/etc/blued.conf"))
		("h,help", "Print usage")
		("version", "Print version information");

	cxxopts::ParseResult cmds = options.parse(argc, argv);
	if (cmds.count("help"))
	{
		std::cout << options.help() << std::endl;
		return 0;
	}

	if (cmds.count("version"))
	{
		std::cout << "Version: \n" BLUED_VERSION_STRING << std::endl;
		return 0;
	}

	if (getuid() != 0)
	{
		syslog(LOG_ERR, "blued must be started as root");
		std::cerr << "blued must be started as root" << std::endl;
		return 1;
	}


	const conf_options opts =
		parse_config(cmds["config_file"].as<std::string>());

	syslog(LOG_NOTICE, "Initialising daemon...");

	if (cmds.count("d"))
	{
		syslog(LOG_NOTICE, "-d given, forking to background");
		if (daemon(0, 0) == -1)
		{
			syslog(LOG_ERR, "Could not fork to background: %s",
			       strerror(errno));
			return 1;
		}
	}

	blue::util::shared_cap_chan channel = blue::util::init_cap_chan();
	if (!channel)
	{
		syslog(LOG_ERR, "Failed to init generic capability: %s",
			   strerror(errno));
		return 1;
	}

	try
	{
		ev::default_loop loop;
		sig_handler int_handler(loop);
		ev::sig s_int(loop);
		sig_handler term_handler(loop);
		ev::sig s_term(loop);

		s_int.set<sig_handler, &sig_handler::handle>(&int_handler);
		s_int.set(SIGINT);
		s_int.start();

		s_term.set<sig_handler, &sig_handler::handle>(&term_handler);
		s_term.set(SIGTERM);
		s_term.start();

		blue_daemon daemon(opts, channel, loop);

		drop_privileges(opts);

		channel = nullptr;

		syslog(LOG_NOTICE, "Entering main loop. Running!");
		loop.run();

		syslog(LOG_NOTICE, "Exiting");
	}
	catch (const std::exception &e)
	{
		syslog(LOG_ERR, "Unhandled error. Exiting! %s", e.what());
		exit_code = 1;
	}

	return exit_code;
}
