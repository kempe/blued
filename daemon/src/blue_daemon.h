#ifndef DAEMON_H
#define DAEMON_H

#include <bluetooth.h>
#include <ev++.h>
#include <msgpack.hpp>
#include <netgraph/ng_message.h>
#include <libutil.h>

#include <array>
#include <list>
#include <map>
#include <string>

#include "api.h"
#include "hci_socket.h"
#include "btdevice.h"
#include "rpc_client.h"
#include "db.h"
#include "conf.h"
#include "hid_daemon.h"
#include "util/unique_fd.h"
#include "util/shared_cap_chan.h"

/*
 * blue_daemon is the main class for the blued daemon. It manages the
 * list of known and paired devices and responds to RPC commands.
 */
class blue_daemon : public blue::hci_socket_observer,
	public blue::device_observer, public rpc_observer
{
public:
	blue_daemon(const conf_options &opts,
			blue::util::shared_cap_chan chan, ev::loop_ref loop);
	virtual ~blue_daemon();

	/*
	 * Perform a scan for visible bluetooth devices.
	 */
	void scan();
	/*
	 * Get a reference to the map containing all known devices.
	 */
	const std::map<blue::bdaddr, blue::btdevice> &get_devices() const;

	/*
	 * Callbacks from RPC clients.
	 * See rpc_client.h class rpc_observer for details.
	 */
	blue::rpc_op pair_request(const blue::bdaddr &addr) override;
	blue::rpc_op set_pin_request(const blue::bdaddr &addr,
		const std::string &pin) override;
	const std::map<blue::bdaddr, blue::btdevice> &list_known_request() override;
	blue::rpc_op unpair_request(const blue::bdaddr &addr) override;
	blue::rpc_op name_request(const blue::bdaddr &addr) override;
	blue::rpc_op connection_request(const blue::bdaddr &addr) override;
	blue::rpc_op disconnect_request(const blue::bdaddr &addr) override;
	void client_disconnected(rpc_client &client) override;
	blue::rpc_op scan_request() override;

	/*
	 * Returns true if the device with addr is paired, false if it is
	 * not paired or unknown.
	 */
	bool paired(blue::bdaddr addr) const;
	/*
	 * Returns true if the device is known, false otherwise.
	 */
	bool known(blue::bdaddr addr) const;

	/*
	 * Returns the local HCI socket address.
	 *
	 * throws std::runtime_error() on error.
	 */
	blue::bdaddr get_local_address() const;

private:
	/*
	 * Callbacks from blue::btdevice, see lib/include/btdevice.h class
	 * device_observer for details.
	 */
	void device_event(blue::btdevice &d, uint8_t operation,
			uint8_t status) override;
	void pairing_finished(blue::btdevice &d, bool success) override;
	void remote_name(blue::btdevice &d, bool success) override;
	void connection_complete(blue::btdevice &d, bool success) override;
	void disconnect_complete(blue::btdevice &d, bool success) override;

	/*
	 * Callback from blue::hci_socket, see lib/include/hci_socket.h
	 * class hci_socket_observer for details.
	 */
	void hci_event(blue::hci_socket &s, uint8_t operation,
			uint8_t status) override;
	void query_complete(blue::hci_socket &s, bool success,
			const std::vector<blue::device_info> &devices) override;

	/*
	 * Called from the constructor to connect the RPC socket.
	 */
	void connect_socket(const conf_options &opts);

	/*
	 * Called when a new RPC client tries to connect to the daemon.
	 */
	void rpc_callback(ev::io &watcher, int events);

	/*
	 * Exctract a map containing all paired devices.
	 *
	 * The paired devices are moved from the internal devices map to
	 * the map returned by this function, i.e. they are removed from
	 * the internal list.
	 */
	std::map<blue::bdaddr, blue::btdevice> extract_paired();

	/*
	 * Add newly detected devices to the known device list and update
	 * the scan parameters of already known devices.
	 */
	void update_device_list(blue::hci_socket &s,
		const std::vector<blue::device_info> &discovered_devices);

	ev::loop_ref loop;

	blue::util::unique_fd rpc_fd;
	ev::io rpc_watcher;
	/*
	 * We use an std::forward_list() here to avoid having to implement
	 * move semantics for rpc_client. The ev::io class did not want to
	 * behave when moved.
	 */
	std::forward_list<rpc_client> clients;

	blue::util::unique_fd socket_dir_fd;
	pidfh *pid_file;
	blue::hci_socket hci_socket;
	std::string socket_name;

	std::map<blue::bdaddr, blue::btdevice> devices;

	// True if a scan is in progress.
	bool scanning;

	hid_daemon hid;
	DB db;
};

#endif // DAEMON_H
