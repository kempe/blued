#ifndef HID_DAEMON_H
#define HID_DAEMON_H

#include <ev++.h>

#include <l2cap_socket.h>

#include <map>

#include <bdaddr.h>
#include <hid/bthid.h>
#include <hid/hid_device.h>
#include <util/shared_cap_chan.h>
#include <util/unique_fd.h>

using blue::l2cap::l2cap_listen_socket;

class blue_daemon;

class hid_daemon : public blue::hid::hid_watcher,
                   public blue::l2cap::l2cap_listen_observer
{
public:
	hid_daemon(blue::util::shared_cap_chan chan, blue_daemon *hci,
	           bool uinput, ev::loop_ref loop);
	~hid_daemon() = default;

	void disconnect(blue::bdaddr addr) override;

	void connect(blue::bdaddr addr);

private:
	hid_daemon(const hid_daemon &) = delete;
	hid_daemon &operator=(const hid_daemon &) = delete;
	hid_daemon(hid_daemon &&) = delete;
	hid_daemon &operator=(hid_daemon &&) = delete;

	// Callback from the two L2CAP sockets on incoming connection
	void incoming_connection(l2cap_listen_socket &skt,
	                         blue::util::unique_fd client_fd,
	                         blue::bdaddr client_addr) override;
	// {ctrl,intr}_callback are called from incoming_connection
	// depending on which socket received the connection
	void ctrl_callback(blue::util::unique_fd client_fd, blue::bdaddr addr);
	void intr_callback(blue::util::unique_fd client_fd, blue::bdaddr addr);


	blue::util::shared_cap_chan kbd_chan;
	blue::util::shared_cap_chan l2cap_chan;
	blue::util::shared_cap_chan sdp_chan;
	blue_daemon *hci;
	ev::loop_ref loop;

	std::map<blue::bdaddr, blue::hid::hid_device> devices;

	blue::util::unique_fd cons;	 /* /dev/consolectl */
	bool uinput; /* is uinput enabled? */

	l2cap_listen_socket ctrl;
	l2cap_listen_socket intr;
};

#endif // HID_DAEMON_H
