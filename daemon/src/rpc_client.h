#ifndef RPC_CLIENT_H
#define RPC_CLIENT_H

#include <ev++.h>
#include <msgpack.hpp>

#include <string>

#include "api.h"
#include "bdaddr.h"
#include "btdevice.h"
#include "util/unique_fd.h"

class rpc_client;

/*
 * Class for the parent of class rpc_client that receives callbacks
 * when RPC calls are made by an RPC client.
 */
class rpc_observer
{
public:
	virtual ~rpc_observer() = default;

	/*
	 * Request to pair device with address addr.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op pair_request(const blue::bdaddr &addr) = 0;
	/*
	 * Request to set the PIN of the device with address addr.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op set_pin_request(const blue::bdaddr &addr,
		const std::string &pin) = 0;
	/*
	 * Request to list all known devices.
	 *
	 * Returns a const reference to a map of all known devices.
	 */
	virtual const std::map<blue::bdaddr, blue::btdevice>
		&list_known_request() = 0;
	/*
	 * Request to unpair the device with address addr.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op unpair_request(const blue::bdaddr &addr) = 0;
	/*
	 * Request to query the human readable name of the device with
	 * address addr.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op name_request(const blue::bdaddr &addr) = 0;
	/*
	 * Request to connect to the device with address addr.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op connection_request(const blue::bdaddr &addr) = 0;
	/*
	 * Request to disconnect f rom the device with address addr.
	 *
	 * Return the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op disconnect_request(const blue::bdaddr &addr) = 0;
	/*
	 * Request to perform a scan for visible devices.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual blue::rpc_op scan_request() = 0;
	/*
	 * Notification that the RPC client has disconnected.
	 *
	 * Returns the right rpc_op to return to the client.
	 */
	virtual void client_disconnected(rpc_client &client) = 0;
};

/*
 * This class represents an RPC client connected to the daemon. It
 * parses RPC messages and makes calls to its parent to perform
 * various functions.
 */
class rpc_client
{
public:
	rpc_client(blue::util::unique_fd &&fd, ev::loop_ref, rpc_observer &parent);
	~rpc_client();

	rpc_client &operator=(const rpc_client &rhs) = delete;
	rpc_client(const rpc_client &other) = delete;
	rpc_client(rpc_client &&other) = delete;
	rpc_client &operator=(rpc_client &&rhs) = delete;

	bool operator<(const rpc_client &rhs) const;
	bool operator==(const rpc_client &rhs) const;

	/*
	 * Called by parent to report the result of a pairing reqest.
	 */
	void pairing_finished(const blue::bdaddr &addr, bool success);
	/*
	 * Called by parent to report a device name.
	 */
	void remote_name(const blue::bdaddr &addr, bool success,
		const std::string &name);
	/*
	 * Called by parent to report the result of a connection request.
	 */
	void connection_finished(const blue::bdaddr &addr, bool success);
	/*
	 * Called by parent to report the result of a disconnect request.
	 */
	void disconnect_finished(const blue::bdaddr &addr, bool success);
	/*
	 * Called by parent to report the result of a scan request.
	 */
	void scan_finished(const std::vector<blue::device_info> &devices);

private:
	/*
	 * Callback triggered when there is activity on the RPC socket for
	 * this client.
	 */
	void rpc_callback(ev::io &watcher, int events);

	/*
	 * Check whether we have an outstanding pairing request for the
	 * device with address addr.
	 */
	bool check_pair_request(const blue::bdaddr &addr);
	/*
	 * Check whether we have an outstanding name request for the
	 * device with address addr.
	 */
	bool check_name_request(const blue::bdaddr &addr);
	/*
	 * Check whether we have an outstanding connection request for the
	 * device with address addr.
	 */
	bool check_connection_request(const blue::bdaddr &addr);
	/*
	 * Check whether we have an outstanding disconnect request for the
	 * device with address addr.
	 */
	bool check_disconnect_request(const blue::bdaddr &addr);
	/*
	 * Check whether we have an outstanding scan request.
	 */
	bool check_scan_request();

	msgpack::unpacker unpacker;

	rpc_observer &parent;

	/*
	 * Keep track of outstanding async requests. If the value of the
	 * request variable matches the provided argument to
	 * check_$request_name(), the request client is waiting for the
	 * request.
	 */
	bool scan_request;
	blue::bdaddr name_request;
	blue::bdaddr pair_request;
	blue::bdaddr connection_request;
	blue::bdaddr disconnect_request;

	ev::io watcher;
	blue::util::unique_fd fd;
};

#endif // RPC_CLIENT_H
