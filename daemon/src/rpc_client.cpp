#include "rpc_client.h"

#include <ev++.h>
#include <syslog.h>

#include <unistd.h>

#include "blue_daemon.h"
#include "api_helper.h"

static void send_empty_reply(int fd, blue::rpc_op op, const char *error_msg)
{
	blue::rpc_msg reply{};
	reply.op = op;
	if (!blue::send_reply(fd, reply))
		syslog(LOG_WARNING, "%s, fd = %i: %s", error_msg,
		       static_cast<int>(fd), strerror(errno));
}

template<class T>
static bool send_reply(int fd, T msg, const char *error_msg)
{
	bool ret;

	ret = blue::send_reply(fd, msg);
	if (!ret)
		syslog(LOG_WARNING, "%s, fd = %i: %s", error_msg,
		       static_cast<int>(fd), strerror(errno));

	return ret;
}


rpc_client::rpc_client(blue::util::unique_fd &&fd_in, ev::loop_ref loop,
		rpc_observer &parent) :
	parent(parent), watcher(loop), fd(std::move(fd_in))
{
	watcher.set<rpc_client, &rpc_client::rpc_callback>(this);
	watcher.set(fd, ev::READ);
	watcher.start();

	syslog(LOG_DEBUG, "Client connected on fd %i", static_cast<int>(fd));
}

rpc_client::~rpc_client()
{
	watcher.stop();

	if (fd != -1)
		syslog(LOG_DEBUG, "Client on fd %i disconnected", static_cast<int>(fd));
}

bool rpc_client::operator<(const rpc_client &rhs) const
{
	return fd < rhs.fd;
}

bool rpc_client::operator==(const rpc_client &rhs) const
{
	return fd == rhs.fd;
}

void rpc_client::pairing_finished(const blue::bdaddr &addr, bool success)
{
	if (!check_pair_request(addr))
		return;

	send_empty_reply(fd,
	                 success ? blue::rpc_op::OP_PAIR_DONE :
	                           blue::rpc_op::OP_PAIR_ERROR,
	                 "Could not send pairing finished message");
}

void rpc_client::remote_name(const blue::bdaddr &addr, bool success,
		const std::string &name)
{
	if (!check_name_request(addr))
		return;

	if (success)
	{
		blue::msg_name_request_done m;
		m.addr = addr.str();
		m.name = name;

		send_reply(fd, m,
		           "Failed to send device name to client");
	}
	else
	{
		send_empty_reply(fd,
		                 blue::rpc_op::OP_NAME_REQUEST_ERROR,
		                 "Could not send OP_NAME_REQUEST_ERROR");
	}
}

void rpc_client::connection_finished(const blue::bdaddr &addr, bool success)
{
	if (!check_connection_request(addr))
		return;

	send_empty_reply(fd,
	                 success ? blue::rpc_op::OP_CONNECTION_REQUEST_DONE :
	                           blue::rpc_op::OP_CONNECTION_REQUEST_ERROR,
	                 "Could not send OP_CONNECTION_REQUEST result");
}

void rpc_client::disconnect_finished(const blue::bdaddr &addr, bool success)
{
	if (!check_disconnect_request(addr))
		return;

	send_empty_reply(fd,
	                 success ? blue::rpc_op::OP_DISCONNECT_REQUEST_DONE :
	                           blue::rpc_op::OP_DISCONNECT_REQUEST_ERROR,
	                 "Could not send OP_DISCONNECT_REQUEST result");
}

void rpc_client::scan_finished(const std::vector<blue::device_info> &devices)
{
	if (!check_scan_request())
		return;

	for (const blue::device_info &d : devices)
	{
		blue::msg_scan_item msg{};
		msg.addr = d.addr.str();
		if (!send_reply(fd, msg,
		                "Failed to send device to client"))
		{
			break;
		}
	}

	blue::rpc_msg done_msg{};
	done_msg.op = blue::rpc_op::OP_SCAN_DONE;
	send_reply(fd, done_msg,
	           "Failed to send scan done to client");
}

void rpc_client::rpc_callback(ev::io &watcher, int events)
{
	ssize_t res;

	if (!(events & EV_READ))
		return;
	try
	{
		unpacker.reserve_buffer(blue::MAX_RPC_MSG_SIZE);
		res = recv(fd, unpacker.buffer(), blue::MAX_RPC_MSG_SIZE, 0);
		if (res == -1 || res == 0)
		{
			if (res == -1)
				syslog(LOG_DEBUG, "Client read error on fd %i: %s",
				       static_cast<int>(fd), strerror(errno));
			// return must follow this call since *this is deleted by
			// the parent.
			parent.client_disconnected(*this);
			return;
		}
		unpacker.buffer_consumed(res);

		msgpack::object_handle oh;
		while (unpacker.next(oh))
		{
			msgpack::object msg_obj = oh.get();

			blue::rpc_msg m;
			msg_obj.convert(m);

			// Keep track of a request for async operations.
			switch (m.op)
			{
			case blue::rpc_op::OP_SCAN_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_SCAN_START received on fd %i",
				       static_cast<int>(fd));

				blue::rpc_msg rep{};
				rep.op = parent.scan_request();

				if (rep.op == blue::rpc_op::OP_SCAN_START)
					scan_request = true;

				send_reply(fd, rep, "Could not send scan request reply");

				break;
			}
			case blue::rpc_op::OP_PAIR_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_PAIR_REQUEST received on fd %i",
				       static_cast<int>(fd));

				blue::msg_pair_request pr;
				msg_obj.convert(pr);
				try
				{
					blue::rpc_msg reply{};
					reply.op = parent.pair_request(pr.addr);

					if (reply.op == blue::rpc_op::OP_PAIR_START)
						pair_request = blue::bdaddr(pr.addr);


					send_reply(fd, reply, "Could not send pair request reply");

				}
				catch (const std::runtime_error &e)
				{
					syslog(LOG_ERR, "Incorrect bdaddr from RPC client: %s",
					       e.what());
					send_empty_reply(fd, blue::rpc_op::OP_PAIR_ERROR,
									 "Could not send OP_PAIR_ERROR");
				}
				break;
			}
			case blue::rpc_op::OP_DEVICE_PIN_SET:
			{
				syslog(LOG_DEBUG, "OP_SET_DEVICE_PIN received on fd %i",
				       static_cast<int>(fd));

				blue::msg_set_pin_request p_msg;
				msg_obj.convert(p_msg);

				blue::rpc_msg rep{};
				rep.op = parent.set_pin_request(p_msg.addr, p_msg.pin);
				send_reply(fd, rep, "Could not send OP_DEVICE_PIN_SET reply");
				break;
			}
			case blue::rpc_op::OP_LIST_KNOWN_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_LIST_KNOWN_REQUEST received on fd %i",
				       static_cast<int>(fd));

				for (auto &[addr, device] : parent.list_known_request())
				{
					blue::msg_known_item m;
					m.addr << device.get_addr();
					m.name = device.get_name();
					m.paired = device.is_paired();
					m.connected = device.is_connected();
					if (!send_reply(fd, m,
					                "Could not send known item to client"))
					{
						break;
					}
				}
				send_empty_reply(fd, blue::rpc_op::OP_LIST_KNOWN_DONE,
						"Could not send OP_LIST_KNOWN_DONE to client.");
				break;
			}
			case blue::rpc_op::OP_UNPAIR_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_UNPAIR_REQUEST received on fd %i",
				       static_cast<int>(fd));

				blue::msg_unpair_request m;
				msg_obj.convert(m);

				blue::rpc_msg rep{};
				try
				{
					blue::bdaddr addr(m.addr);
					rep.op = parent.unpair_request(addr);
				}
				catch (const std::runtime_error &e)
				{
					syslog(LOG_ERR, "Could not unpair device: %s", e.what());
					rep.op = blue::rpc_op::OP_UNPAIR_ERROR;
				}

				send_reply(fd, rep,
				           "Could not send OP_UNPAIR_REQUEST reply");
				break;
			}
			case blue::rpc_op::OP_NAME_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_NAME_REQUEST received on fd %i",
				       static_cast<int>(fd));

				blue::msg_name_request nr;
				msg_obj.convert(nr);

				blue::rpc_msg rep{};
				try
				{
					blue::bdaddr addr(nr.addr);
					rep.op = parent.name_request(addr);

					if (rep.op == blue::rpc_op::OP_NAME_REQUEST_RUNNING)
						name_request = blue::bdaddr(nr.addr);

				}
				catch (const std::runtime_error &e)
				{
					syslog(LOG_ERR, "Could not request name from device: %s",
						   e.what());
					rep.op = blue::rpc_op::OP_NAME_REQUEST_ERROR;
				}

				send_reply(fd, rep, "Could not send OP_NAME_REQUEST reply");
				break;
			}
			case blue::rpc_op::OP_CONNECTION_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_CONNECTION_REQUEST received on fd %i",
					   static_cast<int>(fd));

				blue::msg_conn_request cr;
				msg_obj.convert(cr);

				blue::rpc_msg rep{};
				try
				{
					blue::bdaddr addr(cr.addr);
					rep.op = parent.connection_request(addr);

					if (rep.op == blue::rpc_op::OP_CONNECTION_REQUEST_RUNNING)
						connection_request = blue::bdaddr(cr.addr);
				}
				catch (const std::runtime_error &e)
				{
					syslog(LOG_ERR, "Could not connect to device: %s",
						   e.what());
					rep.op = blue::rpc_op::OP_CONNECTION_REQUEST_ERROR;
				}

				send_reply(fd, rep,
				           "Could not send OP_CONNECTION_REQUEST reply");
				break;
			}
			case blue::rpc_op::OP_DISCONNECT_REQUEST:
			{
				syslog(LOG_DEBUG, "OP_DISCONNECT_REQUEST received on fd %i",
					   static_cast<int>(fd));

				blue::msg_discon_request dr;
				msg_obj.convert(dr);

				blue::rpc_msg rep{};
				try
				{
					blue::bdaddr addr(dr.addr);
					rep.op = parent.disconnect_request(addr);

					if (rep.op == blue::rpc_op::OP_DISCONNECT_REQUEST_RUNNING)
						disconnect_request = blue::bdaddr(dr.addr);
				}
				catch (const std::runtime_error &e)
				{
					syslog(LOG_ERR, "Could not disconnect from device: %s",
						   e.what());
					rep.op = blue::rpc_op::OP_DISCONNECT_REQUEST_ERROR;
				}

				send_reply(fd, rep,
				           "Could not send OP_DISCONNECT_REQUEST reply");
				break;
			}
			default:
				syslog(LOG_WARNING, "An unexpected RPC message was received "
				       "(OP: %u)", static_cast<uint32_t>(m.op));
				break;
			}
		}
	}
	catch (const msgpack::type_error &e)
	{
		syslog(LOG_ERR, "Malformed RPC message received: %s", e.what());
	}
}

bool rpc_client::check_pair_request(const blue::bdaddr &addr)
{
	bool ret;

	ret = (pair_request == addr);
	if (ret)
		pair_request = blue::bdaddr();

	return ret;
}

bool rpc_client::check_name_request(const blue::bdaddr &addr)
{
	bool ret;

	ret = (name_request == addr);
	if (ret)
		name_request = blue::bdaddr();

	return ret;
}

bool rpc_client::check_connection_request(const blue::bdaddr &addr)
{
	bool ret;

	ret = (connection_request == addr);
	if (ret)
		connection_request = blue::bdaddr();

	return ret;
}

bool rpc_client::check_disconnect_request(const blue::bdaddr &addr)
{
	bool ret;

	ret = (disconnect_request == addr);
	if (ret)
		disconnect_request = blue::bdaddr();

	return ret;
}

bool rpc_client::check_scan_request()
{
	if (scan_request)
	{
		scan_request = false;
		return true;
	}
	else
		return false;
}
