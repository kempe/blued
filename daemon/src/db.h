#ifndef DB_H
#define DB_H
#include <map>
#include <string>

#include <ksql.h>

#include "btdevice.h"
#include "hci_socket.h"

class DB
{
public:
	DB(const std::string &path);
	~DB();

	void save_device(const blue::btdevice &device);
	void save_link_key(const blue::btdevice &device);
	void save_pin(const blue::btdevice &device);
	void save_name(const blue::btdevice &device);

	void delete_device(const blue::btdevice &device);

	std::map<blue::bdaddr, blue::btdevice> load_devices(blue::hci_socket *skt);

private:
	void initialise_tables();
	int64_t get_device_db_id(const blue::btdevice &device);

	ksql *db;
};

#endif // DB_H
