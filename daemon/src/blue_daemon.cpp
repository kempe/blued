#include "blue_daemon.h"

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <syslog.h>

#include <libgen.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>

#include <filesystem>
#include <iostream>
#include <string>
#include <sstream>

#include <bdaddr.h>
#include <hid/bthid.h>

#include "conf.h"

#include "hid_daemon.h"

using path = std::filesystem::path;

static int create_socket_dir(const conf_options &opts)
{
	const std::string path = opts.socket_dir;
	struct stat sb;
	int fd, ret;

	// Create the directory if it does not exist.
	if ((ret = stat(path.c_str(), &sb)) == -1)
	{
		if (errno != ENOENT)
			throw std::runtime_error("Could not stat socket dir " + path +
			                         ": " + std::string(strerror(errno)));

		// No directory exists because stat failed with ENOENT. Create it.
		if (mkdir(path.c_str(), 0755) == -1)
			throw std::runtime_error("Could not create socket dir at " +
			                         path + ": " + std::string(strerror(errno)));
	}

	// The directory should exist now so open it.
	fd = open(opts.socket_dir.c_str(), O_DIRECTORY | O_NOFOLLOW);
	if (fd == -1)
		throw std::runtime_error("Could not open socket directory: " +
				std::string(strerror(errno)));

	/*
	 * ret != -1 means the directory existed already and was not
	 * created. Since we did not create it, we want to check it has
	 * correct permissions.
	 */
	if (ret != -1)
	{
		/*
		 * The path exists. Make sure it is a directory and set our
		 * desired permissions. If it is not a directory, we should
		 * have failed when trying to open it above, but add an extra
		 * check just to be sure.
		 */
		if (!S_ISDIR(sb.st_mode))
			throw std::runtime_error(path + " already exists but is not a "
			                         "directory!");
		if (fchmodat(fd, ".", 0755, AT_SYMLINK_NOFOLLOW) == -1)
			throw std::runtime_error("Could not set permissions for " +
			                         path + ": " + std::string(strerror(errno)));
	}


	// Make the daemon user own the directory to be able to remove the
	// rpc socket on exit.
	if (fchownat(fd, ".", opts.user, opts.group,
	             AT_SYMLINK_NOFOLLOW) == -1)
	{
		throw std::runtime_error("Could not set owner for " + path +
		                         ": " + std::string(strerror(errno)));
	}

	return fd;
}

static pidfh *create_pid_file(const conf_options &opts)
{
	pidfh *tmp;
	pid_t other_pid;

	tmp = pidfile_open((opts.socket_dir + "/blued.pid").c_str(), 0600,
	                   &other_pid);
	if (!tmp)
	{
		if (errno == EEXIST)
		{
			syslog(LOG_ERR, "Daemon already running, pid: %i", other_pid);
			throw std::runtime_error("Daemon already running");
		}

		// It is okay for pid_file to be null since the other
		// PID functions accept null pointers.
		syslog(LOG_NOTICE, "Could not create PID file");
	}
	pidfile_write(tmp);

	return tmp;
}

blue_daemon::blue_daemon(const conf_options &opts,
		blue::util::shared_cap_chan chan, ev::loop_ref loop) :
	loop(loop),
	rpc_watcher(loop),
	socket_dir_fd(create_socket_dir(opts)),
	pid_file(create_pid_file(opts)),
	hci_socket(loop),
	scanning(false),
	hid(chan, this, opts.uinput, loop),
	db(opts.db_path)
{
	connect_socket(opts);

	hci_socket.bind(opts.hci_node);
	hci_socket.connect(opts.hci_node);
	hci_socket.register_socket_observer(this);

	syslog(LOG_DEBUG, "Local HCI address: %s",
	       get_local_address().str().c_str());

	try
	{
		devices = db.load_devices(&hci_socket);

		for (auto& [addr, device] : devices)
			device.register_observer(this);
	}
	catch (const std::exception &e)
	{
		syslog(LOG_ERR, "Failed to load devices from DB: %s", e.what());
	}

}

blue_daemon::~blue_daemon()
{
	unlinkat(socket_dir_fd, socket_name.c_str(), 0);
	pidfile_remove(pid_file);
}

void blue_daemon::device_event(blue::btdevice &d, uint8_t operation,
		uint8_t status)
{
}

void blue_daemon::pairing_finished(blue::btdevice &d,
		bool success)
{
	syslog(LOG_DEBUG, "Device pairing %s ",
			success ? "succeeded" : "failed");
	try
	{
		if (success)
		{
			db.save_device(d);

			// Try to make a HID connection, hid_daemon will check whether
			// it is an actual HID device.
			hid.connect(d.get_addr());
		}
	}
	catch (const std::runtime_error &e)
	{
		syslog(LOG_ERR, "%s", e.what());
	}

	for (rpc_client &client : clients)
		client.pairing_finished(d.get_addr(), success);

}

void blue_daemon::remote_name(blue::btdevice &d, bool success)
{
	if (success)
	{
		syslog(LOG_DEBUG, "Device %s reported new name %s",
		       d.get_addr().str().c_str(), d.get_name().c_str());

		// Only remember the name of paired devices. No need to try to
		// fill the database with random devices.
		if (paired(d.get_addr()))
			db.save_name(d);
	}
	else
		syslog(LOG_DEBUG, "Name request for device %s failed",
		       d.get_addr().str().c_str());

	// Calling d.get_name() is safe on both success and failure.
	for (rpc_client &client : clients)
		client.remote_name(d.get_addr(), success, d.get_name());
}

void blue_daemon::connection_complete(blue::btdevice &d, bool success)
{
	syslog(LOG_DEBUG, "%s: connection %s", d.get_addr().str().c_str(),
			success ? "successful" : "failed");

	// If we don't have the device's name, query for it.
	if (success && d.get_name() == "")
		d.request_name();

	for (rpc_client &client : clients)
		client.connection_finished(d.get_addr(), success);
}

void blue_daemon::disconnect_complete(blue::btdevice &d, bool success)
{
	syslog(LOG_DEBUG, "%s: disconnect %s", d.get_addr().str().c_str(),
			success ? "successful" : "failed");

	for (rpc_client &client : clients)
		client.disconnect_finished(d.get_addr(), success);
}

void blue_daemon::hci_event(blue::hci_socket &s, uint8_t operation,
		uint8_t status)
{
}

void blue_daemon::query_complete(blue::hci_socket &s, bool success,
		const std::vector<blue::device_info> &discovered_devices)
{
	scanning = false;

	if (!success)
		syslog(LOG_ERR, "Device query failed.");

	// Report that the scan finished to our RPC clients.
	for (rpc_client &client : clients)
		client.scan_finished(discovered_devices);

	// Update the device list to only contain paired devices since we
	// want to keep them regardless of them being detected in the scan
	// or not while the unpaired devices will be added back to the
	// list if they are still visible.
	devices = extract_paired();

	// Add any visible unpaired device and update the parameters of
	// paired devices.
	update_device_list(s, discovered_devices);
}

const std::map<blue::bdaddr, blue::btdevice> &blue_daemon::get_devices() const
{
	return devices;
}

void blue_daemon::scan()
{
	if (!scanning)
	{
		hci_socket.query_devices();
		scanning = true;
	}
}

void blue_daemon::connect_socket(const conf_options &opts)
{
	struct stat sb;

	/*
	 * If the fstatat fails, we assume that the socket doesn't exist
	 * and try to create it. If it failed  for some other reason, the
	 * socket creation will catch it.
	 */
	if (fstatat(socket_dir_fd, opts.socket_name.c_str(), &sb,
	            AT_SYMLINK_NOFOLLOW) == 0)
	{
		if (S_ISSOCK(sb.st_mode))
		{
			if (unlinkat(socket_dir_fd, opts.socket_name.c_str(), 0) == -1)
				throw std::runtime_error("Could not remove old RPC socket: " +
				                         std::string(strerror(errno)));
		}
		else
			throw std::runtime_error("A file that is not a socket already "
			                         "exists at the RPC socket path.");
	}

	rpc_fd = blue::util::make_unique_fd(socket(PF_UNIX, SOCK_STREAM, 0));
	if (rpc_fd == -1)
		throw std::runtime_error("Could not create socket: " +
				std::string(strerror(errno)));

	sockaddr_un addr{};

	if (opts.socket_name.length() >= sizeof(addr.sun_path))
		throw std::overflow_error("socket path too long");

	std::copy(opts.socket_name.begin(), opts.socket_name.end(), addr.sun_path);
	addr.sun_len = opts.socket_name.length();
	addr.sun_family = AF_UNIX;

	if (bindat(socket_dir_fd, rpc_fd, reinterpret_cast<sockaddr *>(&addr),
	           sizeof(addr)) == -1)
	{
		throw std::runtime_error("Could not connect unix socket: " +
				std::string(strerror(errno)));
	}

	if (fchmodat(socket_dir_fd, opts.socket_name.c_str(),
	             opts.socket_permissions, AT_SYMLINK_NOFOLLOW) == -1)
	{
		throw std::runtime_error("Could not set socket permissions: " +
		                         std::string(strerror(errno)));
	}

	if (fchownat(socket_dir_fd, opts.socket_name.c_str(),
	             opts.user, opts.group, AT_SYMLINK_NOFOLLOW) == -1)
	{
		throw std::runtime_error("Could not set socket ownership: " +
		                         std::string(strerror(errno)));
	}

	if (listen(rpc_fd, -1) == -1)
	{
		throw std::runtime_error("Could not listen on unix socket: " +
				std::string(strerror(errno)));
	}

	rpc_watcher.set<blue_daemon, &blue_daemon::rpc_callback>(this);
	rpc_watcher.set(rpc_fd, EV_READ);
	rpc_watcher.start();

	// Remember the socket name for the destructor.
	socket_name = opts.socket_name;
}

void blue_daemon::rpc_callback(ev::io &watcher, int events)
{
	if (events & EV_READ)
	{
		sockaddr_un addr{};
		unsigned int addr_len = sizeof(addr);
		int fd = accept(rpc_fd, reinterpret_cast<sockaddr *>(&addr), &addr_len);
		if (fd != -1)
		{
			clients.emplace_front(blue::util::make_unique_fd(fd), loop, *this);
		}
		else
			syslog(LOG_ERR, "Failed to accept rpc client from socket: %s",
					strerror(errno));
	}
}

void blue_daemon::client_disconnected(rpc_client &client)
{
	// Since client is deleted, the client reference MUST NOT be used
	// after this line.
	clients.remove(client);
}

blue::rpc_op blue_daemon::scan_request()
{
	scan();

	return blue::rpc_op::OP_SCAN_START;
}

bool blue_daemon::paired(blue::bdaddr addr) const
{
	if (devices.count(addr) > 0)
		return devices.at(addr).is_paired();
	else
		return false;
}

bool blue_daemon::known(blue::bdaddr addr) const
{
	return devices.count(addr) > 0;
}

blue::bdaddr blue_daemon::get_local_address() const
{
	return hci_socket.get_address();
}

blue::rpc_op blue_daemon::pair_request(const blue::bdaddr &addr)
{
	blue::rpc_op ret;

	if (!known(addr))
		return blue::rpc_op::OP_PAIR_UNKNOWN;

	try
	{
		if (devices.at(addr).pair())
			ret = blue::rpc_op::OP_PAIR_START;
		else
			ret = blue::rpc_op::OP_PAIR_ALREADY_PAIRED;
	}
	catch (const std::runtime_error &e)
	{
		ret = blue::rpc_op::OP_PAIR_ERROR;
		syslog(LOG_ERR, "Could not pair device: %s", e.what());
	}

	return ret;
}

blue::rpc_op blue_daemon::set_pin_request(const blue::bdaddr &addr,
		const std::string &pin)
{
	blue::rpc_op ret;

	if (!known(addr))
		return blue::rpc_op::OP_DEVICE_PIN_UNKNOWN;

	try
	{
		devices.at(addr).set_pin(pin);

		ret = blue::rpc_op::OP_DEVICE_PIN_DONE;
	}
	catch (const std::runtime_error &)
	{
		ret = blue::rpc_op::OP_DEVICE_PIN_TOO_LONG;
	}

	return ret;
}

const std::map<blue::bdaddr, blue::btdevice> &blue_daemon::list_known_request()
{
	return get_devices();
}

blue::rpc_op blue_daemon::unpair_request(const blue::bdaddr &addr)
{
	if (!known(addr))
		return blue::rpc_op::OP_PAIR_UNKNOWN;

	blue::btdevice &d = devices.at(addr);

	if (!d.is_paired())
		return blue::rpc_op::OP_UNPAIR_NOT_PAIRED;

	hid.disconnect(d.get_addr());

	try
	{
		db.delete_device(d);
	}
	catch (const std::runtime_error &e)
	{
		syslog(LOG_ERR, "Failed to delete device %s from DB: %s",
			   d.get_addr().str().c_str(), e.what());
		return blue::rpc_op::OP_UNPAIR_ERROR;
	}

	devices.erase(addr);

	return blue::rpc_op::OP_UNPAIR_DONE;
}

blue::rpc_op blue_daemon::name_request(const blue::bdaddr &addr)
{
	blue::rpc_op ret;

	if (known(addr))
	{
		devices.at(addr).request_name();
		ret = blue::rpc_op::OP_NAME_REQUEST_RUNNING;
	}
	else
		ret = blue::rpc_op::OP_NAME_REQUEST_UNKNOWN;

	return ret;
}

blue::rpc_op blue_daemon::connection_request(const blue::bdaddr &addr)
{
	blue::rpc_op ret;

	if (!known(addr))
		return blue::rpc_op::OP_CONNECTION_REQUEST_UNKNOWN;

	blue::btdevice &d = devices.at(addr);

	if (d.is_connected())
		ret = blue::rpc_op::OP_CONNECTION_REQUEST_DONE;
	else
	{
		d.connect();
		ret = blue::rpc_op::OP_CONNECTION_REQUEST_RUNNING;
	}

	return ret;
}

blue::rpc_op blue_daemon::disconnect_request(const blue::bdaddr &addr)
{
	blue::rpc_op ret;

	if (!known(addr))
		return blue::rpc_op::OP_DISCONNECT_REQUEST_UNKNOWN;

	blue::btdevice &d = devices.at(addr);

	if (!d.is_connected())
		ret = blue::rpc_op::OP_DISCONNECT_REQUEST_DONE;
	else
	{
		d.disconnect();
		ret = blue::rpc_op::OP_DISCONNECT_REQUEST_RUNNING;
	}

	return ret;
}

std::map<blue::bdaddr, blue::btdevice> blue_daemon::extract_paired()
{
	std::map<blue::bdaddr, blue::btdevice> ret;

	for (auto &[addr, device] : devices)
	{
		if (device.is_paired())
		{
			ret.emplace(addr, std::move(device));
		}
	}

	return ret;
}

void blue_daemon::update_device_list(blue::hci_socket &s,
		const std::vector<blue::device_info> &discovered_devices)
{
	for (const blue::device_info &info: discovered_devices)
	{
		if (!known(info.addr))
		{
			// Construct blue::btdevice objects for discovered objects
			// that are not known.
			blue::btdevice d(info, &s);
			d.register_observer(this);
			d.request_name();
			devices.emplace(info.addr, std::move(d));
		}
		else
		{
			// If the objects is known, only update the existing
			// object's parameters.
			blue::btdevice &d = devices.at(info.addr);
			d.set_page_scan_mode(info.page_scan_mode);
			d.set_page_scan_rep_mode(info.page_scan_rep_mode);
			d.set_page_scan_period_mode(info.page_scan_period_mode);
			d.set_clock_offset(info.clock_offset);
			d.request_name();
		}
	}
}
