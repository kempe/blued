#ifndef CONF_H
#define CONF_H
#include <string>

#include <sys/stat.h>

struct conf_options
{
	// Path of the RPC socket dir
	std::string socket_dir;
	// Name of the RPC socket
	std::string socket_name;
	// socket permissions
	mode_t socket_permissions;
	// DB path
	std::string db_path;
	// HCI node name
	std::string hci_node;
	// user to drop to after resources have been acquired
	uid_t user;
	// group to drop to after resources have been acquired
	gid_t group;
	// whether to enable uinput support
	bool uinput;
};

#endif // CONF_H
